<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3412393" href="./book_N3412393.html">SuiteTalk (Web Services)</a>
            <a rev="part_N3412452" href="./part_N3412452.html">SuiteTalk (Web Services) Platform Guide</a>
            <a rev="chapter_N3428523" href="./chapter_N3428523.html">Records, Fields, Forms, and Sublists in Web Services</a>
            <a rev="section_N3428663" href="./section_N3428663.html">Working with Records in Web Services</a>
            <a class="nshelp_navlast" rev="section_N3432681" href="./section_N3432681.html">Using Internal IDs, External IDs, and References</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N3432681" class="nshelp_title">Using Internal IDs, External IDs, and References</h1>
      <p>Each record in NetSuite can be uniquely identified by its record type in combination with either an external ID or a system-generated NetSuite internal ID.</p>
      <p>Internal and external IDs are NOT unique across all record types. Therefore, to perform an operation on an existing record, you need two pieces of data: the record type and either the internal ID or the external ID.</p>
      <p>References are implemented through the RecordRef type, which is defined in the <a href="https://webservices.netsuite.com/xsd/platform/v2016_2_0/core.xsd" target="_blank">core</a> XSD.</p>
      <p>The RecordRef type is described by three attributes — the internal ID, external ID, and the type:</p>
      <pre class="nshelp">&lt;complexType name="RecordRef"&gt;
&lt;complexContent&gt;
   &lt;extension base="platformCore:BaseRef"&gt;
      &lt;attribute name="internalId" type="xsd:string"/&gt;
      &lt;attribute name="externalId" type="xsd:string"/&gt;
      &lt;attribute name="type" type="platformCoreTyp:RecordType"/&gt;
   &lt;/extension&gt;
&lt;/complexType&gt;</pre>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">When referencing records in any particular update operation, you must provide the internalId or the externalId, but not both. If both are provided, the internalId is used to locate the record and the externalId is ignored. Additionally, the externalid attribute is case sensitive.</p>
      </div>
      <br>
      <h3 id="bridgehead_3740632950">Uniqueness of Internal and External IDs</h3>
      <p>Internal and external IDs must be unique not just within a specific record type, but also within certain record groups. These groups consist of multiple record types. For details, see <a rev="section_N3436356" href="./section_N3436356.html">Shared Internal and External IDs</a>.</p>
      <p>Further, internal IDs are not reused in the system. After an internal ID has been assigned, if the associated record is subsequently deleted, the ID is not reused.</p>
      <p>Note also that an external ID exists only if you provided it, either at the time of record creation or during an update. For more on external IDs, see <a rev="section_N3433806" href="./section_N3433806.html">Understanding External IDs</a>.</p>
      <h3 id="bridgehead_3740633421">Internal IDs Do Not Indicate When Record Was Created</h3>
      <p>NetSuite's assignment of internal IDs to new records is sequential, but not based on chronology or when the record was added. This is true even within specific record groups where internal IDs are unique. Therefore, users should not conclude that records with lower internal IDs were added first, whereas those with higher internal IDs were added last.</p>
      <p>For custom applications that rely on the chronological aspect of when data was entered, NetSuite suggests using the Date Modified or Date Created fields, as they are guaranteed to be chronological. Additionally, for transactions with auto-generated numbers, the sequence number (for example, “tranid”) is guaranteed to be sequential based on the date transactions were entered. The internal ID of a record should never be used to determine when data was entered.</p>
      <h3 id="bridgehead_N3432744">Internal IDs for NetSuite Record Types</h3>
      <p>Like individual records, each record <em>type</em> also has an internal ID. In certain situations, you must use this ID to specify which type of record you are referencing. By contrast, in some cases, you can use a string to reference the record type (for example, when using RecordRef). But in other cases you must use the internal ID, or typeId (for example, when using <a rev="section_N3452954" href="./section_N3452954.html#bridgehead_N3455299">ListOrRecordRef</a> or <a rev="section_N3452954" href="./section_N3452954.html#bridgehead_N3454996">CustomRecordRef</a>). The table below lists the NetSuite record types and their associated typeIds.</p>
      <p>Note that custom fields of type <a rev="section_N3458179" href="./section_N3458179.html#bridgehead_N3460021">MultiSelectCustomFieldRef</a> on custom records also reference these values, because they contain an array of <a rev="section_N3452954" href="./section_N3452954.html#bridgehead_N3455299">ListOrRecordRef</a> types. For details on working with custom records, see <a rev="section_N3768988" href="./section_N3768988.html">Custom Record</a>.</p>
      <table class="alternate_rows" style="min-width:134px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:82px;" class="bolight_bgdark">
								<p>Record Type</p>
							</th>
            <th style="min-width:34px;" class="bolight_bgdark">
								<p>typeId</p>
							</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:82px;" class="bolight_bgwhite">
								<p>Account</p>
							</td>
            <td style="min-width:34px;" class="bolight_bgwhite">
								<p>-112</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Accounting Period</p>
							</td>
            <td class="bolight_bglight">
								<p>-105</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Bin</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-242</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Call</p>
							</td>
            <td class="bolight_bglight">
								<p>-22</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Campaign</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-24</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Case</p>
							</td>
            <td class="bolight_bglight">
								<p>-23</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Class</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-101</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Competitor</p>
							</td>
            <td class="bolight_bglight">
								<p>-108</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Contact</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-6</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Customer</p>
							</td>
            <td class="bolight_bglight">
								<p>-2</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Customer Category</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-109</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Department</p>
							</td>
            <td class="bolight_bglight">
								<p>-102</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Email Template</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-120</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Employee</p>
							</td>
            <td class="bolight_bglight">
								<p>-4</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Employee Type</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-111</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Entity Status</p>
							</td>
            <td class="bolight_bglight">
								<p>-104</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Event</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-20</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Issue</p>
							</td>
            <td class="bolight_bglight">
								<p>-26</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Item</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-10</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Item Type</p>
							</td>
            <td class="bolight_bglight">
								<p>-106</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Job (Project)</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-7</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Location</p>
							</td>
            <td class="bolight_bglight">
								<p>-103</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Module</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-116</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Opportunity</p>
							</td>
            <td class="bolight_bglight">
								<p>-31</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Partner</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-5</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Product</p>
							</td>
            <td class="bolight_bglight">
								<p>-115</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Product Build</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-114</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Product Version</p>
							</td>
            <td class="bolight_bglight">
								<p>-113</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Project (Job)</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-7</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Role</p>
							</td>
            <td class="bolight_bglight">
								<p>-118</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Saved Search</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-119</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Subsidiary</p>
							</td>
            <td class="bolight_bglight">
								<p>-117</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Task</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-21</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Transaction</p>
							</td>
            <td class="bolight_bglight">
								<p>-30</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Transaction Type</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-100</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Vendor</p>
							</td>
            <td class="bolight_bglight">
								<p>-3</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Vendor Category</p>
							</td>
            <td class="bolight_bgwhite">
								<p>-110</p>
							</td>
          </tr>
        </tbody>
      </table>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3428663" href="./section_N3428663.html">Working with Records in Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3433806" href="./section_N3433806.html">Understanding External IDs</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3436475" href="./section_N3436475.html">Working with Fields in Web Services</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

