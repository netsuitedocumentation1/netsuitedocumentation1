<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3412393" href="./book_N3412393.html">SuiteTalk (Web Services)</a>
            <a rev="part_N3412452" href="./part_N3412452.html">SuiteTalk (Web Services) Platform Guide</a>
            <a rev="chapter_N3445516" href="./chapter_N3445516.html">Web Services Security</a>
            <a class="nshelp_navlast" rev="section_N3447680" href="./section_N3447680.html">Session Management for Web Services</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N3447680" class="nshelp_title">Session Management for Web Services</h1>
      <p>After a user has been successfully authenticated using the <a rev="section_N3512617" href="./section_N3512617.html">login</a> operation, a sessionID is created that must be passed to each subsequent request. Additional logins are not required as long as the session is active.</p>
      <p>In NetSuite, sessions management includes:</p>
      <ul class="nshelp">
        <li>
          <p>
            <a rev="section_N3447680" href="#bridgehead_N3447809">Session Timeouts</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N3447680" href="#bridgehead_N3447864">Session Limits</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N3447680" href="#bridgehead_N3447901">Manually Managing Cookies</a>
          </p>
        </li>
      </ul>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">The following topics pertain to sessions that have been initiated using the <a rev="section_N3512617" href="./section_N3512617.html">login</a> operation. Users who authenticate to NetSuite by submitting their credentials in the SOAP header of their request should see <a rev="section_4489690806" href="./section_4489690806.html#bridgehead_N3447228">Request-Level Credentials</a>.</p>
      </div>
      <br>
      <h2 id="bridgehead_N3447809">Session Timeouts</h2>
      <p>The Open Web Application Security Project (OWASP) provides the following guideline: All sessions should implement an absolute timeout, regardless of session activity. This timeout defines the maximum amount of time a session can be active. The session is closed and invalidated upon the defined absolute period. After the session is invalidated, the user must authenticate (log in) again in the web application and establish a new session. The absolute session timeout limits the amount of time possible for a potential attacker to use a hijacked session to impersonate a user.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">To enhance the security of your account, and to comply with the OWASP guideline, NetSuite 2017.1 implements an absolute session timeout for web services sessions. Currently, the absolute session timeout value is set to 60 minutes.</p>
      </div>
      <br>
      <p>If you use sessions with your web services integrations, you must ensure that your web services calls are able to handle session timeouts. It is recommended that your integrations should use sessionless protocols based on request level credentials, such as User Credentials or Token-based Authentication (TBA). See <a rev="section_N3445710" href="./section_N3445710.html">Authentication for Web Services</a> for more information.</p>
      <p>As in previous releases, web services sessions automatically time out after 20 minutes of inactivity, requiring a login to resume activity. If the server resubmits the cookie during the first 20 minutes, the inactivity timer is reset.</p>
      <p>If a web services operation takes more than 15 minutes to complete, it automatically times out. In this case, consider using asynchronous request processing. For more information, see <a rev="section_N3444207" href="./section_N3444207.html">Synchronous Versus Asynchronous Request Processing</a>.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">If you need shorter sessions for security reasons, you should create a login route for the client that calls <a rev="section_N3513430" href="./section_N3513430.html">logout</a> when operations are complete.</p>
      </div>
      <br>
      <p>If you explicitly log out of a session, and then attempt to use the same session, a SESSION_TIMED_OUT error message is returned. Your code should be prepared to handle session timeouts by retrying if an InvalidSessionFault (SESSION_TIMED_OUT) is seen.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">Do not make any integration decisions based on the value of the absolute timeout value or the idle session timeout values, as these values can be changed at any time without notice.</p>
      </div>
      <br>
      <p>Timeouts for NetSuite UI and NetSuite Web store sessions are handled differently than web services session timeouts.</p>
      <ul class="nshelp">
        <li>
          <p>After 180 minutes of inactivity in the NetSuite application, the screen is locked and a popup displays requiring the user to log in again.</p>
        </li>
        <li>
          <p>After 20 minutes of inactivity in a NetSuite Web store, the user is logged out and becomes an anonymous shopper. (There is no automatic relogin to the Web store).</p>
        </li>
      </ul>
      <h2 id="bridgehead_N3447864">Session Limits</h2>
      <p>A specific login (username/password) is limited to two sessions, one through the browser and one through web services. These two sessions can occur concurrently. The UI session and the web services session do not cause termination of each other.</p>
      <p>However, if two web services clients attempt to establish two concurrent sessions, the first session is terminated. Note that more than two independent concurrent sessions are possible with the use of products such as NetSuite for Outlook.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Using the same login from multiple client applications, or multiple instances of the same application is NOT supported. However, NetSuite does offer a Web Services Concurrent License for purchase. This license allows designated employees to use the same credentials ten times concurrently before their first session is invalidated. For details see <a rev="section_N3448861" href="./section_N3448861.html">Enabling Web Services Concurrent Users with SuiteCloud Plus</a>.</p>
      </div>
      <br>
      <h2 id="bridgehead_N3447901">Manually Managing Cookies</h2>
      <p>When submitting web services requests, the NetSuite server can accept any SOAP document as long as it is valid against the NetSuite WSDL — it does not matter how the SOAP was generated. When generating a SOAP document from tools or sources that do NOT automatically generate the NetSuite port objects, you must ensure that all cookies received from the server are managed and passed back to the server in subsequent calls. There are a total of three cookies that are initially returned in the HTTP header of the login response, including the JSESSIONID.</p>
      <p>Ensure that your client persists any cookies that our server sends you (re-submits the cookie on the next request). In Axis, this is accomplished by enabling <strong>MaintainSession</strong> on the generated Binding object. In .NET this involves allocating a CookieContainer. If persistence is not maintained, you will receive an InvalidSessionFault — PLEASE_LOGIN_BEFORE_PERFORMING_ACTION.</p>
      <h3 id="bridgehead_3819268808">Example</h3>
      <p>The following code shows the cookie information returned in the login response.</p>
      <pre class="nshelp">HTTP/1.1 200 OK
Date: Wed, 18 May 2008 18:43:27 GMT
Server: Oracle-Application-Server-10g/9.0.4.0.0 Oracle-HTTP-Server
Set-Cookie: NS_VER=2008.1.0; domain=joe.corp.netsuite.com; path=/
Vary: User-Agent
Set-Cookie: JSESSIONID=ac101fae1f4312dfxxx062fc829447eaa00c3dcd70af41d; Path=/
Set-Cookie: lastUser=TSTDRV198400_935_3; Expires=Wed, 25-May-2008 17:42:24 GMT; Path=/
Cache-Control: private
Keep-Alive: timeout=150, max=1000
Connection: Keep-Alive
Transfer-Encoding: chunked
Content-Type: text/xml; charset=utf-8</pre>
      <p>For each subsequent request, all of the cookies must be returned in the HTTP header to maintain the session as follows:</p>
      <h3 id="bridgehead_N3447951">C#</h3>
      <pre class="nshelp">NetSuiteService nss = new NetSuiteService();
nss.Url = https://webservices.netsuite.com/services/NetSuitePort_2015_1_0 ; 
nss.CookieContainer = new CookieContainer();</pre>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">This snippet assumes that your integration includes a <code>Using System.Net;</code> statement.</p>
      </div>
      <br>
      <h3 id="bridgehead_3705125415">Java with Axis 1.3</h3>
      <pre class="nshelp">NetSuiteServiceLocator nss = new NetSuiteServiceLocator();
nss.setMaintainSession(true);</pre>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3412777" href="./chapter_N3412777.html">SuiteTalk Platform Overview</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3445516" href="./chapter_N3445516.html">Web Services Security</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3445710" href="./section_N3445710.html">Authentication for Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3447510" href="./section_N3447510.html">Authorization for Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3448169" href="./section_N3448169.html">Encryption for Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3448524" href="./section_N3448524.html">Working with Custom Field Security</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

