<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3363377" href="./book_N3363377.html">SuiteBundler</a>
            <a rev="chapter_N3364150" href="./chapter_N3364150.html">SuiteApp Creation and Distribution</a>
            <a rev="section_N3384420" href="./section_N3384420.html">Working with Saved Bundles</a>
            <a class="nshelp_navlast" rev="section_N3391248" href="./section_N3391248.html">Copying a Bundle to Other Accounts</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N3391248" class="nshelp_title">Copying a Bundle to Other Accounts</h1>
      <p><span id="dyn_help_feature" dynid="CREATESUITEBUNDLES">&nbsp;</span>   </p>
      <p>You can use the Copy option available in the Action dropdown on the Saved Bundles page to copy a customization bundle from one account into another. You can copy one bundle to multiple other accounts. If you make changes to the original bundle, you can repeat the copy action to update copied bundles in other accounts.</p>
      <ul class="nshelp">
        <li>
          <p>Copied bundles expand the options for bundle deployment. For example, you can copy a bundle to a deployment account to support your release process.</p>
        </li>
        <li>
          <p>Copied bundles can help you to implement a bundle versioning strategy. You can copy a bundle to a development account, allowing the separation of changes to the released bundle version from the development of a new version.</p>
        </li>
        <li>
          <p>The ability to copy bundles is linked to the ability to deprecate bundles, meaning replacing a bundle with an updated version. For details about deprecation, see <a rev="section_N3386679" href="./section_N3386679.html">Deprecating a Bundle</a>.</p>
        </li>
      </ul>
      <p>For details about copying bundles to other accounts, see the following:</p>
      <ul class="nshelp">
        <li>
          <p>
            <a rev="section_N3391248" href="#bridgehead_N3391452">Limitations for Copying Bundles</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N3391248" href="#bridgehead_N3391534">Steps to Copy a Bundle</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N3391248" href="#bridgehead_N3391649">Reviewing Copy Bundle Status</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N3391248" href="#bridgehead_N3391699">Notes about Copied Bundles</a>
          </p>
        </li>
      </ul>
      <h3 id="bridgehead_N3391452">Limitations for Copying Bundles</h3>
      <p>Note the following limitations on copying bundles:</p>
      <ul class="nshelp">
        <li>
          <p>The copy action is available only for customization bundles. You cannot copy or deprecate configuration bundles.</p>
        </li>
        <li>
          <p>You cannot create a copy of the bundle in the same account where it is currently located (the source account). Each copy must be made to a different account.</p>
        </li>
        <li>
          <p>You cannot create more than one copy in any account. When you copy a bundle to an account where it has been previously copied, the existing copy is updated.</p>
        </li>
        <li>
          <p>Bundle copy is only supported for production accounts and development accounts. You cannot copy a bundle from or to any type of account other than production or development.</p>
        </li>
        <li>
          <p>You cannot copy a bundle to an account where it has been previously installed, and you cannot install a bundle in an account where it has been previously copied. For bundles that are copies of copies, this limitation extends to any bundles that have a common ancestor, based on tracking of the full ancestral path of each bundle.</p>
        </li>
      </ul>
      <h3 id="bridgehead_N3391534">Steps to Copy a Bundle</h3>
      <div class="nshelp_procedure">
        <h4 id="procedure_N3391543">To copy a bundle:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to Customization &gt; SuiteBundler &gt; Create Bundle &gt; List.</p>
          </li>
          <li>
            <p>In the <strong>Action</strong> dropdown for the bundle you want to deprecate, click <strong>Copy</strong>.</p>
            <p>The Copy Bundle to Accounts page displays, with a list of the accounts to which you have administrator access. (Account IDs and names in the following screenshot have been obfuscated for privacy reasons.)</p>
            <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBundler/BundleCopy2.png" width="608px" height="145px">
          </li>
          <li>
            <p>Check one or more boxes in the <strong>Copy/Update</strong> column, and click the <strong>Copy</strong> button.</p>
            <p>The <strong>Status</strong> column indicates whether the bundle has been previously copied in the account.</p>
            <ul class="nshelp">
              <li>
                <p>If you check the box for an account where the bundle has not been copied, a new copy is created.</p>
              </li>
              <li>
                <p>If you check the box for an account where the bundle has been previously copied, this copy is updated with any changes.</p>
              </li>
            </ul>
          </li>
        </ol>
      </div>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">During a bundle copy, if conflicts are detected between a copied bundle's objects and existing target account objects, copied bundle objects are added and renamed. For more information about how SuiteBundler resolves conflicts, see <a rev="section_N3394371" href="./section_N3394371.html">Resolving Conflicting Objects</a>.</p>
      </div>
      <br>
      <h3 id="bridgehead_N3391649">Reviewing Copy Bundle Status</h3>
      <p>After you click the Copy button on the Copy Bundle to Accounts page, the Copy Bundle Status page displays. This page is also available from the Saved Bundles page, by clicking Show Copies in the Action dropdown.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBundler/BundleCopyStatus.png" width="630px" height="138px">
      <ul class="nshelp">
        <li>
          <p>The copied bundle's status displays as Pending, whereas previously copied bundle(s) display a green check mark to indicate that the copy action is complete.</p>
        </li>
        <li>
          <p>Each copied bundle has a new bundle ID, different from the ID of the source bundle.</p>
        </li>
      </ul>
      <h3 id="bridgehead_N3391699">Notes about Copied Bundles</h3>
      <ul class="nshelp">
        <li>
          <p>A copied bundle is available from the Saved Bundles list in the new account and has the same actions available as bundles created in that account. It includes a Copied From column with the bundle ID and account ID of the bundle from which it was copied, in the format <strong>&lt;Bundle ID&gt; Account:&lt;Account ID&gt;</strong>.</p>
        </li>
        <li>
          <p>When you copy a bundle that includes locked objects, the objects are not locked in the account where the bundle is copied <a rev="section_N3373802" href="./section_N3373802.html">Step 4 Set Preferences</a> . However, their locked settings are maintained on the <a rev="section_N3373802" href="./section_N3373802.html">Step 4 Set Preferences</a> page of the copied bundle, and are enforced in target accounts where the copied bundle is installed, unless the settings are changed in the copied bundle. See <a rev="section_N3376982" href="./section_N3376982.html">Locking Objects in Customization Bundles</a>.</p>
        </li>
        <li>
          <p>When you copy a bundle that includes hidden scripts, the scripts are not hidden in the account where the bundle is copied. However, their hidden property is enforced in target accounts where the copied bundle is installed. See <a rev="section_N3377764" href="./section_N3377764.html">Protecting Your Bundled Server SuiteScripts</a>.</p>
        </li>
        <li>
          <p>Bundle object preferences set in the source account are not applied to the account where the bundle is copied. Note that data is always replaced and script deployments are always updated during an update of a bundle copy. However, source account preferences are maintained on the <a rev="section_N3373802" href="./section_N3373802.html">Step 4 Set Preferences</a> page of the copied bundle, and are applied in target accounts where the copied bundle is installed, unless preferences are edited in the copied bundle. See <a rev="section_N3376108" href="./section_N3376108.html">Setting Bundle Object Preferences</a>.</p>
        </li>
        <li>
          <p>The initial availability of a copied bundle is always set to Private, no matter what the availability of the source bundle was, and any shared account IDs listed for the source bundle are cleared from the copied bundle. Copied bundle availability can be reset to Shared or Public as needed, and the account ID list can be cut and pasted from the source bundle, if desired.</p>
          <ul class="nshelp">
            <li>
              <p>To change the availability level and add account IDs for a copied bundle, click Set Availability in the Action dropdown. See <a rev="section_N3385867" href="./section_N3385867.html">Sharing a Bundle</a>.</p>
            </li>
            <li>
              <p>To make other changes to a copied bundle, click Edit in the Action dropdown. See <a rev="section_N3392791" href="./section_N3392791.html">Editing a Bundle</a>.</p>
            </li>
          </ul>
        </li>
        <li>
          <p>You can differentiate objects added to your account through bundle copy from other account objects, because they have a bundle ID listed in the From Bundle column of list pages. See <a rev="section_N3401780" href="./section_N3401780.html">Identifying Bundle Objects in Target Accounts</a>.</p>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3364150" href="./chapter_N3364150.html">SuiteApp Creation and Distribution</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3384420" href="./section_N3384420.html">Working with Saved Bundles</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3385668" href="./section_N3385668.html">Setting Bundle Availability</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3386679" href="./section_N3386679.html">Deprecating a Bundle</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3382953" href="./section_N3382953.html">Understanding Managed Bundles</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4444212213" href="./chapter_4444212213.html">SuiteApp Development Process with SuiteBundler</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

