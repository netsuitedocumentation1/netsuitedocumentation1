<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2723865" href="./book_N2723865.html">SuiteFlow (Workflow)</a>
            <a rev="chapter_4103690129" href="./chapter_4103690129.html">SuiteFlow Reference and Examples</a>
            <a rev="section_4103731715" href="./section_4103731715.html">Triggers Reference</a>
            <a rev="section_4103734459" href="./section_4103734459.html">Server Triggers Reference</a>
            <a class="nshelp_navlast" rev="section_4103739018" href="./section_4103739018.html">After Record Submit Trigger</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4103739018" class="nshelp_title">After Record Submit Trigger</h1>
      <p>The After Record Submit trigger is a server trigger and executes after NetSuite saves the record data to the database. Actions and transitions set to execute on After Record Submit execute on the NetSuite server, after the record data is saved to the database.</p>
      <p>When an action executes on an After Record Submit trigger, NetSuite internally reloads the record, executes the action, and then saves the record again. NetSuite only saves the record again if the record has changed by the action.</p>
      <p>The After Record Submit trigger can be used for the following elements of a workflow:</p>
      <ul class="nshelp">
        <li>
          <p>Workflow initiation. See <a rev="section_N2730601" href="./section_N2730601.html#bridgehead_4108930105">Workflow Initiation Triggers</a> and <a rev="section_4077993199" href="./section_4077993199.html">SuiteFlow Trigger Execution Model</a>.</p>
        </li>
        <li>
          <p>Actions. For a list of actions that support the After Record Submit trigger, see <a rev="section_4150693781" href="./section_4150693781.html">Workflow Triggers Quick Reference</a>.</p>
        </li>
        <li>
          <p>Transitions. See <a rev="section_4079591327" href="./section_4079591327.html">Transition Triggers</a>.</p>
        </li>
      </ul>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">NetSuite can execute workflows on the After Record Submit trigger asynchronously. See <em>Asynchronous Execution on the After Record Submit Trigger</em>.</p>
      </div>
      <br>
      <p>The following figure shows typical record events and when the After Record Submit trigger executes:</p>
      <img class="nshelp_diagram_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteFlowWorkflow/AfterRecordSubmitTriggerDiagram.png" width="630px" height="182px">
      <p>In general, use the After Record Submit trigger when you want to use a workflow to make changes to the record after NetSuite saves the data to the database. For example, use the After Record Submit trigger in the following situations:</p>
      <ul class="nshelp">
        <li>
          <p>A field or field value value is not available until after NetSuite saves the record.</p>
        </li>
        <li>
          <p>Actions should occur after NetSuite saves the record. These actions can include the Send Email, Go To Page or Go To Record, or Create Record actions.</p>
        </li>
        <li>
          <p>Record must transition to another state after NetSuite saves the record.</p>
        </li>
        <li>
          <p>Some fields on the current record are set by NetSuite by other fields that are changed when the record is submitted.&nbsp;</p>
        </li>
        <li>
          <p>You want to perform other actions on an After Record Submit trigger that require you do computation and then store the result in the current record.</p>
        </li>
      </ul>
      <h3 id="bridgehead_4151397342">After Record Submit Trigger Example</h3>
      <p>The following screenshot shows actions for a state set to execute on the Before User Submit and After Record Submit triggers. The actions on the After Record Submit trigger require specific field values to be present in the database to execute.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteFlowWorkflow/AfterRecordSubmitTriggerExampleState.png" width="630px" height="185px">
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:113px;" class="bolight_bgdark" valign="top">
									<p>Trigger Type</p>
								</th>
            <th style="min-width:498px;" class="bolight_bgdark" valign="top">
									<p>Action Behavior</p>
								</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:113px;" class="bolight_bgwhite">
									<p>Before User Submit</p>
								</td>
            <td style="min-width:498px;" class="bolight_bgwhite">
									<p>Sets the <strong>Service Manager</strong> field on the record, which is required for one of the Send Email actions.</p>
								</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
									<p>After Record Submit</p>
								</td>
            <td class="bolight_bglight">
									<p>NetSuite sends both emails after NetSuite saves the record data to the database.</p>
									<p>The first Send Email action requires the Customer information from the record. The required data is present in the database after the record save.</p>
									<p>The second Send Email action requires the <strong>Service Manager</strong> field value, which is set before the record save by the Set Field Value action.</p>
								</td>
          </tr>
        </tbody>
      </table>
      <h3 id="bridgehead_4249843276">Comparing the Before Record Submit and After Record Submit Triggers</h3>
      <p>Although actions set to execute on the Before Record Submit and After Record Submit triggers both occur after the user clicks <strong>Save</strong> on a record, their use can affect edits made to the record.</p>
      <p>The following table describes the differences between the two triggers for the Set Field Value, Custom, Create Record, Initiate Workflow and Subscribe To Record actions:</p>
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:150px;" class="bolight_bgdark" valign="top">
									<p>Before Record Submit</p>
								</th>
            <th style="min-width:204px;" class="bolight_bgdark" valign="top">
									<p>After Record Submit</p>
								</th>
            <th style="min-width:248px;" class="bolight_bgdark" valign="top">
									<p>Other Considerations</p>
								</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:150px;" class="bolight_bgwhite">
									<p>All change made to the record by the workflow and the user are stored at the same time.</p>
								</td>
            <td style="min-width:204px;" class="bolight_bgwhite">
									<p>Any change made on the record by the workflow causes the record to be saved again when the workflow completes.</p>
								</td>
            <td style="min-width:248px;" class="bolight_bgwhite">
									<p>The Before Record Submit trigger results in better performance, for actions such as Set Field Value, as the record does not need to be potentially saved twice.</p>
								</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
									<p>If the workflow execution fails for any reason, NetSuite cancels the entire operation. Any edit made by user is lost.</p>
								</td>
            <td class="bolight_bglight">
									<p>If the workflow execution fails for any reason, the data entered by the user has already been saved. The only changes that are not saved are those made by the actions that execute on the After Record Submit trigger.</p>
								</td>
            <td class="bolight_bglight">
									<p>Use the Before Record Submit trigger for any actions, such as Set Field Value, that depend on record consistency.</p>
									<p>Modifications to the record made on the After Record Submit trigger are rolled back in case of failure, which may leave the record in an inconsistent state. The inconsistency results from necessary record changes not being made.</p>
								</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
									<p>Any business logic that executes when the record is stored has not executed yet, and can cause an inconsistent state of the record.</p>
								</td>
            <td class="bolight_bgwhite">
									<p>Any business logic that executes when the record is stored has already executed, resulting in a more consistent state for the record.</p>
								</td>
            <td class="bolight_bgwhite">
									<p>Typically, actions that use both trigger types modify the record but also may need to read the record, usually in an action condition.</p>
									<p>In this case, the After Record Submit trigger may be more effective.</p>
								</td>
          </tr>
        </tbody>
      </table>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Most other actions can use either of the above triggers, but not both.</p>
      </div>
      <br>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4103731715" href="./section_4103731715.html">Triggers Reference</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4150693781" href="./section_4150693781.html">Workflow Triggers Quick Reference</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4071954788" href="./section_4071954788.html#bridgehead_4074679460">Server Triggers</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4077993199" href="./section_4077993199.html">SuiteFlow Trigger Execution Model</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

