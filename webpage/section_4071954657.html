<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2723865" href="./book_N2723865.html">SuiteFlow (Workflow)</a>
            <a rev="chapter_4068260113" href="./chapter_4068260113.html">SuiteFlow Overview</a>
            <a rev="section_4071953058" href="./section_4071953058.html">Workflow Elements</a>
            <a class="nshelp_navlast" rev="section_4071954657" href="./section_4071954657.html">Workflow Event Types</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4071954657" class="nshelp_title">Workflow Event Types</h1>
      <p>You can define workflows to initiate or execute actions and transitions on specific events. You can direct workflows to initiate when records of the base record type are created, updated, or viewed with the <strong>On Create</strong> and <strong>On Update</strong> primary event types.</p>
      <p>SuiteFlow includes additional event types with the <strong>Event Type</strong> field on the workflow definition, action, and transition definition pages. These events represent the user interface activity used to create, view, or update the record for a workflow.</p>
      <p>Use these event types to further limit when workflows initiate or when actions and transitions execute. For a full list of all event types, see <a rev="section_4103763960" href="./section_4103763960.html">Event Types Reference</a>.</p>
      <p>The following screenshot shows the <strong>Event Type</strong> field on the workflow definition page:</p>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/SuiteFlowWorkflow/WorkflowEventTypes.png" width="588px" height="387px">
      <p>The Workflow Manager dynamically populates the event types you can select in the <strong>Event Type</strong> field. The following table describes the criteria for including an event type in the field:</p>
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:79px;" class="bolight_bgdark" valign="top">
								<p>Criteria</p>
							</th>
            <th style="min-width:532px;" class="bolight_bgdark" valign="top">
								<p>Description</p>
							</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:79px;" class="bolight_bgwhite">
								<p>Server trigger</p>
							</td>
            <td style="min-width:532px;" class="bolight_bgwhite">
								<p>The Workflow Manager displays different event types based on the server trigger, which includes Entry and Exit for actions and transitions in a state.</p>
								<p>For example, the Before Record Load trigger type and the <strong>View</strong> event type are compatible. The only time you can view a record is when it loads into the browser. However, if you set the workflow to initiate on the After Record Submit trigger, the <strong>View</strong> event type is not available as an event type.</p>
								<p>See <a rev="section_4103763960" href="./section_4103763960.html">Event Types Reference</a> for information on the relationship between server triggers and event types.</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Base record type</p>
							</td>
            <td class="bolight_bglight">
								<p>Some events are specific to a record type. For example, the Mark Complete event type will appear in the <strong>Event Type</strong> field only if the workflow base record type is Task or Phone Call.</p>
								<p>Other event types may appear depending on the features enabled in the account. For example, you must have the Pick, Pack, and Ship feature enabled to see Pack and Ship event types on an Item record such as Item Fulfillment.</p>
							</td>
          </tr>
        </tbody>
      </table>
      <h3 id="bridgehead_4080861479">Rules and Guidelines for Event Types</h3>
      <p>Use the following general rules and guidelines with event types:</p>
      <ul class="nshelp">
        <li>
          <p>The <strong>Event Type</strong> field is not available for actions that execute on a client trigger or the Scheduled trigger for workflow initiation or action or transition execution.</p>
        </li>
        <li>
          <p>The Workflow Manager does not limit all incompatible relationships between <strong>On Create</strong> and <strong>On Update</strong> and the corresponding events in the <strong>Event Type</strong> field.</p>
          <p>For example, you can use the View event type for workflow initiation. However, view event only occurs if the record opens in view (read-only) mode, rather than in edit mode. Consequently, if you define the workflow to only initiate on the creation of a record and select the View event type, the view event never occurs and the workflow never initiates.</p>
        </li>
      </ul>
      <h3 id="bridgehead_4080818079">More Information About Event Types</h3>
      <p>Use the following table to get more information about working with event types in a workflow:</p>
      <table class="alternate_rows" style="min-width:541px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:343px;" class="bolight_bgdark" valign="top">
								<p>Task</p>
							</th>
            <th style="min-width:180px;" class="bolight_bgdark" valign="top">
								<p>For more information, see ...</p>
							</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:343px;" class="bolight_bgwhite">
								<p>Specify the event type in a workflow definition</p>
							</td>
            <td style="min-width:180px;" class="bolight_bgwhite">
								<p><a rev="section_4101527388" href="./section_4101527388.html">Creating a Workflow</a></p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Specify the event type in an action or transition</p>
							</td>
            <td class="bolight_bglight">
								<p><a rev="section_4103049209" href="./section_4103049209.html">Creating an Action</a>, <a rev="section_4103065793" href="./section_4103065793.html">Creating a Transition</a></p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Get a list of all event types and when they are available</p>
							</td>
            <td class="bolight_bgwhite">
								<p><a rev="section_4103763960" href="./section_4103763960.html">Event Types Reference</a></p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Get examples of using the Print, Mark Complete, and Drop Ship event types</p>
							</td>
            <td class="bolight_bglight">
								<p><a rev="section_4103803818" href="./section_4103803818.html">Event Type Examples</a></p>
							</td>
          </tr>
        </tbody>
      </table>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4068260113" href="./chapter_4068260113.html">SuiteFlow Overview</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4071953058" href="./section_4071953058.html">Workflow Elements</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4103763960" href="./section_4103763960.html">Event Types Reference</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4103803818" href="./section_4103803818.html">Event Type Examples</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

