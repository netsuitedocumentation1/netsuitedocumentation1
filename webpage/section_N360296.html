<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N125873" href="./set_N125873.html">Account Administration</a>
            <a rev="book_4470700566" href="./book_4470700566.html">CSV Imports</a>
            <a rev="chapter_N356211" href="./chapter_N356211.html">Guidelines for CSV Import Files</a>
            <a rev="section_N359586" href="./section_N359586.html">Activities Import Type</a>
            <a class="nshelp_navlast" rev="section_N360296" href="./section_N360296.html">Tasks Import</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N360296" class="nshelp_title">Tasks Import</h1>
      <p>A task record stores details about a unit of work to be completed. NetSuite supports the creation of two different types of tasks: CRM Tasks and Project Tasks. The Tasks import supports the import of CRM tasks, not project tasks. CRM tasks, usually labeled Tasks in the NetSuite user interface, do not have to be associated with projects.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Project tasks are available in accounts where the Project Management feature is enabled, and are used only in association with project records. A different import process for project tasks is available at <span id="dyn_help_taskpath" dynid="LIST_PROJECTTASKIMPORT">&nbsp;</span>. See <a rev="section_N438898" href="./section_N438898.html">Project Tasks Import</a>.</p>
      </div>
      <br>
      <p>The CRM tasks data that you can import depend on the fields available on either your preferred Task form, or the custom task form selected on the Import Assistant Import Options page. For details about specifying a custom form, see <a rev="section_N345887" href="./section_N345887.html">Set Advanced CSV Import Options</a>.</p>
      <p>For details about fields that can be mapped in the Task record, see the Schema Browser’s <a href="../../help/helpcenter/en_US/srbrowser/Browser2016_2/schema/record/task.html" target="_blank">task</a> reference page. You can use the field definitions here as a basis for creating your own CSV import template file. For information about working with the Schema Browser, see <a rev="section_N3639052" href="./section_N3639052.html">Working with the SuiteTalk Schema Browser</a>.</p>
      <p>Be aware of the following:</p>
      <ul class="nshelp">
        <li>
          <p>Required task fields for import are: Assigned To, Due Date, Priority, Start Date, Status, and Title.</p>
        </li>
        <li>
          <p>You need to understand the use of the Start Date and End Date fields on task records, and to exercise care when you format values for the Start Date, End Date, and Due Date fields in your CSV files. To avoid errors, review and follow the guidelines in <a rev="section_N360296" href="#bridgehead_N360596">Setting Date Field Values for Tasks Imports</a>.</p>
        </li>
        <li>
          <p>Unlike in the user interface, the Assigned To field does not default to be the logged in user. You need to include values for this field in your CSV file or set up a default on the Import Assistant Field Mapping page. See <a rev="section_N349918" href="./section_N349918.html">Assigning Default Values during Field Mapping</a>.</p>
        </li>
        <li>
          <p>The Send Email field is not available for import mapping. Imports cannot send email to task assignees.</p>
        </li>
        <li>
          <p>The Tasks import supports the import of the following sublist data:</p>
        </li>
      </ul>
      <table class="grid" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:84px;" class="bolight_bgwhite">
								<p>Sublist</p>
							</th>
            <th style="min-width:527px;" class="bolight_bgwhite">
								<p>Notes</p>
							</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:84px;" class="bolight_bgwhite">
								<p>Contacts</p>
							</td>
            <td style="min-width:527px;" class="bolight_bgwhite">
								<p>Selectively updatable based on Company/Project(Entity) or Contact key field.</p>
								<p>Maps to Companies and Contacts subtab on Related Records subtab of Task record, can include other records, such as customers and contacts, that are related to each task.</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Time Tracking</p>
							</td>
            <td class="bolight_bgwhite">
								<p>When Time Tracking feature is enabled.</p>
							</td>
          </tr>
        </tbody>
      </table>
      <p class="indent_1">For more information, see <a rev="section_N439392" href="./section_N439392.html">Importing Sublist Data</a>.</p>
      <ul class="nshelp">
        <li>
          <p>If you are doing a tasks import for the addition of new tasks, select the <strong>Add</strong> data handling option, rather than <strong>Add or Update</strong>.</p>
        </li>
        <li>
          <p>If you need to use an import to update existing task records, you must map either the Internal ID or External ID field to uniquely identify records. This requirement is enforced when you select the <strong>Update</strong> data handling option and when you select the <strong>Add or Update</strong> data handling option. The Title field is not a key field and does not have to be unique.</p>
        </li>
      </ul>
      <h3 id="bridgehead_N360596">Setting Date Field Values for Tasks Imports</h3>
      <p>Before you set up a CSV file for a tasks import, you need to understand how Start Date and End Date fields are used, and you need to be aware of formatting requirements for Start Date, End Date, and Due Date field values.</p>
      <h4 id="bridgehead_3705040959">Understanding Start Date and End Date Fields</h4>
      <p>You can review the standard Task form to understand the use of the Start Date and End Date fields in task records, to properly specify their values in your CSV files.</p>
      <ul class="nshelp">
        <li>
          <p>Reserve Time - Indicates whether to schedule time for the task on the assignee's calendar. How the Start Date and End Date fields are used depends upon whether Reserve Time is enabled (set to True). By default, Reserve Time is not enabled (set to False).</p>
        </li>
        <li>
          <p>Start Date - The date when a task is started, as shown in the field labeled <strong>Start Date</strong> on the standard Task form, number 1 in the following screenshot. If Reserve Time is enabled, the Start Date value should also include a time that is the beginning of reserved time on the assignee's calendar, as shown in the field labeled <strong>Start Time</strong>, number 2 in the following screenshot.</p>
        </li>
        <li>
          <p>End Date - If Reserve Time is enabled, the time that is the end of reserved time on the assignee's calendar, as shown in the field labeled <strong>End Time</strong> on the standard Task form, number 3 in the following screenshot. The date is not shown, and is assumed to be the same as the date in the Start Date field.</p>
        </li>
      </ul>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/AccountAdministration/CSVImports/TaskDateFields_2014_2.png" width="630px" height="304px">
      <h4 id="bridgehead_3705041487">Formatting Guidelines for Task Date Field Values</h4>
      <p>Follow these guidelines when you enter values for date fields in CSV files used for tasks imports.</p>
      <ul class="nshelp">
        <li>
          <p>For Start Date fields:</p>
          <ul class="nshelp">
            <li>
              <p>If Reserve Time is set to False, a Start Date value requires only a date, for example: <strong>04/12/2010</strong>. If a time is included, it is ignored.</p>
            </li>
            <li>
              <p>If Reserve Time is set to True, Start Date requires both a date and a time, for example: <strong>04/12/2010 09:00 AM</strong>.</p>
            </li>
          </ul>
        </li>
        <li>
          <p>For End Date fields:</p>
          <ul class="nshelp">
            <li>
              <p>If Reserve Time is set to False, no End Date value is required.</p>
            </li>
            <li>
              <p>If Reserve Time is set to True, End Date requires both a date, that should be the same as the date set for Start Date, and a time, for example: <strong>04/12/2010 11:00 AM</strong>.</p>
            </li>
          </ul>
        </li>
        <li>
          <p>For Start Date, End Date, and Due Date fields, values should be specified according to the NetSuite Date Format, and if applicable, Time Format, set at <span id="dyn_help_taskpath" dynid="TRAN_USERPREFS">&nbsp;</span> for the user doing the import. The following are example formats:</p>
          <ul class="nshelp">
            <li>
              <p>Date Format is <strong>MM/DD/YYYY</strong>.</p>
            </li>
            <li>
              <p>Time Format is <strong>hh:mm AM/PM</strong>.</p>
            </li>
            <li>
              <p>For a Start Date or End Date field that includes both date and time, a space should be included between them, for example: <strong>04/12/2010 10:00 AM</strong>.</p>
            </li>
          </ul>
        </li>
      </ul>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">If your tasks import returns date-related errors such as “You have entered an Invalid Field Value” or “Due Date occurs before Start Date”, try removing leading zeros from date values in your CSV file, for example changing <strong>08/01/2010</strong> to <strong>8/1/2010</strong>.</p>
      </div>
      <br>
      <ul class="nshelp">
        <li>
          <p>To avoid Excel formatting errors for Start Date, End Date, and Due Date values, NetSuite recommends that you right-click the column and choose Format Cells to explicitly select a format that matches the Date Format (and if applicable, Time Format) set up in NetSuite.</p>
          <img class="nshelp_composite" src="/help/helpcenter/en_US/Output/Help/images/AccountAdministration/CSVImports/TaskDateFormats.png" width="524px" height="471px">
        </li>
      </ul>
      <p>The Import Assistant is available at&nbsp;<span id="dyn_help_taskpath" dynid="ADMI_IMPORTCSV">&nbsp;</span>. After you select the record type for import, you choose the import character encoding. For more information, see <a rev="section_N344030" href="./section_N344030.html">Select a Record Type for Import</a> and <a rev="section_N344158" href="./section_N344158.html">Choose Import Character Encoding</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Additional Information</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N506499" href="./section_N506499.html">Working with CRM Tasks</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N359586" href="./section_N359586.html">Activities Import Type</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_3743314536" href="./section_3743314536.html">Events Import</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N359696" href="./section_N359696.html">Phone Calls Import</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

