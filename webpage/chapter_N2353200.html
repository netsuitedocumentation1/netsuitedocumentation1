<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2249433" href="./book_N2249433.html">Inventory Management</a>
            <a class="nshelp_navlast" rev="chapter_N2353200" href="./chapter_N2353200.html">Inventory Reporting</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="chapter_N2353200" class="nshelp_title">Inventory Reporting</h1>
      <p><span id="dyn_help_feature" dynid="INVENTORY">&nbsp;</span>   </p>
      <p>NetSuite reports are a powerful tool you can use to assess inventory at all stages of your inventory workflow.</p>
      <p>You can view reports with inventory costs, stock levels, activity, valuation, revenue generated and profitability. Then, use the information from reports to identify successes and problems within each workflow process, as well as within the workflow as a whole.</p>
      <p>For example, you can view reports that return this inventory data for the previous quarter:</p>
      <ul class="nshelp">
        <li>
          <p>inventory valuation rises each week</p>
        </li>
        <li>
          <p>item sales revenue decreases each week</p>
        </li>
        <li>
          <p>gross profitability of your top seller remains level</p>
        </li>
      </ul>
      <p>With this information, you can take action as necessary -- changing processes that have created problems and maintaining processes that are successful:</p>
      <ul class="nshelp">
        <li>
          <p>High inventory values increase overhead and may cut your profitability.</p>
          <p>You can investigate items that have a high percentage of total inventory value. Based on their costs and turnover rates, you may need to change your purchasing practices.</p>
        </li>
        <li>
          <p>Sales revenue decreases can result from customer dissatisfaction caused by difficulties and delays during fulfillment.</p>
          <p>You can investigate items that are frequently backordered and adjust their set reorder points and preferred stock levels.</p>
        </li>
        <li>
          <p>A steady profitability on a popular item is a positive indication.</p>
          <p>You can maintain the appropriate stock level and price point currently set on the item record. Because it is not problematic, you can focus your attention elsewhere.</p>
        </li>
      </ul>
      <p>When you can take action to make improvements as needed, you can execute more efficient management of your inventory.</p>
      <p>It is important to track your inventory so you know what stock you have and what stock you need. Keeping the appropriate levels of stock on hand enables you to maintain a streamlined stock level, which results in reduced overhead. It also enables you to fulfill orders in a timely manner, which results in customer satisfaction.</p>
      <p>You can use inventory reports to make regular assessments of stock quantities and identify your replenishment needs to maintain a streamlined inventory, as well as evaluate your overall inventory workflow.</p>
      <p>First, determine the status of your inventory and identify your replenishment needs. Then, you can maintain optimum stock levels by purchasing or redistributing inventory as needed to maintain the ideal quantities.</p>
      <p>You can use reports to assess your inventory and workflow. Reports can help you identify:</p>
      <ul class="nshelp">
        <li>
          <p>current stock status</p>
        </li>
        <li>
          <p>items that are profitable or problematic</p>
        </li>
        <li>
          <p>areas of the inventory workflow that are beneficial or detrimental</p>
        </li>
      </ul>
      <p>Then, you can respond to information in the reports by:</p>
      <ul class="nshelp">
        <li>
          <p>modifying replenishment to streamline stock levels</p>
        </li>
        <li>
          <p>making changes to correct problems in the workflow</p>
        </li>
      </ul>
      <p>To view reports, go to the Reports tab. Inventory reports can be accessed under the Inventory/Items heading.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Links to all reports may not show on the Reports tab. The reports that are available depend on the role assigned to you. For example, an Administrator has access to more reports than a Sales Manager.</p>
      </div>
      <br>
      <p>Reports can show current status of inventory items and backorders to help you find out what you need.</p>
      <p>Inventory assessment can have two main goals: to determine your inventory level or to determine your inventory values. Assessing the level of your inventory tells you how much inventory you have. Assessing the value tells you how much your inventory is worth.</p>
      <p>One report can help you make both assessments: <a rev="section_4521145431" href="./section_4521145431.html">Stock Ledger</a>.</p>
      <p>Other inventory reports can be categorized by two main functions—inventory level assessment and inventory value assessment.</p>
      <h3 id="procedure_N2353334">Inventory Levels</h3>
      <p>You can make an <a rev="section_N2354076" href="./section_N2354076.html">Inventory Level Assessments with Reports</a> to determine if the quantities you are stocking on hand are the appropriate amounts.</p>
      <ul class="nshelp">
        <li>
          <p>The <a rev="section_4521145431" href="./section_4521145431.html">Stock Ledger</a> shows activity for items across a period. It is a tool for better understanding inventory SKU activity across classes, departments, and subsidiaries for multiple locations. For example, it can help identify inventory items that are slow-moving, fast-moving, or overstocked. It can help identify inventory investment strategies for planning.</p>
        </li>
        <li>
          <p>The <a rev="section_N2354418" href="./section_N2354418.html">Physical Inventory Worksheet</a> helps you take a physical count of your stock to be sure it matches the quantity in NetSuite.</p>
        </li>
        <li>
          <p>Use the <a rev="section_N2355248" href="./section_N2355248.html">Current Inventory Status Report</a> to monitor inventory levels and determine ordering schedules. Determine a reorder point and preferred stock level, or identify overstocking on items.</p>
        </li>
        <li>
          <p>Use the <a rev="section_N2355658" href="./section_N2355658.html">Inventory Back Order Report</a> to find items that are committed on sales orders, but are not in stock, as well as analyze the replenishment process of your workflow. For items repeatedly on backorder, you may consider increasing the reorder point for that item.</p>
        </li>
        <li>
          <p>The <a rev="section_N2355966" href="./section_N2355966.html">Inventory Activity Detail Report</a> shows the quantity per transaction of inventory items. You can use this report to identify trends in sales for items and make informed decisions about stocking your items.</p>
        </li>
        <li>
          <p>Items that are pending fulfillment are committed to be sold, are in stock and are ready to ship to the customer. The <a rev="section_N2356254" href="./section_N2356254.html">Items Pending Fulfillment Report</a> shows all open transaction lines on sales orders and can help you to process items to fulfill orders.</p>
        </li>
        <li>
          <p>You need to know how quickly each product moves through inventory because it is not efficient to have large quantities of inventory sitting idle on your shelves. Inventory turnover measures item efficiency by examining how quickly you sell a product. The <a rev="section_N2356560" href="./section_N2356560.html">Inventory Turnover Report</a> helps you assess stock level changes over time. This report lists the cost of sales, average value, turnover rate (or turn rate) and average days on hand for each inventory item.</p>
        </li>
      </ul>
      <h3 id="procedure_N2353452">Inventory Values</h3>
      <p><a rev="section_N2358468" href="./section_N2358468.html">Inventory Value Assessments with Reports</a> help you identify inventory value and profitability, as well as revenue generated by items.</p>
      <ul class="nshelp">
        <li>
          <p>The <a rev="section_4521145431" href="./section_4521145431.html">Stock Ledger</a> shows activity for items across a period. It is a tool for better understanding inventory SKU activity across classes, departments, and subsidiaries for multiple locations. For example, it can help identify inventory items that are slow-moving, fast-moving, or overstocked. It can help identify inventory investment strategies for planning.</p>
        </li>
        <li>
          <p>The <a rev="section_N2358569" href="./section_N2358569.html">Inventory Profitability Report</a> shows cost, revenue, and profitability information about your inventory.You can view this report to determine margins on your items and identify items that are most and least profitable. If you operate an online store with NetSuite, this report includes sales from your online store.</p>
        </li>
        <li>
          <p>The <a rev="section_N2358933" href="./section_N2358933.html">Inventory Valuation Report</a> summarizes the value of your inventory at a specific point in time and can help you identify areas you can potentially use to streamline your inventory. For each inventory item, this report details item name, description, inventory value (in dollars), percentage of total inventory value and quantity on hand.</p>
        </li>
        <li>
          <p>An <a rev="section_N2359248" href="./section_N2359248.html">Inventory Valuation Detail Report</a> shows the transactions affecting the value of your inventory. This report is useful for troubleshooting costing problems with your items because it lists transactions that affect inventory assets.</p>
        </li>
        <li>
          <p><a rev="section_N2359520" href="./section_N2359520.html">Inventory Revenue Report</a> An Inventory Revenue report shows the total sales amounts for inventory items and overall revenue from your inventory items. You can identify an item that has low sales and is a poor contributor to your revenue total and may need to be discontinued. Pinpoint items that have high sales and are good contributors to your revenue total so you can maintain the stock, marketing, and pricing of these items.</p>
          <p>If you operate an online store with NetSuite, these reports will include any sales from your store.</p>
        </li>
        <li>
          <p>An <a rev="section_N2359826" href="./section_N2359826.html">Inventory Revenue Detail Report</a> shows the revenue generated by your inventory items for a particular period by sales transaction. You can use this information to determine which items are successful items by finding items that generate high revenue and have a high gross profit percentage.</p>
          <p>If you operate an online store with NetSuite, these reports include sales from your store.</p>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics:</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2250682" href="./chapter_N2250682.html">Inventory Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2268753" href="./section_N2268753.html">Locations and Inventory Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2317586" href="./section_N2317586.html">Warehouse Processing</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2270284" href="./section_N2270284.html">Bin Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2281204" href="./section_N2281204.html">Selling and Fulfilling Inventory</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2285050" href="./chapter_N2285050.html">Advanced Inventory Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2019313" href="./section_N2019313.html#bridgehead_N2019609">Multi-Location Inventory</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2353200" href="./chapter_N2353200.html">Inventory Reporting</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

