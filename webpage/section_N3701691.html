<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3412393" href="./book_N3412393.html">SuiteTalk (Web Services)</a>
            <a rev="part_N3634544" href="./part_N3634544.html">SuiteTalk (Web Services) Records Guide</a>
            <a rev="chapter_N3657735" href="./chapter_N3657735.html">Transactions</a>
            <a class="nshelp_navlast" rev="section_N3701691" href="./section_N3701691.html">Work Order Completion</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N3701691" class="nshelp_title">Work Order Completion</h1>
      <p>If the Manufacturing Work In Process (WIP) feature has been enabled, you can use SuiteTalk to interact with work order completion records. You can check to see if WIP is enabled by going to Setup &gt; Company &gt; Enable Features, and reviewing the Items &amp; Inventory tab.</p>
      <p>With WIP, instead of creating a single assembly build record to denote that a work order has been addressed, you track progress of the work using three records: work order issue, work order completion, and work order close. This approach lets you manage the assembly process in a more granular way, and to keep the General Ledger up to date as materials move through the different phases of assembly. For more on the benefits of WIP, refer to <a rev="section_N2335392" href="./section_N2335392.html">Manufacturing Work In Process (WIP)</a>.</p>
      <p>The work order completion record is used to indicate that assemblies have been built. You can also optionally use this record to record that raw materials — items in the componentList sublist — have been consumed as part of the assembly process. This latter option is called entering a completion with backflush. For example, you might enter a completion with backflush if previous records (such as work order issue) did not record the consumption of all the materials you ended up using.</p>
      <p>In the UI, you can view the form used for creating the work order completion record by choosing Transactions &gt; Manufacturing &gt; Enter Completions, selecting a Subsidiary (for OneWorld accounts), then clicking the Complete link that corresponds with one of the listed work orders. An alternate method is to view the work order and click one of two buttons: Enter Completions or Enter Completions With Backflush. For help filling out the form manually, refer to <a rev="section_N2339352" href="./section_N2339352.html">Entering Work Order Completions</a>.</p>
      <p>The work order completion record is defined in the <a href="https://webservices.netsuite.com/xsd/transactions/v2016_2_0/inventory.xsd" target="_blank">tranInvt (inventory)</a> XSD.</p>
      <h2 id="bridgehead_N3701747">Supported Operations</h2>
      <p>The following operations can be used with work order completion records:</p>
      <p><a rev="section_N3480855" href="./section_N3480855.html">add</a> | <a rev="section_N3481360" href="./section_N3481360.html">addList</a> | <a rev="section_N3481947" href="./section_N3481947.html">attach / detach</a> | <a rev="section_N3486046" href="./section_N3486046.html">delete</a> | <a rev="section_N3486552" href="./section_N3486552.html">deleteList</a> | <a rev="section_N3488543" href="./section_N3488543.html">get</a> | <a rev="section_N3497592" href="./section_N3497592.html">getDeleted</a> | <a rev="section_N3499748" href="./section_N3499748.html">getList</a> | <a rev="section_N3503649" href="./section_N3503649.html">getSavedSearch</a> | <a rev="section_N3508536" href="./section_N3508536.html">initialize / initializeList</a> | <a rev="section_N3514306" href="./section_N3514306.html">search</a> | <a rev="section_N3522244" href="./section_N3522244.html">searchMore</a> | <a rev="section_N3524037" href="./section_N3524037.html">searchNext</a> | <a rev="section_N3527090" href="./section_N3527090.html">update</a> | <a rev="section_N3527652" href="./section_N3527652.html">updateList</a> | <a rev="section_N3532463" href="./section_N3532463.html">upsert</a> | <a rev="section_N3533243" href="./section_N3533243.html">upsertList</a></p>
      <h2 id="bridgehead_N3702009">Field Definitions</h2>
      <p>The SuiteTalk Schema Browser includes definitions for all body fields, sublist fields, search filters, and search joins available to this record. For details, see the Schema Browser’s <a href="../../help/helpcenter/en_US/srbrowser/Browser2016_2/schema/record/workordercompletion.html" target="_blank">work order completion</a> reference page.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">For information on using the SuiteTalk Schema Browser, see <a rev="section_N3639052" href="./section_N3639052.html">Working with the SuiteTalk Schema Browser</a>.</p>
      </div>
      <br>
      <h2 id="bridgehead_N3702047">Usage Notes</h2>
      <p>Refer to the following sections for more details on interacting with work order completion records.</p>
      <h3 id="bridgehead_N3702059">Prerequisites for Creating a Record</h3>
      <p>Before you can create a work order completion record, a work order record must already exist, and the work order must be configured to use WIP (the WIP box on the work order record must be selected). This is true regardless of whether you are creating the work order issue record using initialize and add, or add by itself. If you try to create a work order issue record referencing a work order that has <em>not</em> been configured to use WIP, the system generates an error reading in part, “One of the following problems exists: You have an invalid work order &lt; <em>work order ID</em> &gt;, the work order does not use WIP, or the work order is already closed.”</p>
      <p>You can create work orders by choosing Transactions &gt; Manufacturing &gt; Enter Work Orders. You can also interact with work orders using web services, as described in <a rev="section_N3697954" href="./section_N3697954.html">Work Order</a>.</p>
      <p>Further, the assembly item referenced in the work order must be properly set up for WIP. For details on this process, see <a rev="section_N2335987" href="./section_N2335987.html">Setting Up Items as WIP Assemblies</a>.</p>
      <p>Note that it does not matter if a work order issue record already exists for your work order. You can go straight from entering the work order to creating the work order completion record.</p>
      <h3 id="bridgehead_N3702105">Using Initialize Versus Add</h3>
      <p>You can initialize a work order completion record from a <a rev="section_N3697954" href="./section_N3697954.html">Work Order</a> record. This approach is the recommended one, though you can also create the record using the add operation by itself.</p>
      <h3 id="procedure_N3702127">Using Both Initialize and Add</h3>
      <p>The initialize operation emulates the UI workflow by prepopulating fields on transaction line items with values from a related record. For more information about this operation, see <a rev="section_N3508536" href="./section_N3508536.html">initialize / initializeList</a>.</p>
      <p>Note that when you use the initialize operation, certain required fields are not initialized and must be set manually. These fields vary depending on the work order that you are using, as follows:</p>
      <ul class="nshelp">
        <li>
          <p>If the work order uses routing, the startOperation and endOperation fields are both required (but are not initialized). These fields denote which operation tasks were completed.</p>
        </li>
        <li>
          <p>If the work order does not use routing, the quantity field is required (but is not initialized). The quantity element denotes the total number of assembly items that were completed in the build process. Note that you cannot update this field to a value greater than the value called for in the work order. Attempting to set the value too high results in an error.</p>
        </li>
      </ul>
      <p>In each case, you must manually set the required values in a statement between the initialize and add statements. To see an example, refer to <a rev="section_N3701691" href="#bridgehead_N3702265">Using Initialize and Setting isBackflush to False</a>.</p>
      <h3 id="bridgehead_N3702181">Using Add</h3>
      <p>If you are using the add operation by itself, note that you must use the createdFrom field to identify the appropriate work order. If you fail to set a value for this field, the system generates an error reading, “Transaction can only be created from a work order.”</p>
      <h3 id="bridgehead_N3702193">Working with the OperationList Sublist</h3>
      <p>When routing is used, an additional sublist, operationList, is available. If the original work order does not use routing, the operationList sublist is unavailable.</p>
      <h3 id="bridgehead_N3702213">Working with the ComponentList Sublist</h3>
      <p>When working with the componentList sublist, note that the same general restrictions apply as when you are using the work order issue record. For details, see <a rev="section_N3698631" href="./section_N3698631.html#bridgehead_N3699067">Working with the ComponentList Sublist</a>.</p>
      <p>In addition, note that, with work order completion, you can interact with the componentList sublist <em>only</em> if the isBackflush element is set to true.</p>
      <p>Refer also to <a rev="section_N3698631" href="./section_N3698631.html#bridgehead_N3699091">Using ReplaceAll</a> for details on the validation that occurs when replaceAll is set to true.</p>
      <h3 id="bridgehead_N3702253">Sample Code</h3>
      <p>The following code illustrates how to create a work order completion record using a few different techniques.</p>
      <h4 id="bridgehead_N3702265">Using Initialize and Setting isBackflush to False</h4>
      <p>The recommended strategy of creating a work order completion record is to use the initialize operation. Note that this approach generates two sets of SOAP requests and responses — one for the initialize operation and one for the add operation. This example shows how to use initialize to create a work order completion with isBackflush set to false.</p>
      <p>Note that isBackflush is set to false by default. To include quantity values for items on the componentList, you would have to set isBackflush to true and specify the quantity values for the component items. (To see an example that sets isBackflush to true, see <a rev="section_N3701691" href="#bridgehead_N3702356">Using Add and Setting isBackflush to True</a>.)</p>
      <p>Further, note that in this example, the work order being referenced does not use routing. For this reason, the quantity value is required — and because the value is not initialized, it must be manually set. You should set the quantity value between the initialize and add statements.</p>
      <h5 id="bridgehead_N28571081">Java</h5>
      <pre class="nshelp">InitializeRef initRef = new InitializeRef();
initRef.setType(InitializeRefType.workOrder);
initRef.setInternalId("167");
 
WorkOrderCompletion woRecord = (WorkOrderCompletion) c.initialize(initRef,InitializeType.workOrderCompletion,null);
 
woRecord.setQuantity(3.0);
 
c.addRecord(woRecord);</pre>
      <h5 id="bridgehead_N28571141">SOAP Request (Initialize)</h5>
      <pre class="nshelp">&lt;soapenv:Body&gt;  
   &lt;initialize xmlns="urn:messages_2013_1.platform.webservices.netsuite.com"&gt;   
      &lt;initializeRecord&gt;    
         &lt;ns7:type xmlns:ns7="urn:core_2013_1.platform.webservices.netsuite.com"&gt;workOrderCompletion&lt;/ns7:type&gt;    
         &lt;ns8:reference internalId="167" type="workOrder" xmlns:ns8="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;   
      &lt;/initializeRecord&gt;  
   &lt;/initialize&gt; 
&lt;/soapenv:Body&gt;</pre>
      <h5 id="bridgehead_N28571201">SOAP Response (Initialize)</h5>
      <pre class="nshelp">&lt;soapenv:Body&gt;
   &lt;initializeResponse xmlns="urn:messages_2013_1.platform.webservices.netsuite.com"&gt;
      &lt;readResponse&gt;
         &lt;platformCore:status isSuccess="true" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;
         &lt;record xsi:type="tranInvt:WorkOrderCompletion" xmlns:tranInvt="urn:inventory_2013_1.transactions.webservices.netsuite.com"&gt;
            &lt;tranInvt:createdDate&gt;2013-03-07T07:10:00.000-08:00&lt;/tranInvt:createdDate&gt;
            &lt;tranInvt:lastModifiedDate&gt;2013-03-07T08:14:00.000-08:00&lt;/tranInvt:lastModifiedDate&gt;
            &lt;tranInvt:tranId&gt;2&lt;/tranInvt:tranId&gt;
            &lt;tranInvt:item internalId="247" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"&gt;
               &lt;platformCore:name&gt;JS Assembly Item Y&lt;/platformCore:name&gt;
            &lt;/tranInvt:item&gt;
            &lt;tranInvt:isBackflush&gt;false&lt;/tranInvt:isBackflush&gt;
            &lt;tranInvt:orderQuantity&gt;5.0&lt;/tranInvt:orderQuantity&gt;
            &lt;tranInvt:createdFrom internalId="167" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"&gt;
               &lt;platformCore:name&gt;Work Order #1&lt;/platformCore:name&gt;
            &lt;/tranInvt:createdFrom&gt;
            &lt;tranInvt:tranDate&gt;2013-03-07T00:00:00.000-08:00&lt;/tranInvt:tranDate&gt;
            &lt;tranInvt:postingPeriod internalId="141" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"&gt;
               &lt;platformCore:name&gt;Mar 2013&lt;/platformCore:name&gt;
            &lt;/tranInvt:postingPeriod&gt;
            &lt;tranInvt:subsidiary internalId="3" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"&gt;
               &lt;platformCore:name&gt;SUB  UK&lt;/platformCore:name&gt;
            &lt;/tranInvt:subsidiary&gt;
            &lt;tranInvt:location internalId="2" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"&gt;
               &lt;platformCore:name&gt;Location UK&lt;/platformCore:name&gt;
      &lt;/tranInvt:location&gt;
            &lt;tranInvt:componentList&gt;
               &lt;tranInvt:workOrderCompletionComponent&gt;
                  &lt;tranInvt:item internalId="245" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;
                  &lt;tranInvt:quantityPer&gt;5.0&lt;/tranInvt:quantityPer&gt;
                  &lt;tranInvt:lineNumber&gt;5&lt;/tranInvt:lineNumber&gt;
               &lt;/tranInvt:workOrderCompletionComponent&gt;
                  &lt;tranInvt:workOrderCompletionComponent&gt;
                  &lt;tranInvt:item internalId="246" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;
                  &lt;tranInvt:quantityPer&gt;6.0&lt;/tranInvt:quantityPer&gt;
                  &lt;tranInvt:lineNumber&gt;6&lt;/tranInvt:lineNumber&gt;
               &lt;/tranInvt:workOrderCompletionComponent&gt;
            &lt;/tranInvt:componentList&gt;
            &lt;tranInvt:customFieldList xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"&gt;
               &lt;platformCore:customField internalId="65" scriptId="custbody_633637_bsubmit" xsi:type="platformCore:BooleanCustomFieldRef"&gt;
                  &lt;platformCore:value&gt;false&lt;/platformCore:value&gt;
               &lt;/platformCore:customField&gt;
               &lt;platformCore:customField internalId="215" scriptId="custbody_633637_bload" xsi:type="platformCore:BooleanCustomFieldRef"&gt;
                  &lt;platformCore:value&gt;false&lt;/platformCore:value&gt;
               &lt;/platformCore:customField&gt;
               &lt;platformCore:customField internalId="53" scriptId="custbody_633637_asubmit" xsi:type="platformCore:BooleanCustomFieldRef"&gt;
                  &lt;platformCore:value&gt;false&lt;/platformCore:value&gt;
               &lt;/platformCore:customField&gt;
            &lt;/tranInvt:customFieldList&gt;
         &lt;/record&gt;
      &lt;/readResponse&gt;
   &lt;/initializeResponse&gt;
&lt;/soapenv:Body&gt;</pre>
      <h5 id="bridgehead_N28571261">SOAP Request (Add)</h5>
      <pre class="nshelp">&lt;soapenv:Body&gt;  
   &lt;add xmlns="urn:messages_2013_1.platform.webservices.netsuite.com"&gt;   
      &lt;record xsi:type="ns7:WorkOrderCompletion" xmlns:ns7="urn:inventory_2013_1.transactions.webservices.netsuite.com"&gt;    
         &lt;ns7:createdDate xsi:type="xsd:dateTime"&gt;2013-03-07T15:10:00.000Z&lt;/ns7:createdDate&gt;    
         &lt;ns7:lastModifiedDate xsi:type="xsd:dateTime"&gt;2013-03-07T16:14:00.000Z&lt;/ns7:lastModifiedDate&gt;    
         &lt;ns7:tranId xsi:type="xsd:string"&gt;2&lt;/ns7:tranId&gt;    
         &lt;ns7:item internalId="247" xsi:type="ns8:RecordRef" xmlns:ns8="urn:core_2013_1.platform.webservices.netsuite.com"&gt;     
            &lt;ns8:name xsi:type="xsd:string"&gt;JS Assembly Item Y&lt;/ns8:name&gt;    
         &lt;/ns7:item&gt;    
         &lt;ns7:quantity xsi:type="xsd:double"&gt;3.0&lt;/ns7:quantity&gt;    
         &lt;ns7:isBackflush xsi:type="xsd:boolean"&gt;false&lt;/ns7:isBackflush&gt;    
         &lt;ns7:orderQuantity xsi:type="xsd:double"&gt;5.0&lt;/ns7:orderQuantity&gt;    
         &lt;ns7:createdFrom internalId="167" xsi:type="ns9:RecordRef" xmlns:ns9="urn:core_2013_1.platform.webservices.netsuite.com"&gt;     
            &lt;ns9:name xsi:type="xsd:string"&gt;Work Order #1&lt;/ns9:name&gt;    
         &lt;/ns7:createdFrom&gt;    
         &lt;ns7:tranDate xsi:type="xsd:dateTime"&gt;2013-03-07T08:00:00.000Z&lt;/ns7:tranDate&gt;    
         &lt;ns7:postingPeriod internalId="141" xsi:type="ns10:RecordRef" xmlns:ns10="urn:core_2013_1.platform.webservices.netsuite.com"&gt;     
            &lt;ns10:name xsi:type="xsd:string"&gt;Mar 2013&lt;/ns10:name&gt;    
         &lt;/ns7:postingPeriod&gt;    
         &lt;ns7:subsidiary internalId="3" xsi:type="ns11:RecordRef" xmlns:ns11="urn:core_2013_1.platform.webservices.netsuite.com"&gt;     
            &lt;ns11:name xsi:type="xsd:string"&gt;SUB  UK&lt;/ns11:name&gt;    
         &lt;/ns7:subsidiary&gt;    
         &lt;ns7:location internalId="2" xsi:type="ns12:RecordRef" xmlns:ns12="urn:core_2013_1.platform.webservices.netsuite.com"&gt;     
            &lt;ns12:name xsi:type="xsd:string"&gt;Location UK&lt;/ns12:name&gt;    
         &lt;/ns7:location&gt;    
         &lt;ns7:componentList replaceAll="false" xsi:type="ns7:WorkOrderCompletionComponentList"&gt;     
         &lt;ns7:workOrderCompletionComponent xsi:type="ns7:WorkOrderCompletionComponent"&gt;      
         &lt;ns7:item internalId="245" xsi:type="ns13:RecordRef" xmlns:ns13="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;      
         &lt;ns7:quantityPer xsi:type="xsd:double"&gt;5.0&lt;/ns7:quantityPer&gt;      
         &lt;ns7:lineNumber xsi:type="xsd:long"&gt;5&lt;/ns7:lineNumber&gt;     
         &lt;/ns7:workOrderCompletionComponent&gt;     
         &lt;ns7:workOrderCompletionComponent xsi:type="ns7:WorkOrderCompletionComponent"&gt;      
         &lt;ns7:item internalId="246" xsi:type="ns14:RecordRef" xmlns:ns14="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;      
         &lt;ns7:quantityPer xsi:type="xsd:double"&gt;6.0&lt;/ns7:quantityPer&gt;      
         &lt;ns7:lineNumber xsi:type="xsd:long"&gt;6&lt;/ns7:lineNumber&gt;     
         &lt;/ns7:workOrderCompletionComponent&gt;    
         &lt;/ns7:componentList&gt;    
         &lt;ns7:customFieldList xsi:type="ns15:CustomFieldList" xmlns:ns15="urn:core_2013_1.platform.webservices.netsuite.com"&gt;     
            &lt;ns15:customField internalId="65" scriptId="custbody_633637_bsubmit" xsi:type="ns15:BooleanCustomFieldRef"&gt;      
               &lt;ns15:value xsi:type="xsd:boolean"&gt;false&lt;/ns15:value&gt;     
            &lt;/ns15:customField&gt;  
            &lt;ns15:customField internalId="215" scriptId="custbody_633637_bload" xsi:type="ns15:BooleanCustomFieldRef"&gt;      
               &lt;ns15:value xsi:type="xsd:boolean"&gt;false&lt;/ns15:value&gt;        
            &lt;/ns15:customField&gt;     
            &lt;ns15:customField internalId="53" scriptId="custbody_633637_asubmit" xsi:type="ns15:BooleanCustomFieldRef"&gt;      
               &lt;ns15:value xsi:type="xsd:boolean"&gt;false&lt;/ns15:value&gt;     
            &lt;/ns15:customField&gt;    
         &lt;/ns7:customFieldList&gt;   
      &lt;/record&gt;  
   &lt;/add&gt; 
&lt;/soapenv:Body&gt;</pre>
      <h5 id="bridgehead_N3702345">SOAP Response (Add)</h5>
      <pre class="nshelp">&lt;soapenv:Body&gt;
   &lt;addResponse xmlns="urn:messages_2013_1.platform.webservices.netsuite.com"&gt;
      &lt;writeResponse&gt;
         &lt;platformCore:status isSuccess="true" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;
         &lt;baseRef internalId="268" type="workOrderCompletion" xsi:type="platformCore:RecordRef" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;
      &lt;/writeResponse&gt;
   &lt;/addResponse&gt;
&lt;/soapenv:Body&gt;</pre>
      <h4 id="bridgehead_N3702356">Using Add and Setting isBackflush to True</h4>
      <p>The following example omits the initialize operation — using only the add operation to create the record. In this example, isBackflush is set to true and values are sent for the quantities of items in the componentList sublist.</p>
      <p>Note that this example sets a value for the quantity body field. However, if the original work order used a routing, the quantity field would not be updatable.</p>
      <h5 id="bridgehead_N28571601">Java</h5>
      <pre class="nshelp">RecordRef createdFromRef = new RecordRef();
createdFromRef.setInternalId("167");
 
RecordRef postingPeriodRef = new RecordRef();
postingPeriodRef.setInternalId("141");
 
RecordRef departmentRef = new RecordRef();
departmentRef.setInternalId("2");
 
RecordRef classRef = new RecordRef();
classRef.setInternalId("2");
 
RecordRef item1Ref = new RecordRef();
item1Ref.setInternalId("245");
 
RecordRef item2Ref = new RecordRef();
item2Ref.setInternalId("246");
 
WorkOrderCompletion newWOCo = new WorkOrderCompletion();
newWOCo.setExternalId("WOCo-JS-001-TEST-BFT");
newWOCo.setCreatedFrom(createdFromRef);
newWOCo.setQuantity(2.0);
newWOCo.setIsBackflush(true);
newWOCo.setPostingPeriod(postingPeriodRef);
newWOCo.setDepartment(departmentRef);
newWOCo.set_class(classRef);
newWOCo.setMemo("Add - isBackFlush = T");
 
WorkOrderCompletionComponent[] componentListArray = { new WorkOrderCompletionComponent(), new WorkOrderCompletionComponent() };
componentListArray[0].setItem(item1Ref);
componentListArray[0].setQuantity(3.0);
componentListArray[1].setItem(item2Ref);
componentListArray[1].setQuantity(4.0);
 
WorkOrderCompletionComponentList componentList = new WorkOrderCompletionComponentList();
componentList.setWorkOrderCompletionComponent(componentListArray);
newWOCo.setComponentList(componentList);
 
c.addRecord(newWOCo);</pre>
      <h5 id="bridgehead_N28571661">SOAP Request</h5>
      <pre class="nshelp">&lt;soapenv:Body&gt;  
   &lt;add xmlns="urn:messages_2013_1.platform.webservices.netsuite.com"&gt;   
      &lt;record externalId="WOCo-JS-001-TEST-BFT" xsi:type="ns6:WorkOrderCompletion" xmlns:ns6="urn:inventory_2013_1.transactions.webservices.netsuite.com"&gt;    
         &lt;ns6:quantity xsi:type="xsd:double"&gt;2.0&lt;/ns6:quantity&gt;    
         &lt;ns6:isBackflush xsi:type="xsd:boolean"&gt;true&lt;/ns6:isBackflush&gt;    
         &lt;ns6:createdFrom internalId="167" xsi:type="ns7:RecordRef" xmlns:ns7="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;    
         &lt;ns6:postingPeriod internalId="141" xsi:type="ns8:RecordRef" xmlns:ns8="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;    
         &lt;ns6:memo xsi:type="xsd:string"&gt;Add - isBackFlush = T&lt;/ns6:memo&gt;    
         &lt;ns6:department internalId="2" xsi:type="ns9:RecordRef" xmlns:ns9="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;    
         &lt;ns6:class internalId="2" xsi:type="ns10:RecordRef" xmlns:ns10="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;    
         &lt;ns6:componentList replaceAll="false" xsi:type="ns6:WorkOrderCompletionComponentList"&gt;     
            &lt;ns6:workOrderCompletionComponent xsi:type="ns6:WorkOrderCompletionComponent"&gt;      
               &lt;ns6:item internalId="245" xsi:type="ns11:RecordRef" xmlns:ns11="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;      
               &lt;ns6:quantity xsi:type="xsd:double"&gt;3.0&lt;/ns6:quantity&gt;     
            &lt;/ns6:workOrderCompletionComponent&gt;     
            &lt;ns6:workOrderCompletionComponent xsi:type="ns6:WorkOrderCompletionComponent"&gt;      
               &lt;ns6:item internalId="246" xsi:type="ns12:RecordRef" xmlns:ns12="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;      
               &lt;ns6:quantity xsi:type="xsd:double"&gt;4.0&lt;/ns6:quantity&gt;     
            &lt;/ns6:workOrderCompletionComponent&gt;    
         &lt;/ns6:componentList&gt;   
      &lt;/record&gt;  
   &lt;/add&gt; 
&lt;/soapenv:Body&gt;</pre>
      <h5 id="bridgehead_N3702403">SOAP Response</h5>
      <pre class="nshelp">&lt;soapenv:Body&gt;
   &lt;addResponse xmlns="urn:messages_2013_1.platform.webservices.netsuite.com"&gt;
      &lt;writeResponse&gt;
         &lt;platformCore:status isSuccess="true" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;
         &lt;baseRef internalId="267" externalId="WOCo-JS-001-TEST-BFT" type="workOrderCompletion" xsi:type="platformCore:RecordRef" xmlns:platformCore="urn:core_2013_1.platform.webservices.netsuite.com"/&gt;
      &lt;/writeResponse&gt;
   &lt;/addResponse&gt;
&lt;/soapenv:Body&gt;</pre>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3698631" href="./section_N3698631.html">Work Order Issue</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3702654" href="./section_N3702654.html">Multiple Shipping Routes in Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3657735" href="./chapter_N3657735.html">Transactions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3658677" href="./section_N3658677.html">Usage Notes for Transaction Record Types</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3659492" href="./section_N3659492.html">Transaction Search</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3702654" href="./section_N3702654.html">Multiple Shipping Routes in Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3635039" href="./section_N3635039.html">How to Use Web Services Records Help</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3635369" href="./section_N3635369.html">Web Services Supported Records</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3639052" href="./section_N3639052.html">Working with the SuiteTalk Schema Browser</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3412777" href="./chapter_N3412777.html">SuiteTalk Platform Overview</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2319010" href="./section_N2319010.html">Assembly Items</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2335392" href="./section_N2335392.html">Manufacturing Work In Process (WIP)</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2329173" href="./section_N2329173.html">Entering an Individual Work Order</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

