<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2453322" href="./set_N2453322.html">SuiteCommerce</a>
            <a rev="book_4260168573" href="./book_4260168573.html">SuiteCommerce Advanced</a>
            <a rev="part_4387345266" href="./part_4387345266.html">Developer’s Guide</a>
            <a rev="section_4307434462" href="./section_4307434462.html">SuiteCommerce Advanced Architecture</a>
            <a rev="section_4307437864" href="./section_4307437864.html">Module Architecture</a>
            <a class="nshelp_navlast" rev="section_4307439828" href="./section_4307439828.html">Views</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4307439828" class="nshelp_title">Views</h1>
      <p><span id="dyn_help_feature" dynid="SUITECOMMERCEENTERPRISE">&nbsp;</span>   </p>
      <p>Within the Backbone.js framework, views listen to the user interface events and coordinate with the data required by the associated module. In some modules, when a router initializes a view, the router also passes a model or collection containing the data required. In other modules, the view includes all necessary models or collections as dependencies.</p>
      <h4 id="bridgehead_1487263343">View Architecture</h4>
      <p>This section applies to the <strong>Elbrus</strong> release of SCA and later.</p>
      <p>All Backbone.Views are Backbone.CompositeViews by default. Composite views are simply views that contain child views. Composite views further modularize the application, so some types of views can be used in multiple contexts. For example, some modules define Child Views that are extensions of Backbone.View. As a consequence, those Child Views are also Backbone.CompositeViews. This ensures that some types of data are displayed in a consistent way across the application.</p>
      <p>All views extend Backbone.CompositeView by default. Any custom views should use similar code to the example below:</p>
      <pre class="nshelp">...js
   var MyView = Backbone.View.extend({
      initialize: function()
      {
         //every view now is a composite view!
      }
   });
...</pre>
      <img class="nshelp_diagram" src="/help/helpcenter/en_US/Output/Help/images/SuiteCommerce/SCAWebStores/Develop/ViewArchitecture-ElbrusAndLater.png" width="213px" height="390px">
      <p>A Parent View must declare its Child Views. To do this, each Parent View declares the <code>childViews</code> property, which is an object containing all the Container names from the View (the data-views). Each container has an object with the Child View’s name and an object for each View with the following information:</p>
      <ul class="nshelp">
        <li>
          <p><code>childViewIndex</code> – this is the order in which the children render in that container.</p>
        </li>
        <li>
          <p><code>childViewConstructor</code> – This can be either functions or Backbone.View subclasses. Each defined function must return an instance of a Backbone.View.</p>
        </li>
      </ul>
      <p>The following example declares the <code>childViews</code> property, containing the <code>Buttons</code> container. The Buttons container declares the <code>Wishlist</code> and <code>SaveForLater</code> objects. You can include multiple views in one container.</p>
      <pre class="nshelp">...
   childViews: {
      'Buttons': {
         'Whishlist': {
            'childViewIndex': 10
         ,   'childViewConstructor': function() {
               return new WhishlistView();
            }
         }
         'SaveForLater': {
            'childViewIndex': 20
         ,   'childViewConstructor': SomeView
         }
      }
   }
...</pre>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">You can add new Child Views to a View class using the <code>addChildViews( )</code> method. You can also add new Child Views to an instance of a view using the <code>addChildViews( )</code> method or by passing the <code>childViews</code> property in the options when initializing it.</p>
      </div>
      <br>
      <p>To add a plugin before or after you initialize the View, add the plugin to the View Class and execute code in those moments (<code>beforeInitialize</code> and <code>afterInitialize</code>), as shown below:</p>
      <pre class="nshelp">...
Backbone.View.beforeInitialize.install({
      name: 'compositeView'
   ,   priority: 1
   ,   execute: function ()
      {
         var childViews = _.extend({}, this.childViews, this.constructor.childViews);

         this.childViewInstances =  {};

         this.addChildViews(childViews);

         if (this.options)
         {
            if (this.options.extraChildViews)
            {
               console.warn('DEPRECATED: "options.extraChildViews" is deprecated. Use "options.childViews" instead');
               //Add extra child views from view's initialization options

               this.addChildViews(this.options.extraChildViews);
            }

            if (this.options.extraChildViews)
            {
               //Add child views from view's initialization options
               this.addChildViews(this.options.extraChildViews);
            }
         }
      }
   });
...</pre>
      <h5 id="bridgehead_1487870617">Backwards Compatibility</h5>
      <p>To make this change backward compatible with earlier releases of SCA, Backbone.CompositeView.js includes a CompositeView.add() method as a no operation (noop) method. This prevents any errors when calling it from any custom code in implementations prior to Elbrus release. SCA supports the previous format as shown below:</p>
      <pre class="nshelp">...
   childViews: {
      'Promocode': function()
      {
         return new PromocodeView({model: this.model.get('promocode')});
      }
   ,   'SomeWidget': SomeWidget
   }
...</pre>
      <p>In this case, the container and the view name have the same, and you can combine the old and the new format and in the <code>childViews</code> property.</p>
      <pre class="nshelp">...
   childViews: {
      Promocode':
      {
         'Promocode':
         {
            childViewIndex: 10
         ,   childViewConstructor: function(options)
            {
               return new PromocodeView({model: this.model.get('promocode')});
            }
         }
      }
   }
...</pre>
      <h4 id="bridgehead_1487262641">View Architecture (Vinson Release and Earlier)</h4>
      <p>This section applies to the <strong>Vinson</strong> release of SCA and earlier.</p>
      <p>In the Vinson release of SCA and earlier, each view must declare itself as a composite view using the <code>require( )</code> method and call <code>CompositeView.add(this)</code> within the <code>initialize( )</code> method. Any custom views should use similar code to the example below:</p>
      <pre class="nshelp">...
   var CompositeView = require('Backbone.CompositeView');
   var MyView = new backbone.View.extend({
      initialize: function()
      {
         CompositeView.add(this);
         //now this view is a composite view!
      }
   });
...</pre>
      <img class="nshelp_diagram" src="/help/helpcenter/en_US/Output/Help/images/SuiteCommerce/SCAWebStores/Develop/ViewArchitecture-VinsonAndEarlier.png" width="462px" height="282px">
      <h4 id="bridgehead_1487263434">Rendering Data From the View</h4>
      <p>The process of rending the HTML is handled by the templating engine and the Backbone.js framework. To work within this framework, each view defines the following:</p>
      <ul class="nshelp">
        <li>
          <p>A template file included as a dependency within the define method of the view.</p>
        </li>
        <li>
          <p>A template property defined in the body of the view.</p>
        </li>
        <li>
          <p>A <code>getContext</code> method that returns all of the data attributes required by the template.</p>
        </li>
      </ul>
      <p>When you compile SuiteCommerce Advance using the developer tools, Gulp.js uses these elements to compile the templates into the application. See <a rev="section_4340366459" href="./section_4340366459.html">Logic-less Templates and Handlebars.js</a> for more information on templates and the template engine.</p>
      <p>If implementing the Elbrus release of SCA or later, a data view acts as a container for multiple child views. Any parent view must declare a placeholder element in its associated template to render the children within it as depicted in the following example:</p>
      <pre class="nshelp">...
html
         &lt;div data-view="Buttons"&gt;&lt;/div&gt;
...</pre>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307437864" href="./section_4307437864.html">Module Architecture</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307439566" href="./section_4307439566.html">Entry Point</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307439693" href="./section_4307439693.html">Routers</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307439952" href="./section_4307439952.html">Models, Collections, and Services</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

