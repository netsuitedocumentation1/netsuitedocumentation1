<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N2823893" href="./book_N2823893.html">SuiteBuilder (Customization)</a>
            <a rev="chapter_N2826978" href="./chapter_N2826978.html">Custom Fields</a>
            <a rev="section_N2829580" href="./section_N2829580.html">Creating a Custom Field</a>
            <a class="nshelp_navlast" rev="section_N2839623" href="./section_N2839623.html">Setting Sourcing Criteria</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2839623" class="nshelp_title">Setting Sourcing Criteria</h1>
      <p>A custom field can source information from another record in your account. The information populated into the custom field is then dependent on fields associated with a record selected on another field within that form. Sourcing enhances your NetSuite forms by reducing data-entry errors and ensuring that your customers and employees always have the most current information.</p>
      <p>You can source from both standard and custom fields.</p>
      <div class="nshelp_informalexample">
        <p>For example, two custom fields — <strong>Sales Rep</strong> and <strong>Sales Rep Email</strong> — are placed on a custom case form. When a company record is selected in the Company field, the sales representative already defined in the selected company record is sourced to the <strong>Sales Rep</strong> field on the case form and the <strong>Sales Rep Email</strong> field defaults to the email address defined for the sourced Sales Rep.</p>
      </div>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The information is sourced into the custom field only when the record is created or if the specific fields involved are altered when editing the record. In the example above, if you change the sales representative selected on the customer, the sourced field would change to show the email address of the new sales representative.</p>
      </div>
      <br>
      <p>When setting up sourcing, you have the option to store the value. When the field is not stored, the information is not saved in the custom field. A custom field that does not store the value enables you to look at data that is stored elsewhere when you are setting up searches and reports.</p>
      <p>By storing a sourced field, the sourcing will automatically fill the field with a value when the master field is changed. You can then change the value of the custom field. The value is stored independently and has no impact on the source field, so any changes made in the custom field are not updated in the source.</p>
      <div class="nshelp_procedure">
        <h4 id="procedure_N2839660">To set sourcing and filtering criteria:</h4>
        <ol class="nshelp">
          <li>
            <p>Edit the custom field you want to add sourcing and filtering criteria to.</p>
          </li>
          <li>
            <p>If you do not want to store the value, clear the <strong>Store Value</strong> box. In most cases you do not want to store the value.</p>
          </li>
          <li>
            <p>On the Display subtab, check the <strong>Display Type</strong> field. If the <strong>Display Type</strong> field is set to <strong>Hidden</strong>, sourcing does not occur.</p>
          </li>
          <li>
            <p>Click the <strong>Sourcing &amp; Filtering</strong> subtab.</p>
            <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/sourcing2.png" width="608px" height="76px">
          </li>
          <li>
            <p>From the <strong>Source List</strong> list, select the field that references the record you would like to source information from.</p>
            <div class="nshelp_informalexample">
              <p>For example, you are creating a custom field to appear on the customer record to show the email address of the sales representative assigned to the customer. In the <strong>Source List</strong> field, select <strong>Sales Rep</strong>.</p>
            </div>
            <p>When working with entity fields, you can also define the field to source from a field on the parent record by selecting <strong>Parent</strong> from the <strong>Source List</strong> list.</p>
            <div class="nshelp_note">
              <h3>Note</h3>
              <p class="nshelp_first">You <strong>cannot</strong> source information for a Multiple Select field type.</p>
            </div>
            <br>
          </li>
          <li>
            <p>From the <strong>Source From</strong> list, select the field you want to source from. The source from field is found on the record you selected from the <strong>Source List</strong> list.</p>
            <p>Any fields available on the record you select in the <strong>Source List</strong> field can be selected from the <strong>Source From</strong> list.</p>
            <div class="nshelp_informalexample">
              <p>In the example above, select <strong>Email</strong> from the <strong>Source From</strong> list.</p>
            </div>
            <p>The value stored in the field selected here populates the field when the record is selected. The field selected here must be consistent with the type selected for the custom field. For example, if you select <strong>E-mail</strong> as the field type and then select an address field from the <strong>Source From</strong> list, you receive an error.</p>
            <div class="nshelp_note">
              <h3>Note</h3>
              <p class="nshelp_first">If your field is a List/Record field, the field selected for the Source From field <strong>must</strong> be in the record type selected as the List/Record.</p>
            </div>
            <br>
          </li>
          <li>
            <p>If your field is a List/Record field, you can filter the choices that can be selected.</p>
            <p>When a List/Record Type field is being defined, you can choose to populate the custom field with values that meet specific parameters in the sourced list or record. First, select the desired item to filter by from the <strong>Source Filter by</strong> list. Then select an item from the <strong>Source List</strong> and, optionally from the <strong>Source From</strong> lists. When you select an element from the source list, it will fill the option-sourced custom field with all elements where the source filter by field matches the source list (or the source from field of the source list).</p>
            <p>The record you are sourcing from must be associated with the type of record you want to appear in your custom field.</p>
            <div class="nshelp_note">
              <h3>Note</h3>
              <p class="nshelp_first">The field selected from the <strong>Source Filter by</strong> list <strong>must</strong> be in the record type selected as the List/Record.</p>
            </div>
            <br>
            <p>For more information, see <a rev="section_N2839994" href="./section_N2839994.html">Understanding the ‘Source Filter by' Field</a>.</p>
          </li>
          <li>
            <p>After you have set the sourcing criteria, you should set any filtering criteria. See <a rev="section_N2840153" href="./section_N2840153.html">Setting Filtering Criteria</a>.</p>
          </li>
        </ol>
      </div>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">A custom field with a sourcing relationship is not available for mass updates or inline editing. See <a rev="section_N666653" href="./section_N666653.html">Making Mass Changes or Updates</a>.</p>
      </div>
      <br>
      <h3 id="bridgehead_N2839850">Note about Custom Transaction Column Field Sourcing</h3>
      <p>Before Version 2012 Release 2, you could source a custom transaction column field's values from a body field by selecting &lt;Record_Name&gt; (Line) in the Source List list on the Sourcing &amp; Filtering subtab of the Transaction Column Field page. Now, to source a field's values from a body field, you need to select &lt;Record_Name&gt; in the list. If you select &lt;Record_Name&gt; (Line), sourcing is from a field in the sublist and if there is no such field, values for the sourced custom transaction column field are blank.</p>
      <p>The following example shows a custom field applied to the Items sublist on Sales Order records. Because the sublist does not include a Customer field, if Customer (Line) is selected, the values for the new custom field will be blank. Selecting Customer sources the new custom field's values from the Customer body field on Sales Orders.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/CustFieldSourcing.png" width="630px" height="406px">
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2829580" href="./section_N2829580.html">Creating a Custom Field</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2830027" href="./section_N2830027.html">Assigning Custom Fields to Specific Record Types</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2830238" href="./section_N2830238.html">Setting Display Options for Custom Fields</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2830711" href="./section_N2830711.html">Setting Validation and Defaulting Properties</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2839994" href="./section_N2839994.html">Understanding the ‘Source Filter by' Field</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2840153" href="./section_N2840153.html">Setting Filtering Criteria</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2840468" href="./section_N2840468.html">Multiple Dependent Dropdown Lists</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2841053" href="./section_N2841053.html">Restricting Access to Custom Fields</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2842596" href="./section_N2842596.html">Creating Read-Only Custom Fields</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4308565496" href="./section_4308565496.html">Adding Translations for Custom Fields</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2828541" href="./section_N2828541.html">Adding Custom Fields to Transaction Forms</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2842413" href="./section_N2842413.html">Tracking Changes to Custom Fields</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

