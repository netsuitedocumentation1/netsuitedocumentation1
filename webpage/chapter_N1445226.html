<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N1379402" href="./set_N1379402.html">Accounting</a>
            <a rev="preface_3710627041" href="./preface_3710627041.html">General Accounting</a>
            <a class="nshelp_navlast" rev="chapter_N1445226" href="./chapter_N1445226.html">Working with Accounting Periods</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="chapter_N1445226" class="nshelp_title">Working with Accounting Periods</h1>
      <p><span id="dyn_help_feature" dynid="ACCOUNTINGPERIODS">&nbsp;</span>   </p>
      <p>The Accounting Periods feature supports general ledger management. If accounting periods are not used, NetSuite transactions post in real time. When accounting periods are enabled, users can select a past or future posting period for each transaction, and accounting personnel can more effectively monitor and control the status of accounts.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">If your system has historical transaction data and you want to use the Accounting Periods feature, enable the feature and then create an accounting period for a past quarter or month. NetSuite automatically assigns that accounting period to the transactions dated within the period's date range.</p>
      </div>
      <br>
      <p>A user with the Enable Features permission can enable the Accounting Periods feature at <span id="dyn_help_taskpath" dynid="ADMI_FEATURES">&nbsp;</span>, on the Accounting subtab. After the feature is enabled, periods can be set up at <span id="dyn_help_taskpath" dynid="LIST_PERIOD">&nbsp;</span>. See <a rev="section_N1445585" href="./section_N1445585.html">Setting Up Accounting Periods</a>.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">If you do not have a OneWorld account, you cannot disable the Accounting Periods feature if an accounting period exists. To disable the feature you must delete all created accounting periods.</p>
      </div>
      <br>
      <p>OneWorld accounts support using fiscal calendars to define fiscal years with different start and end dates per subsidiary. See <a rev="section_N1449211" href="./section_N1449211.html">Using Fiscal Calendars</a>.</p>
      <p>If your international auditing requirements include gapless numbering sequences to all GL posting transactions, enable the GL Audit Numbering feature. When this feature is enabled, numbering GL posting transactions is a required task when closing the <strong>last month</strong> in an accounting period. For more information see, <a rev="section_3735573963" href="./section_3735573963.html">Using GL Audit Numbering</a>.</p>
      <p>If NetSuite Professional Services has implemented Multi-Book Accounting in your account, you can individually close and reopen accounting periods associated with any accounting book, without impacting your other accounting books. You choose the accounting book for which you want to close a period through the Filters section at the top of the Manage Accounting Periods page. The accounting books available for selection depend upon your user role and associated permissions. For more information about the Extended Accounting Period Close Process feature, see <a rev="section_4308385929" href="./section_4308385929.html">Accounting Book Period Close Management</a>.</p>
      <p>For information about managing tax periods, see <a rev="section_N1797157" href="./section_N1797157.html">Working with Tax Periods</a>.</p>
      <p>Periods can be locked to prevent the posting of transactions that affect the general ledger. The locking of a period to A/P, A/R, and Payroll transactions provides a pre-closed state that permits the balancing of a period's financials before closing. These locking tasks are the first steps in a Period Close Checklist of tasks available in the NetSuite user interface. After these tasks are completed, the period can be closed. After a period is closed, it is unavailable for adding new posting transactions or making edits that impact G/L. For more information, see <a rev="section_N1452509" href="./section_N1452509.html">Closing Accounting Periods</a>.</p>
      <p>You can automatically maintain a number of unlocked current and future periods to facilitate your monthly business and accounting processes while preventing erroneous data entry in future periods. See <a rev="section_N1451349" href="./section_N1451349.html">Locking Future Accounting Periods</a>.</p>
      <p>The Accounting Periods feature permits organization of report, search, and key performance indicator data by periods to simplify financial review and analysis. For information about sorting and filtering report data by period, see <a rev="section_N1458661" href="./section_N1458661.html">Reporting by Accounting Period</a>. For information about filtering and displaying search data by period, see <a rev="section_N1459178" href="./section_N1459178.html">Searching by Accounting Period</a>.</p>
      <p>Be aware of the following permissions requirements for accounting periods:</p>
      <ul class="nshelp">
        <li>
          <p><strong>Edit</strong> or <strong>Full</strong> level of the <strong>Manage Accounting Periods</strong> permission is required to set up, edit, lock transactions for period close, or reopen accounting periods.</p>
          <p>Users with the <strong>View</strong> level of this permission can see the Manage Accounting Periods page but cannot make changes to periods.</p>
          <div class="nshelp_imp">
            <h3>Important</h3>
            <p class="nshelp_first">When a journal entry, intercompany journal entry, statistical journal entry, or commission transaction has no defined posting period, users with <strong>View</strong> level of the <strong>Manage Accounting Periods</strong> permission see a suggested accounting period in the Posting Period field on the transaction. For example, if January is a locked accounting period, an A/R Clerk with <strong>View</strong> level of the permission would see the suggested posting period as February. An administrator with <strong>Full</strong> level of the permission would see the suggested posting period as January.</p>
            <p>For the following transactions, users with <strong>View</strong> level of the <strong>Manage Accounting Periods</strong> permission do not see a suggested posting period when there is no defined accounting period: Cash Sale, Check, Credit Card Charge, Credit Card Refund, Customer Refund, Credit Memo, Customer Payment, Deposit, Expense Report, Inventory Adjustment, Invoice, Item Fulfillment, Item Receipt, Transfer, Vendor Bill, and Vendor Payment.</p>
          </div>
          <br>
        </li>
        <li>
          <p>The <strong>Override Period Restrictions</strong> permission is required to add and make G/L impacting changes to posting transactions in periods that have been locked to transactions.</p>
        </li>
        <li>
          <p>Users must have both of the above permissions in addition to task-specific permissions to complete Period Close Checklist tasks such as automated intercompany adjustments, open currency balance revaluations, and calculation of consolidated exchange rates.</p>
        </li>
        <li>
          <p>If the <strong>Allow Non-G/L Changes</strong> option is enabled for a period, users with the <strong>Allow Non G/L Changes</strong> permission can make non-G/L changes to posting transactions after the period has been locked to transactions, and after the period has been closed.</p>
        </li>
        <li>
          <p>No one can make G/L impacting changes to posting transactions in a closed period. The period must be reopened before these changes can be posted.</p>
        </li>
        <li>
          <p>These permission restrictions do not apply to the addition or editing of non-posting transactions such as sales orders and return authorizations. See <a rev="section_N1452887" href="./section_N1452887.html">Non-Posting Transactions in Locked and Closed Periods</a>.</p>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N560870" href="./section_N560870.html">Locking Transactions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1383640" href="./chapter_N1383640.html">Using Accounting Features and Preferences</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1395057" href="./chapter_N1395057.html">Working with Currencies</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1439850" href="./chapter_N1439850.html">Working with the Chart of Accounts</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1459499" href="./chapter_N1459499.html">General Ledger Impact of Transactions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1468455" href="./chapter_N1468455.html">Working with Journal Entries</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1486105" href="./chapter_N1486105.html">Automated Intercompany Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1503165" href="./chapter_N1503165.html">Working with Budgets</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1508800" href="./chapter_N1508800.html">Working with Account Registers</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1519116" href="./chapter_N1519116.html">Working with Accounting Reports</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1379709" href="./chapter_N1379709.html">NetSuite Accounting Overview</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

