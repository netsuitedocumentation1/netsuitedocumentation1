<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N973142" href="./set_N973142.html">Marketing, Sales Force Automation, and Partners</a>
            <a rev="book_N1035197" href="./book_N1035197.html">Sales Force Automation</a>
            <a rev="chapter_N1074872" href="./chapter_N1074872.html">Record Management</a>
            <a rev="section_N1076428" href="./section_N1076428.html">Customers</a>
            <a class="nshelp_navlast" rev="section_N1080144" href="./section_N1080144.html">Customer Credit Limits and Holds</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N1080144" class="nshelp_title">Customer Credit Limits and Holds</h1>
      <p><span id="dyn_help_feature" dynid="ACCOUNTING">&nbsp;</span>   </p>
      <p>You can use credit limits and credit holds to manage the amount of credit you extend to customers. If a customer is delinquent in making payments or reaches a pre-set credit limit, NetSuite can restrict the entry of new sales transactions on credit for that customer. The customer is restricted on placing new orders until they pay the invoices due or you manually release the hold.</p>
      <p>To use credit limits and holds, you first need to set credit limits on customer records by defining the maximum currency amount the customer is allowed to accrue in outstanding receivables, and then set your preference for handling credit limit warnings or holds.</p>
      <p>A credit limit defines the maximum currency amount the customer is allowed to accrue in outstanding receivables.</p>
      <p>A credit hold blocks entry of a new sales transaction under these circumstances:</p>
      <ul class="nshelp">
        <li>
          <p>When the customer credit limit handling preference is set to Enforce Holds and one of the following occurs:</p>
          <ul class="nshelp">
            <li>
              <p>Credit limit reached – When the customer’s preset credit limit is exceeded, the credit hold is applied automatically. The customer must settle one or more outstanding invoices to reduce the balance due and release the credit hold.</p>
            </li>
            <li>
              <p>Delinquent payments – When a customer fails to pay an invoice by its due date and the specified grace period has elapsed, the payment due on the invoice is classified as delinquent.</p>
            </li>
          </ul>
        </li>
        <li>
          <p>Credit hold applied manually – You can place a credit hold on a customer record manually. NetSuite ignores the assigned credit limit amount and the customer is blocked from placing an order even if the credit limit has not been reached. You must release a manual credit hold to allow the customer to create orders.</p>
        </li>
      </ul>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The customer credit limit handling setting determines if the credit hold blocks a transaction. For more information, see <a rev="section_N1080144" href="#bridgehead_4490498729">Credit Limit Preferences</a>. If you want to display a warning instead of blocking the transaction, you can set the Customer Credit Limit Handling preference to Warn Only. NetSuite checks the credit limit when you add an item to a transaction and displays a warning if the credit limit is exceeded. The transaction can still be completed.</p>
      </div>
      <br>
      <div class="nshelp_procedure">
        <h4>To manage customer credit limits and holds:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to <span id="dyn_help_taskpath" dynid="LIST_CUSTJOB">&nbsp;</span>.</p>
          </li>
          <li>
            <p>Click <strong>Edit</strong> next to the customer name.</p>
          </li>
          <li>
            <p>Click the <strong>Financial</strong> subtab.</p>
          </li>
          <li>
            <p>In the <strong>Credit Limit</strong> field, enter the maximum amount of credit on which the customer can purchase goods or services.</p>
            <p>If you use the <a rev="section_N1398493" href="./section_N1398493.html">Multiple Currencies and Customers</a> feature, the credit limit you enter on the customer record is entered in the customer primary currency. This is the total credit limit for all of the customer's transaction currencies. If you change a customer's primary currency, you must also re-enter the credit limit in that new currency.</p>
          </li>
          <li>
            <p>In the <strong>Hold</strong> field, choose a credit hold setting:</p>
            <ul class="nshelp">
              <li>
                <p><strong>Auto</strong> – Credit warnings and blocks are applied according to the customer’s credit limit and the Credit Limit Handling preference in Accounting Preferences. This is the default option for new customer records.</p>
              </li>
              <li>
                <p><strong>On</strong> – Prevents the customer from purchasing goods on credit even if the credit limit is not reached. This allows you to apply a manual credit hold.</p>
              </li>
              <li>
                <p><strong>Off</strong> – Disables credit limits and credit holds for the customer. The customer’s credit limit is ignored when the transaction is saved and the customer can purchase any amount of goods on credit.</p>
              </li>
            </ul>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <p>Next, set your preference to receive a warning or block new transactions whenever a credit limit has been reached.</p>
      <h2 id="bridgehead_4490498729">Credit Limit Preferences</h2>
      <p>The customer credit limit preferences determine the grace period for overdue invoices and what happens when customers exceed their credit limit. Credit limit handling preferences apply to all customers unless an administrator enables an override.</p>
      <div class="nshelp_procedure">
        <h4>To set the preference for handling credit limits:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to <span id="dyn_help_taskpath" dynid="ADMI_ACCTSETUP">&nbsp;</span>.</p>
          </li>
          <li>
            <p>On the <strong>General</strong> subtab, select your preferred method in the Customer Credit Limit Handling field:</p>
            <ul class="nshelp">
              <li>
                <p><strong>Ignore</strong> – Select this method to allow sales orders and invoices to be entered for a customer that has met or exceeded their credit limit.</p>
              </li>
              <li>
                <p><strong>Warn Only</strong> – Select this method to generate a warning when a transaction is being entered that puts a customer at or above their credit limit. You can choose to enter or cancel the transaction whenever the warning has appeared.</p>
              </li>
              <li>
                <p><strong>Enforce Holds</strong> – Select this method to block the entry of a transaction that puts a customer at or above their credit limit.</p>
              </li>
            </ul>
          </li>
          <li>
            <p>In the <strong>Days Overdue for Warning/Hold</strong> field, enter the number of days overdue at which you want to generate a warning or enforce a hold.</p>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <p>To allow an override, an administrator must go to <span id="dyn_help_taskpath" dynid="ADMI_GENERAL">&nbsp;</span>. On the Overriding Preferences subtab, check the Allow Override box next to Credit Limit Handling, and click Save. Users can then set their individual preference by going to Home &gt; Set Preferences &gt; Transactions and selecting their preferred method in the Customer Credit Limit Handling field.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The Customer Credit Limit Handling preference does not affect the entry of opportunities, estimates or cash sales.</p>
      </div>
      <br>
      <p>Credit limit warnings and overdue invoice warnings also appear when you enter order fulfillments and your credit limit handling is set to Warn Only or Enforce Holds.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">A fulfillment can generate a credit hold warning, but cannot be blocked.</p>
      </div>
      <br>
      <p>A credit hold is lifted after the customer has made the appropriate payments. Or, a manager can choose to check the Override Hold box on the Financial subtab of a customer record for exceptions to your normal A/R and customer credit rules. Then, new transactions can be entered for that customer even when they are over their credit limit or have invoices overdue.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">If a credit memo has been issued to a customer, the amount of the credit memo is added to the customer’s balance due amount. This provides additional credit to the customer because it reduces the customer’s balance due. The total credit available to a customer is the credit limit amount minus the balance due amount. If a credit memo results in a negative balance due amount, the total amount of credit available to the customer will be greater than the specified credit limit. For example, if a customer’s credit limit is 500 USD and the balance due is –250 USD, the customer can purchase goods on credit up to a value of 750 USD.</p>
      </div>
      <br>
      <h2 id="bridgehead_4490500811">Additional Notes About Credit Limit Warnings</h2>
      <p>Note the following regarding credit limit warnings:</p>
      <ul class="nshelp">
        <li>
          <p>If an accounts receivable transaction provokes an On Hold warning, later transactions will not provoke an Overdue warning or a Credit Limit Balance warning.</p>
        </li>
        <li>
          <p>Accounts receivable transactions can provoke both an Overdue warning and a Credit Limit Balance warning.</p>
        </li>
        <li>
          <p>Accounts receivable transactions never provoke both an On Hold warning and an Overdue warning.</p>
        </li>
        <li>
          <p>Accounts receivable transactions never provoke both an On Hold warning and a Credit Limit Balance warning.</p>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1076428" href="./section_N1076428.html">Customers</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1083432" href="./section_N1083432.html">Assigning a Custom Price Level to a Customer</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1084186" href="./section_N1084186.html">Tracking Customer Credit Card Information</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1084924" href="./section_N1084924.html">Attaching Events, Tasks, and Calls to Records and Transactions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1085394" href="./section_N1085394.html">Customer Dashboards</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

