<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2453322" href="./set_N2453322.html">SuiteCommerce</a>
            <a rev="book_4260168573" href="./book_4260168573.html">SuiteCommerce Advanced</a>
            <a rev="part_4387345266" href="./part_4387345266.html">Developer’s Guide</a>
            <a rev="section_4307434462" href="./section_4307434462.html">SuiteCommerce Advanced Architecture</a>
            <a class="nshelp_navlast" rev="section_4786239896" href="./section_4786239896.html">Product Details Page Architecture</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4786239896" class="nshelp_title">Product Details Page Architecture</h1>
      <p><span id="dyn_help_feature" dynid="SUITECOMMERCEENTERPRISE">&nbsp;</span>   </p>
      <p>This section applies to the <strong>Elbrus</strong> release of SuiteCommerce Advanced and later. The following architecture is not backwards compatible with previous versions of SuiteCommerce Advanced.</p>
      <p>The product details page (PDP) provides shoppers with detailed information about a product and lets shoppers add items to their cart. To ensure a positive user experience, the PDP needs to create intuitive interactions and return information from the server quickly and efficiently. The architecture of the PDP is designed to accommodate these challenges. Communication of item details is accomplished through the Items and Product modules. This unifies the data structure governing order management.</p>
      <p>The following diagram details the architecture behind the PDP and how user selections interact with NetSuite.</p>
      <img class="nshelp_diagram_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCommerce/SCAWebStores/Develop/PDP-ArchitectureDiagram.png" width="630px" height="354px">
      <ol class="nshelp">
        <li>
          <p>&nbsp;Logic in the Item module retrieves read-only data from NetSuite. Item.Model retrieves data about an item via the Search API. This is a client-side, read-only instance of an item record in NetSuite. Item.Option receives read-only information about an option associated with the item. Item.Option.Collection is a collection of these possible options.</p>
          <div class="nshelp_note">
            <h3>Note</h3>
            <p class="nshelp_first">The Item.Model is the read-only data from NetSuite used by the Product.Model to validate the user selection. Providing a read-only version of the Item ensures that the original backend data cannot be accidentally overridden.</p>
          </div>
          <br>
        </li>
        <li>
          <p>Product.Model contains an Item.Model associated with an item to display in the PDP. Product.Option.Model contains editable instances of the related Item.Option models, and Product.Option.Collection is a collection of these options.</p>
        </li>
        <li>
          <p>The views and child views in the ProductDetails module use the data in the Product module to render the PDP. ProductDetails.Base.View contains the abstract view from which the Full PDP and Quick View views inherit.</p>
        </li>
        <li>
          <p>The views and child views in the ProductViews module use the data in the Product module to render price and item options in the PDP.</p>
          <div class="nshelp_note">
            <h3>Note</h3>
            <p class="nshelp_first">To enhance extensibility, SCA uses two different item option templates, one for the PDP and another for search results (Facets).</p>
          </div>
          <br>
        </li>
        <li>
          <p>When the user interacts with the PDP to select options, Product.Model validates the data against the Item.Model that each Product.Model contains. This is why the Product.Model contains a Item.Model and why the Item.Model is a client-side representation of the backend record.</p>
        </li>
        <li>
          <p>When the user chooses valid options and clicks Add To Cart, the following conversions occur:</p>
          <ul class="nshelp">
            <li>
              <p>Product.Option.Model gets converted into Transaction.Line.Option.Model</p>
            </li>
            <li>
              <p>Product.Option.Collection gets converted to Transaction.Line.Option.Collection</p>
            </li>
            <li>
              <p>Product.Model gets converted into a Transaction.Line.Model</p>
            </li>
          </ul>
          <p>The Transaction.Model contains a collection of transaction lines, which each include an item and selected options. Transaction.Line.Option.Model contains one of the selected option, and Transaction.Line.Option.Collection is a collection of all selected options.</p>
        </li>
        <li>
          <p>The ProductLine module contains views that are shared between the PDP, Checkout, and My Account applications.</p>
        </li>
        <li>
          <p>The Transaction module uses views in the Transaction.Line.Views module to render transaction details in the cart and in other modules.</p>
        </li>
      </ol>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Item.KeyMapping.js is part of the Item Module. This file contains mapping definitions of what is returned by the Search API. For example, if you want to set the name of items for data stored in a custom field instead of the default <strong>Display Name</strong> field, you extend the mapping in the Item.KeyMapping file, as opposed to customizing each instance across the entire code base.</p>
      </div>
      <br>
      <h3 id="bridgehead_4807170438">Configuration</h3>
      <p>To configure the Product Details Page, you must have the SuiteCommerce Configuration bundle installed. Refer to the correct section for details on configurable properties as described below.</p>
      <div class="nshelp_procedure">
        <h4>To configure the Product Details Page:</h4>
        <ol class="nshelp">
          <li>
            <p>Select the domain to configure at Setup &gt; SuiteCommerce Advanced &gt; Configuration.</p>
          </li>
          <li>
            <p>In the SuiteCommerce Configuration record, navigate to the Shopping Catalog tab and the appropriate subtab:</p>
            <ul class="nshelp">
              <li>
                <p><a rev="bridgehead_4667044038" href="./bridgehead_4667044038.html">Item Options Subtab</a> – configure how item options appear in the Product Details Page and in My Account transactions pages.</p>
              </li>
              <li>
                <p><a rev="bridgehead_4667037522" href="./bridgehead_4667037522.html">Product Details Information Subtab</a> – configure extra fields to appear on your Product Details Page.</p>
              </li>
              <li>
                <p><a rev="bridgehead_1488478244" href="./bridgehead_1488478244.html">Multi-Image Option Subtab</a> – configure multiple images in the Product Details Page based on multiple option selections.</p>
              </li>
            </ul>
          </li>
          <li>
            <p>Save the SuiteCommerce Configuration record to apply changes to your site.</p>
          </li>
        </ol>
      </div>
      <h3 id="bridgehead_4807168028">Code Examples</h3>
      <h4 id="bridgehead_4815559371">Add an Item to the Cart</h4>
      <p>The following example of a custom model adds an item to the cart. Product.Model contains the item via the <code>fetch</code> method. Based on the internal ID of the item, this code then sets the quantity and item options and adds the product to the cart via the LiveOrderModel.</p>
      <pre class="nshelp">define('AddToCart',
[
        'Product.Model'
    ,   'LiveOrder.Model'
],
function(
        ProductModel
    ,   LiveOrderModel
)
{
    var my_item_internal_id = '40'
    ,   quantity = 10
    ,   product = new ProductModel()
    // load the item.
    product.getItem().fetch({
        data: {id: my_item_internal_id}
    }).then(function (){
        // set quantity
        product.set('quantity', quantity);
        // set options
        product.setOption('option_id', 'value');
        // add to cart
        LiveOrderModel.getInstance().addProduct(product);
    });
});</pre>
      <h4 id="bridgehead_4815568969">Validate an Item for Purchase</h4>
      <p>All shopping, wishlist, and quote item validation is centralized in the Product.Model. You can validate per field and developers can override the default validation per field.</p>
      <p>The following customization uses a generic <code>areAttributesValid</code> method to validate per field. In this case <code>options</code> and <code>quantity</code>, as opposed to validating the entire set. This method uses a pre-set default validation to validate the specified fields. The <code>generateOptionalExtraValidation</code> method overrides the default validation for the any fields. In this case, <code>quantity</code>. If the values are not valid, the code introduces an alert message. Otherwise, the code adds the product to the cart.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The default validation is set in Product.Model.js.</p>
      </div>
      <br>
      <pre class="nshelp">define('ValidateAddToCart'
,   [
        'LiveOrder.Model'
    ]
,   function (
        LiveOrderModel
    )
{
    function generateOptionalExtraValidation() {
        return {
            'quantity': function (){}
        }
    }
    var product = obtainSomeProduct(/*...*/);
    var cart = LiveOrderModel.getInstance();
    if (!product.areAttributesValid(['options','quantity'], this.generateOptionalExtraValidation()))
    {
        alert('Invalid Product!')
    }
    else
    {
        cart.addProduct(product);
    }
});</pre>
      <h4 id="bridgehead_4815572568">Add Item to Wishlist</h4>
      <p>The following customization gets new item data from the product and the correct product list. It then adds the product line to the list and alerts the user upon successful validation.</p>
      <pre class="nshelp">define('ValidateForWishList'
,   [
        'ProductList.Item.Model'
    ]
,   function (
        ProductListItemModel
    )
{
    var getNewItemData = function (product, productList)
        {
            var product_list_line = ProductListItemModel.createFromProduct(product);
            product_list_line.set('productList', {
                id: productList.get('internalid')
            ,   owner: productList.get('owner').id
            });
            return product_list_line;
        };
 
    var doAddProductToList = function (product, productList, dontShowMessage)
        {
            var product_list_line_to_save = getNewItemData(product, productList);
            product_list_line_to_save.save(null, {
                validate: false
            ,   success: function (product_list_line_to_save_saved)
                {
                    alert('Product Added!');                   
                }
            });
        };
    var product = getSomeProduct(/*...*/)
    ,   product_list = getSomeProduct(/*...*/);
    doAddProductToList(product, product_list);
});</pre>
      <h4 id="bridgehead_4815577012">Get Options</h4>
      <p>The following customization returns all valid options for an item or matrix item. It then determines which item options are valid. Only valid options will render as selectable options in the browser.</p>
      <pre class="nshelp"> define('ReadProductOptions'
,   [
        'underscore'
    ]
,   function (
        _
    )
{
    var showFirstOptionWithValidValues = function (product)
    {
        //Collection of product.options
        var options_to_render = product.get('options');
        options_to_render.each(function (option)
        {
            //each option is a Model
            if (option.get('isMatrixDimension'))
            {
                var valid_values_for_selected_option = product.getValidValuesForOption(option);
                _.each(option.get('values'), function (value)
                {
                    value.set('isAvailable', _.contains(valid_values_for_selected_option, value.get('label')));
                });
            }
        });
        var first_valid_option = options_to_render.find(function (option)
            {
                return _.some(option.get('values'), function (value) { return value.get('isAvailable'); });
            });
        var selected_option = product.get('options').findWhere({cartOptionId: first_valid_option.get('cartOptionId')});
        //Note that we are always backbone Model and the value of the set option contains all details of the set value (label and internalid)
        alert('First Option with value values: ' + first_valid_option.get('cartOptionId') + ' set value: ' + selected_option.get('value').label)
    };
    showFirstOptionWithValidValues(getSomeIProduct(/*...*/));
});</pre>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307434462" href="./section_4307434462.html">SuiteCommerce Advanced Architecture</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307434574" href="./section_4307434574.html">Core Framework Technologies</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4316139763" href="./section_4316139763.html">The Modules Directory</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307437321" href="./section_4307437321.html">Dependencies</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307437539" href="./section_4307437539.html">Application Modules</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4786239896" href="./section_4786239896.html">Product Details Page Architecture</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307434462" href="./section_4307434462.html">SuiteCommerce Advanced Architecture</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

