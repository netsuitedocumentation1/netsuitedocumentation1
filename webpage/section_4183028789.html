<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N2823893" href="./book_N2823893.html">SuiteBuilder (Customization)</a>
            <a rev="chapter_4172599049" href="./chapter_4172599049.html">Custom Transactions</a>
            <a class="nshelp_navlast" rev="section_4183028789" href="./section_4183028789.html">Benefits of Custom Transaction Types</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4183028789" class="nshelp_title">Benefits of Custom Transaction Types</h1>
      <p><span id="dyn_help_feature" dynid="CUSTOMTRANSACTIONS">&nbsp;</span>   </p>
      <p>The Custom Transactions feature has the following benefits:</p>
      <ul class="nshelp">
        <li>
          <p>You can name your custom transaction types in a way that reflects your business logic. For details, see <a rev="section_4183028789" href="#bridgehead_4183033649">Custom Transaction Type Naming Enables Better Organization</a>.</p>
        </li>
        <li>
          <p>Like standard transactions, each custom transaction type can have its own numbering scheme, permissions, and workflow logic. For details, see <a rev="section_4183028789" href="#bridgehead_4183033724">Custom Transaction Types Support Key NetSuite Features</a>.</p>
        </li>
        <li>
          <p>You can create custom transaction types in multiple styles. For example, the transaction type can resemble a journal entry, or it can behave more like an expense report. For details, see <a rev="section_4183028789" href="#bridgehead_4205550514">Custom Transaction List Styles</a>.</p>
        </li>
      </ul>
      <h3 id="bridgehead_4183033649">Custom Transaction Type Naming Enables Better Organization</h3>
      <p>In your business, there may be a wide variety of events that can require an adjustment to your general ledger. For example, you may need to record adjustments for non-operational income, such as interest income that your company receives through investments. Conversely, you may need to record debits for rewards you give customers through customer loyalty programs. Without the Custom Transactions feature, your options for recording these various adjustments may be limited. One strategy might be to record all adjustments as journal entries. However, when you rely solely on the journal entry record, all of these varying transactions are grouped together in a single list view. Moreover, when employees enter journal entries, they have limited choices for distinguishing one type of journal entry from another.</p>
      <p>By contrast, with the Custom Transactions feature, you can create custom transaction types that are clearly labeled for specific purposes. With this approach, each custom transaction type has its own list view and its own menu path, which you can customize. In the following example, three custom transaction types have been added to the Accounting Center’s Financial tab.</p>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/CustomTxns.png" width="499px" height="280px">
      <p>These enhancements make it possible for employees to automatically classify each transaction when they create it. And if they want to view a list of all transactions of a particular type, they can do so using that type’s list view.</p>
      <h3 id="bridgehead_4183033724">Custom Transaction Types Support Key NetSuite Features</h3>
      <p>When you use custom transaction types, you can leverage many of the same features that are available with standard NetSuite transaction types. For example:</p>
      <ul class="nshelp">
        <li>
          <p>Each custom transaction type can have its own numbering, permissions, and workflows.</p>
        </li>
        <li>
          <p>Custom transaction types can include custom transaction fields that you define.</p>
        </li>
        <li>
          <p>You can create multiple custom entry forms for each transaction type.</p>
        </li>
        <li>
          <p>You can interact with custom transaction instances using SuiteScript. For details, see <a rev="section_4174405432" href="./section_4174405432.html">Custom Transaction</a>.</p>
        </li>
        <li>
          <p>You can interact with custom transaction instances by using SuiteTalk (Web Services). For details, see <a rev="section_4177777043" href="./section_4177777043.html">Custom Transaction</a>.</p>
        </li>
        <li>
          <p>You can interact with custom transaction instances by using the CSV Import Assistant. For details, see <a rev="section_4174435473" href="./section_4174435473.html">Custom Transactions Import</a>.</p>
        </li>
        <li>
          <p>You can reference your custom transaction types when creating saved searches. To find custom transaction types, use the Transaction search type. In the Types list, your custom transaction types are listed with standard transaction types.</p>
        </li>
        <li>
          <p>You can interact with custom transaction instances by using SuiteAnalytics Connect. To reference your custom transaction types, use the Transaction table. Use the transaction_type column to identify your custom transaction type. For more details, see <a rev="chapter_N752122" href="./chapter_N752122.html">Connect Schema</a>.</p>
        </li>
        <li>
          <p>Custom transaction types can be bundled, as described in <a rev="section_4177814825" href="./section_4177814825.html">Adding a Custom Transaction Type to a Bundle</a>. However, be aware that bundles including custom transaction types cannot be installed in NetSuite accounts running Version 2014 Release 2.</p>
        </li>
        <li>
          <p>Custom transaction types can be used in conjunction with the Custom GL Lines plug-in. With the Custom GL Lines plug-in, you can create logic that automatically creates a GL impact. For more information, see <a rev="section_4234787810" href="./section_4234787810.html">Associating a Custom Transaction Type with a Plug-in Implementation</a>.</p>
        </li>
      </ul>
      <p>Support for these features makes it possible for each transaction type to have its own unique behavior and processing. These advantages may be critical as you develop a series of discrete transaction types to meet different needs.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Custom transactions do not appear on the Deposits and Credits subtab of the Reconcile Bank Statement.</p>
      </div>
      <br>
      <p>For example, suppose you have a custom transaction type called Bad Debt, which you use to account for debts that are not collectible. For this type, you may want to restrict access to a limited set of users. You can manage this access on the Permissions subtab by using the transaction type’s Level sublist.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/BadDebtPermissions.png" width="630px" height="272px">
      <p>You may also want to create a custom workflow for this type. On the New Workflow page, custom transaction types are listed with standard transaction types.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/CustomTxnWorkflow2.png" width="630px" height="215px">
      <p>If appropriate, your workflow can reference any custom statuses that you have defined for your transaction type. Custom statuses are a unique feature of custom transaction types and are not available with standard transaction types. You create statuses by using the transaction type’s Statuses subtab.</p>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/BadDebtsStatuses.png" width="629px" height="176px">
      <h3 id="bridgehead_4205550514">Custom Transaction List Styles</h3>
      <p>The Custom Transactions feature supports multiple transaction type formats. That is, you can create transaction types in any of the following styles:</p>
      <ul class="nshelp">
        <li>
          <p><strong>Basic</strong> – Lets users record credit or debit lines to specified accounts. The corresponding account to be adjusted for balancing purposes is defined on the transaction type record. This approach is similar to the expense report transaction, which always debits the same predefined account.</p>
        </li>
        <li>
          <p><strong>Journal</strong> – Lets users record sets of debits and credits to accounts that a user manually specifies when entering the transaction. As with a standard journal entry record, the total value of credits must equal the total balance of debits.</p>
        </li>
        <li>
          <p><strong>Header only</strong> – Relies on a GL plug-in implementation to calculate the GL impact. That is, the transaction does not include a Lines sublist for users to manually enter debits and credits to specific accounts. Rather, the plug-in implementation calculates the impact based on other data. This data can consist of values that users enter on the transaction header or of values they enter on a custom form created by using SuiteScript objects.</p>
        </li>
      </ul>
      <p>For full details on the various list styles, see <a rev="section_4187530024" href="./section_4187530024.html">Understanding Custom Transaction Type List Styles</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4172599049" href="./chapter_4172599049.html">Custom Transactions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4172853747" href="./section_4172853747.html">Getting Started with Custom Transaction Types</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4172784423" href="./section_4172784423.html">Creating and Editing Custom Transaction Types</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4234787810" href="./section_4234787810.html">Associating a Custom Transaction Type with a Plug-in Implementation</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4172848282" href="./section_4172848282.html">Deleting Custom Transaction Types</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4172859930" href="./section_4172859930.html">Creating Workflows with Custom Transaction Types</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4177814825" href="./section_4177814825.html">Adding a Custom Transaction Type to a Bundle</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

