<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N1379402" href="./set_N1379402.html">Accounting</a>
            <a rev="book_N1675734" href="./book_N1675734.html">Revenue Recognition</a>
            <a rev="chapter_N1678106" href="./chapter_N1678106.html">Using Revenue Recognition</a>
            <a class="nshelp_navlast" rev="section_N1694795" href="./section_N1694795.html">Using Percent-Complete Revenue Recognition for Projects</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N1694795" class="nshelp_title">Using Percent-Complete Revenue Recognition for Projects</h1>
      <p><span id="dyn_help_feature" dynid="REVENUERECOGNITION">&nbsp;</span>   </p>
      <p>If you have a projects based business, you can recognize revenue based on the percentage of completed project work by using variable revenue recognition schedules. You can coordinate the recognition of deferred revenue with the completion of stages for your projects. This feature is available only if you use Project Management.</p>
      <p>Set up an item to associate a variable recognition template with projects you track. Then, sales that contain the service item generate a revenue recognition schedule based on the template and the linked project completion.</p>
      <p>As time worked is logged against the project and portions of the project are marked complete, journal entries are created to recognize the related revenue.</p>
      <div class="nshelp_procedure">
        <h4 id="procedure_N1694822">To recognize revenue based on project completion:</h4>
        <ol class="nshelp">
          <li>
            <p>Enable the Revenue Recognition feature. See <a rev="section_N1678353" href="./section_N1678353.html">Setting Up the Revenue Recognition Feature</a>.</p>
          </li>
          <li>
            <p>Create a variable type revenue recognition template. See <a rev="section_N1679446" href="./section_N1679446.html">Creating Revenue Recognition Templates</a>.</p>
          </li>
          <li>
            <p>Associate the variable type revenue recognition template with a service item or service items, either on the item record(s), or on individual sales orders or invoices in the Rev Rec Schedule column. See <a rev="section_N1687451" href="./section_N1687451.html">Associating Revenue Recognition Templates with Items</a>.</p>
            <p>Variable templates can be used on the same items as standard templates.</p>
          </li>
          <li>
            <p>Create an invoice or bill for the project sales order. After an invoice exists, the revenue recognition schedule is generated with forecast revenue lines.</p>
          </li>
          <li>
            <p>Create revenue recognition journal entries as needed. Running revenue recognition updates the schedule based on the changes in planned and actual time for the project.</p>
          </li>
        </ol>
      </div>
      <p>For information about the variable revenue recognition schedule, see <a rev="section_3791187429" href="./section_3791187429.html">Working with Variable Revenue Recognition Schedules</a>.</p>
      <h3 id="bridgehead_N1694882">Additional Revenue Recognition Accounting Preferences</h3>
      <p>When you use the <strong>Use System Percentage of Completion for Schedules</strong> preference, NetSuite automatically determines the percentage of a project that has been completed based on time logged against the project. If the Percent Complete field in the project record has a value, it is used instead of the automatic calculation. If you do not select this preference, you must enter the percent complete value on the project record.</p>
      <p>If you use the <strong>Adv. Billling: Use Sales Order Amount</strong> preference, revenue is recognized based on the percent complete in relation to the sales order rather than in relation to the invoice.</p>
      <p>For more information about these and other accounting preferences related to revenue recognition, see the <a rev="section_N1385293" href="./section_N1385293.html#bridgehead_N1386651">Revenue Recognition</a> section in <a rev="section_N1385293" href="./section_N1385293.html">General Accounting Preferences</a>.</p>
      <div class="nshelp_procedure">
        <h4 id="procedure_N1694894">To set preferences for revenue recognition:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to <span id="dyn_help_taskpath" dynid="ADMI_ACCTSETUP">&nbsp;</span>.</p>
          </li>
          <li>
            <p>Click the <strong>General</strong> subtab.</p>
          </li>
          <li>
            <p>Scroll to the Revenue Recognition section, and check the preferences you want to use.</p>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <h3 id="bridgehead_N1694960">Percent-Complete Based Recognition Journal Entries</h3>
      <p>The Create Revenue Recognition Journal Entries page includes revenue due to post from variable schedules based on project completion.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">Always create revenue recognition journal entries in chronological order when using percent-complete revenue recognition for projects.</p>
      </div>
      <br>
      <p>For the period you select, NetSuite determines the amount due to be recognized for each schedule based on project completion. It calculates project completion based on entered and approved project time entries. The Project record shows the percentage of completion for the project in that period, and NetSuite uses that percentage to determine the revenue due to post.</p>
      <p>A variable schedule does <strong>not</strong> show in the list if:</p>
      <ul class="nshelp">
        <li>
          <p>It has a zero balance to recognize for the selected period.</p>
        </li>
        <li>
          <p>Revenue recognition journal entries have been created for the period and the planned or actual time worked for the project has not changed since that point.</p>
        </li>
      </ul>
      <p>Revenue recognition journal entries typically debit a deferred revenue account and credit a revenue account. In some cases, however, a journal entry may decrease the revenue recognized. In these cases, the revenue recognition journal entries credit a deferred revenue account and debit a revenue account.</p>
      <div class="nshelp_informalexample">
        <p>For example, the time estimate to complete project Alpha is 100 hours. During period One, 50 hours are logged for project Alpha. 50 hours = 50% of completion and 50% of the project revenue is recognized.</p>
        <p>Then, during period Two, the time estimate is increased to 200 hours. The 50 hours logged against the project now equals 25% completion and the revenue recognized needs to be decreased. NetSuite generates a journal entry that credits the deferred revenue account and debits the revenue account to correct the amount of revenue recognized previously. On the Variable Revenue Recognition Schedule, the journal entry line displays a negative amount.</p>
      </div>
      <p>These schedules coordinate the recognition of deferred revenue with the stages of completion of an associated project.</p>
      <p>As time worked is logged against the project and portions of the project are marked complete, journal entries become due to post to recognize the related revenue. For more information, see <a rev="section_3791187429" href="./section_3791187429.html">Working with Variable Revenue Recognition Schedules</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1678106" href="./chapter_N1678106.html">Using Revenue Recognition</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1678353" href="./section_N1678353.html">Setting Up the Revenue Recognition Feature</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1679446" href="./section_N1679446.html">Creating Revenue Recognition Templates</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1687451" href="./section_N1687451.html">Associating Revenue Recognition Templates with Items</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1689004" href="./section_N1689004.html">Working with Revenue Recognition Schedules</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1691981" href="./section_N1691981.html">Working with Revenue Recognition Journal Entries</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1695172" href="./section_N1695172.html">Revenue Recognition Reports</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1179876" href="./chapter_N1179876.html">Using Project Management</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

