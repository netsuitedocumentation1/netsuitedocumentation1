<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N1379402" href="./set_N1379402.html">Accounting</a>
            <a rev="book_N1675734" href="./book_N1675734.html">Revenue Recognition</a>
            <a rev="chapter_4328435538" href="./chapter_4328435538.html">Using Advanced Revenue Management</a>
            <a rev="section_4328435754" href="./section_4328435754.html">Setting Up Advanced Revenue Management</a>
            <a rev="section_4703499262" href="./section_4703499262.html">Transition to the New Revenue Recognition Standard</a>
            <a class="nshelp_navlast" rev="section_4720683465" href="./section_4720683465.html">Migrating Open Revenue Transactions</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4720683465" class="nshelp_title">Migrating Open Revenue Transactions</h1>
      <p><span id="dyn_help_feature" dynid="ADVANCEDREVENUERECOGNITION">&nbsp;</span>   </p>
      <p></p>
      <p>Migrating open revenue transactions is step 5 in the overall transition to the ASC 606/IFRS 15 standard described in <a rev="section_4703499262" href="./section_4703499262.html">Transition to the New Revenue Recognition Standard</a>. When the migration is complete, revenue recognition plans are updated and a one-time adjustment is added to the revenue plans in the current period. The one-time adjustment reflects the difference between the current standard and the new standard. The adjustment posts to the revenue adjustment account you select during migration at the time revenue recognition journal entries are generated for the period.</p>
      <p>For migration purposes, revenue transactions are represented by revenue arrangements. A revenue arrangement is considered open if it includes a revenue element whose revenue has not been fully recognized by the cut-off date. An open revenue arrangement may include other revenue elements for which all revenue has already been recognized. These closed revenue elements must be reprocessed under the new standard with the rest of the revenue arrangement.</p>
      <p>If you have enabled advanced cost amortization, the values in Expense Amortization Plan subtab are also updated during the migration. The one-time adjustment for the difference between the standards is included in the same journal entry as the one-time revenue adjustment. You can select the expense adjustment account at the time you run the migration.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The hidden <strong>Enable Migrate Open Revenue Transactions</strong> preference must be checked to access the Migrate Open Revenue Transactions page. The assistance of NetSuite Professional Services or a qualified NetSuite partner is required for the transition process. Professional Services or a partner must contact Technical Support to request that the preference be enabled.</p>
      </div>
      <br>
      <h4 id="bridgehead_4724890590">Prerequisites</h4>
      <p>Before you begin the migration, you must have already completed the following steps:</p>
      <ol class="nshelp">
        <li>
          <p>Change revenue arrangement and revenue plan processing to manual in the accounting preferences as shown.</p>
          <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/Accounting/RevenueRecognition/ManualAccountingPreferences.png" width="322px" height="110px">
        </li>
        <li>
          <p>Process all revenue contracts, including revenue recognition journal entries and reclassification, up to the cut-off date under the current standard:</p>
          <ol class="nshelp">
            <li>
              <p>Go to <span id="dyn_help_taskpath" dynid="TRAN_MANAGEREVENUEARRANGEMENTS">&nbsp;</span> to open the Update Revenue Arrangements and Revenue Plans page.</p>
            </li>
            <li>
              <p>Set the <strong>Source To</strong> date to the last day of your cut-off period.</p>
              <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/Accounting/RevenueRecognition/UpdateToCutOff.png" width="586px" height="127px">
            </li>
          </ol>
        </li>
        <li>
          <p>Close all accounting periods prior to and including the cut-off date.</p>
        </li>
        <li>
          <p>Reconfigure items and default revenue allocation rules according to the new standard.</p>
        </li>
        <li>
          <p>Create a revenue arrangement saved search that includes all the arrangements you want to migrate. You may use the same saved search as you use to schedule reclassification journal entries. You may include revenue arrangements from more than one accounting book if you are using multi-book accounting. Adjust the search to include only open revenue arrangements if desired.</p>
        </li>
      </ol>
      <p>When you have completed the previous steps, you are ready to migrate your open revenue transactions to the new standard. A revenue arrangement is considered open if it includes a revenue element whose revenue has not been fully recognized by the cut-off date. An open revenue arrangement may include other revenue elements for which all revenue has already been recognized.</p>
      <div class="nshelp_procedure">
        <h4>To migrate open revenue transactions:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to <span id="dyn_help_taskpath" dynid="TRAN_MIGRATEOPENREVENUETRANSACTIONS">&nbsp;</span>. If this navigation path does not exist in your account, the preference has not been enabled.</p>
          </li>
          <li>
            <p>In the <strong>Saved Search</strong> list, select the revenue arrangement saved search that includes the criteria of the revenue arrangements you want to migrate. If you want to migrate revenue arrangements in smaller batches, use the saved search to filter the arrangements that are processed. For example, create separate saved searches for each subsidiary or each accounting book.</p>
          </li>
          <li>
            <p>In the <strong>Transition Period</strong> field, select your cut-off period. The cut-off date is the last day of the period you select. The default is the last period of the current fiscal year.</p>
          </li>
          <li>
            <p>By default, the changes resulting from the migration are posted in a one-time adjustment to the system retained earnings account. You can select a different account as follows:</p>
            <ul class="nshelp">
              <li>
                <p><strong>Revenue Migration Adjustment Account</strong> – Select a different account to use for the one-time adjustment to revenue. Select the blank option to post the adjustment to each item’s revenue account. If the field displays a popup list rather than a dropdown list, to select the blank option, delete the value. A &lt;Type then tab&gt; prompt appears, but the field is blank.</p>
              </li>
              <li>
                <p><strong>Expense Migration Adjustment Account</strong> - This field is available only when the accounting preference Enable Advanced Cost Amortization is checked. Select a different account to use for the one-time adjustment to expense. Select the blank option to post the adjustment to each item’s expense account. If the field displays a popup list rather than a dropdown list, to select the blank option, delete the value. A &lt;Type then tab&gt; prompt appears, but the field is blank.</p>
              </li>
            </ul>
            <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/Accounting/RevenueRecognition/MigrateOpenRevenueTransactions.png" width="608px" height="170px">
          </li>
          <li>
            <p>Click <strong>Submit</strong>.</p>
            <p>The Process Status page opens and displays the status of the Migrate Open Revenue Transactions process. Click <strong>Refresh</strong> as needed to update the results.</p>
          </li>
        </ol>
      </div>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4328435538" href="./chapter_4328435538.html">Using Advanced Revenue Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4328435754" href="./section_4328435754.html">Setting Up Advanced Revenue Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4703499262" href="./section_4703499262.html">Transition to the New Revenue Recognition Standard</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

