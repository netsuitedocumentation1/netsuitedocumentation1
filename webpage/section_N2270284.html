<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2249433" href="./book_N2249433.html">Inventory Management</a>
            <a rev="chapter_N2250682" href="./chapter_N2250682.html">Inventory Management</a>
            <a class="nshelp_navlast" rev="section_N2270284" href="./section_N2270284.html">Bin Management</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2270284" class="nshelp_title">Bin Management</h1>
      <p><span id="dyn_help_feature" dynid="BINMANAGEMENT">&nbsp;</span>   </p>
      <p>You can use bin management to identify places in your warehouse where you store inventory items. Bins help you track on-hand quantities within a warehouse. Tracking items by bins can help organize receiving items and simplify picking items to fulfill orders.</p>
      <p>For example, if you use bins, when you receive a purchase order, the order can tell you which bin to put the items away in. Then, your stock level of that item in that bin is tracked.</p>
      <p>When you enter the item on a sale, you can specify the bin to pull the item from based on available quantities. This helps warehouse employees know exactly where to go to find the quantity of items they need when picking and fulfilling an order. They also know exactly what items need to be put away, or stocked, and where, after they are received from vendors.</p>
      <p>There are two NetSuite features for bin management:</p>
      <ul class="nshelp">
        <li>
          <p><strong>Bin Management</strong> – A basic means of tracking inventory in bins. This feature requires that you associate bins with items before you can use bins on transactions. This feature does not allow using bins with serialized and lot numbered items or on a per-location basis.</p>
          <p>Read <a rev="section_N2271509" href="./section_N2271509.html">Basic Bin Management</a>.</p>
        </li>
        <li>
          <p><strong>Advanced Bin / Numbered Inventory Management</strong> – An enhanced version of tracking bins, including for serial numbered and lot numbered items and on a per-location basis. Using this feature, you are not required to pre-associate bins with items to use bins on transactions. Also, you are allowed to associate bins with serialized and lot numbered items or use bins on a per-location basis.</p>
          <p>Read <a rev="section_N2271791" href="./section_N2271791.html">Advanced Bin / Numbered Inventory Management</a>.</p>
        </li>
      </ul>
      <p>The information in the sections below apply whether you use the basic Bin Management feature or use the Advanced Bin / Numbered Inventory Management feature.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Some sections below mention various preferences available to be used with bins. For details about using these preferences, read <a rev="section_N2273755" href="./section_N2273755.html">Setting Bin Preferences</a>.</p>
      </div>
      <br>
      <h3 id="bridgehead_N2270434">Putting Away Items in Bins</h3>
      <p>To put away an item immediately, you can edit the quantities placed in each bin. If you have enabled the <strong>Use Preferred Bin on Item Receipts</strong> preference, all items are placed in the preferred bin for that location by default.</p>
      <p>To put away an item later, set the quantity for the preferred bin to zero. The item is included with the preferred bin number on the putaway sheet.</p>
      <p>Read more in the topic <a rev="section_N2274903" href="./section_N2274903.html">Bin Putaway Worksheet</a>.</p>
      <h3 id="bridgehead_N2270486">Receiving with Bins</h3>
      <p>When a purchase order or customer return is received, bin numbers are included on the following transactions:</p>
      <ul class="nshelp">
        <li>
          <p>Cash Refunds</p>
        </li>
        <li>
          <p>Credit Card purchases</p>
        </li>
        <li>
          <p>Checks</p>
        </li>
        <li>
          <p>Vendor Bills</p>
        </li>
        <li>
          <p>Inventory Adjustments</p>
        </li>
        <li>
          <p>Inventory Transfers</p>
          <img class="nshelp_diagram_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/InventoryManagement/binreceiving.png" width="608px" height="344px">
        </li>
      </ul>
      <h3 id="bridgehead_N2271127">Bins on Sales</h3>
      <p>When a sales order is placed by a customer, the bin numbers are included on the following transactions:</p>
      <ul class="nshelp">
        <li>
          <p>Picking tickets</p>
        </li>
        <li>
          <p>Item fulfillments</p>
        </li>
        <li>
          <p>Invoices</p>
        </li>
      </ul>
      <p>After an item is added to one or more bins, you can transfer items between bins, if needed. For more information, read <a rev="section_N2278346" href="./section_N2278346.html">Bin Transfers</a>.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">If you use the Basic Bin Management feature along with the Multi-Location Inventory feature, you must either use bins in all locations or in no locations. This means, when using bins with Basic Bin Management, all locations must have at least one bin. If you use the Advanced Bin / Numbered Inventory Management feature with the Multi-Location Inventory feature, you can use bins on a per-location basis. Any location that uses bins must have at least one bin.</p>
      </div>
      <br>
      <h3 id="bridgehead_N2271186">Bins on Picking Tickets</h3>
      <p>Your employees can print the picking tickets to automatically find the bins for the items needed to fill the order. Bins are included on a picking ticket using the following logic:</p>
      <ul class="nshelp">
        <li>
          <p>If the preferred bin has sufficient quantity to pick item, only the preferred bin is printed on the picking ticket.</p>
        </li>
        <li>
          <p>If multiple bins are required to find the quantity needed for an item on the order, each bin is listed on the picking ticket with the preferred bin listed first.</p>
        </li>
        <li>
          <p>If the preferred bin has a quantity of 0 and no other single bin has sufficient quantity to pick all items on the order, bins are listed in order of descending quantity.</p>
        </li>
        <li>
          <p>Only bins with quantity greater than or equal to the quantity ordered are printed on the picking ticket.</p>
        </li>
        <li>
          <p>A Bin Number column is only included on a picking ticket if the ticket includes items with associated bin numbers.</p>
        </li>
      </ul>
      <p>If the employee picking the order pulls a different quantity from a bin than is listed on the picking ticket, he or she can edit the quantity taken from each bin on the item fulfillment page.</p>
      <img class="nshelp_diagram_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/InventoryManagement/binpicking.png" width="630px" height="373px">
      <h3 id="bridgehead_N2271260">Data Per Bin</h3>
      <p>Using either bins feature, item costing is <em>not</em> calculated per bin. Only on-hand quantity is tracked per bin. Available, committed, backordered, and ordered quantities are also not tracked per bin.</p>
      <h3 id="bridgehead_N2271276">Specifying Bins on Transactions</h3>
      <p>Bins are required on all cash sales, invoices, and negative inventory adjustments with bin items. This is required regardless of whether the <strong>Require Bins on All Transactions Except Item Receipts</strong> preference is enabled.</p>
      <p>For example, if you are going to enter a cash sale with a bin item that specifies a location, the item is required to have at least one bin in that location. Also, when you enter an inventory adjustment, any line that deducts a quantity of an item that uses bins will require a bin.</p>
      <p>This also means that when you edit a previously existing sale transaction that has a bin item but no bin specified, bins must then be specified on the transaction.</p>
      <p>If you do not specify a bin, you see the notice: “The number of bins entered (0) is not equal to the item quantity (x)”.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Unless the <strong>Require Bins on All Transactions Except Item Receipts</strong> preference is enabled, NetSuite does not require bins on positive adjustments or purchases because you can use a Bin Putaway Worksheet later.</p>
      </div>
      <br>
      <h3 id="bridgehead_N2271313">Bins and Assembly Items</h3>
      <p>If you use the Assembly Items feature, you must designate a bin for any component item in a build which uses bins. If a parent assembly item uses bins, you must designate a bin for that item to unbuild it.</p>
      <p>Unless the <strong>Require Bins on All Transactions Except Item Receipt</strong> preference is enabled, you are not required to designate a bin for a member item in an unbuild or for an assembly item in a build.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics:</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2271509" href="./section_N2271509.html">Basic Bin Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2272738" href="./section_N2272738.html">Setting Up Bin Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2273046" href="./section_N2273046.html">Enabling Basic Bin Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2273755" href="./section_N2273755.html">Setting Bin Preferences</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2274082" href="./section_N2274082.html">Creating Bin Records</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2274433" href="./section_N2274433.html">Setting Up Item Records for Bins</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2274903" href="./section_N2274903.html">Bin Putaway Worksheet</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2278346" href="./section_N2278346.html">Bin Transfers</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2271791" href="./section_N2271791.html">Advanced Bin / Numbered Inventory Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2273336" href="./section_N2273336.html">Enabling Advanced Bin / Numbered Inventory Management</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

