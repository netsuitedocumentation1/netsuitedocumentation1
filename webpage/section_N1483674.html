<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N1379402" href="./set_N1379402.html">Accounting</a>
            <a rev="preface_3710627041" href="./preface_3710627041.html">General Accounting</a>
            <a rev="chapter_N1468455" href="./chapter_N1468455.html">Working with Journal Entries</a>
            <a rev="section_N1483457" href="./section_N1483457.html">Expense Allocation Overview</a>
            <a class="nshelp_navlast" rev="section_N1483674" href="./section_N1483674.html">Creating Expense Allocation Schedules</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N1483674" class="nshelp_title">Creating Expense Allocation Schedules</h1>
      <p></p>
      <p></p>
      <p></p>
      <p><span id="dyn_help_feature" dynid="EXPENSEALLOCATION">&nbsp;</span>   </p>
      <p>You create allocation schedules to manage the allocation of expenses after they are incurred. Allocation schedules distribute expenses across departments, locations, classes, and custom segments, saving you the time required to enter complex journal entries.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">Your user role must have Edit or Full level of the Create Allocation Schedules permission to use Expense Allocation.</p>
      </div>
      <br>
      <p>With Expense Allocation, you can allocate:</p>
      <ul class="nshelp">
        <li>
          <p>from one account or from multiple accounts</p>
        </li>
        <li>
          <p>to specific department class or locations within expense accounts</p>
        </li>
        <li>
          <p>from one account to multiple accounts</p>
        </li>
      </ul>
      <p>The Dynamic Allocation feature (<span id="dyn_help_taskpath" dynid="ADMI_FEATURES">&nbsp;</span> &gt; Accounting under Advanced Features) extends the existing fixed rate revenue and cost allocation capability. With fixed rate allocation, you must specify the fixed allocation weight for the entire life cycle of any allocation schedule. Dependent upon the Statistical Accounts feature (<span id="dyn_help_taskpath" dynid="ADMI_FEATURES">&nbsp;</span> &gt; Accounting under Advanced Features), Dynamic Allocation enables you to assign any statistical account to any Single or Intercompany Allocation Schedule. Statistical account assignment is useful in advanced costing such as Activity Based Costing and Usage Based Costing, and when you are running cost centers and profit centers. The weight for the allocation, based on the balance of that statistical account through statistical journals or as an absolute value, is dynamically calculated when the allocation journal is generated. To calculate statistical weight, NetSuite uses the flat amount for each destination line in the allocation schedule, rather than dividing by the total amount entered in all of the destination lines.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">You can control the period of time that NetSuite uses to calculate the balance (weight timeline) by specifying the timeline end date through the Next Date field. This end date can be the date on which the schedule runs (system date), or a past or future date. When you select the weight basis (specific date, period to date, quarter to date, or year to date), your weight timeline is relative to the date you enter in the Next Date field. This is useful when you want an allocation schedule to calculate a statistical account balance for a period of time prior to or after the run date. For example, you want to run your allocation schedule on March 3, 2015 (system date) for the February previous period, February 1 through 28, 2015. You also specify the Next Date field as February 28, 2015 to synchronize the source and weight basis timeline to February 1 – 28, 2015. With the new synchronized weight enhancement, the source timeline, created journals, <strong>and</strong> the weight timeline are driven by the value in the Next Date field. For more information, see <a rev="chapter_3866895958" href="./chapter_3866895958.html">Working with Allocation Schedules Weighted by the Balance of a Statistical Account</a>.</p>
      </div>
      <br>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">If you intend to create dynamic allocation schedules based on a statistical account, the segments you define in the statistical account are the only segments that can be used in the allocation schedule. For example, if you want to allocate cost by subsidiary and department, the statistical account must also be segmented by subsidiary and department.</p>
      </div>
      <br>
      <p>When you are creating an expense allocation schedule, keep in mind the following questions to set up a schedule to best meet your needs:</p>
      <ul class="nshelp">
        <li>
          <p>Is the amount being allocated from one account or several accounts?</p>
          <p>On the Source subtab, you can choose one or more accounts from which to take an amount.</p>
        </li>
        <li>
          <p>When you allocate the amount from this source, do you want to move the amount or allocate for reporting purposes?</p>
          <ul class="nshelp">
            <li>
              <p>To add the amount to the destination account <strong>and zero the balance in the source account</strong>, only select an account in the Account field on the Source subtab. <strong>Do not</strong> choose a Credit Account.</p>
              <p>When you do not select a credit account, an offsetting credit <strong>is not</strong> created, so the amount is transferred out of the source account.</p>
            </li>
            <li>
              <p>To add the amount to the destination account <strong>and leave the balance in the source account</strong>, select an account in the Account field on the Source subtab <strong>and</strong> choose a Credit Account.</p>
              <p>Selecting a credit account creates an offsetting credit so the amount is not transferred out of the source. Doing so enables you to look at the expense multiple ways on the Income Statement. For example, you can allocate portions of one rent expense (source) account into several departments (destination). This enables you to view expense reports by department for budget planning, but leaves the actual expense amount in the source account. For example, to allocate $10,000 from the Rent Expense account to several departments.</p>
              <p>Whether you choose a credit account determines what happens to the amount you allocate from the source account:</p>
              <ul class="nshelp">
                <li>
                  <p>When you <strong>do</strong> choose a credit account, $10,000 is allocated to the appropriate destination accounts <strong>and</strong> the $10,000 balance remains in the Rent Expense account because an offsetting credit is created</p>
                </li>
                <li>
                  <p>When you <strong>do not</strong> choose a credit account, $10,000 is allocated to the appropriate destination accounts to credit the rent expense and the source account balance is reduced to zero.</p>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
      <ul class="nshelp">
        <li>
          <p>Is the amount being allocated into one account or several accounts?</p>
          <p>On the Destination subtab, you can choose one or more accounts into which to add an amount.</p>
        </li>
        <li>
          <p>If the Multi-Book Accounting feature is provisioned in your account, you can create an expense allocation schedule for a secondary accounting book. NetSuite first calculates the source balance to obtain the destination balance in the secondary book. Then, after the allocation schedule runs, NetSuite sets the source balance to zero. When you create an expense allocation schedule for all active accounting books, NetSuite generates book-specific allocation journal entries for open accounting periods for all active accounting books.</p>
        </li>
        <li>
          <p>If you enable the <strong>Values are percentages</strong> option, you can enter a percentage as a Weight value for each destination account or custom segment, department, class, location, and custom segment.</p>
        </li>
        <li>
          <p>If you enable the <strong>Use source/credit account</strong> option, the destination lines should be custom segments, departments, classes, locations, and custom segments within accounts specified on the Source subtab.</p>
          <div class="nshelp_note">
            <h3>Note</h3>
            <p class="nshelp_first">If you are creating a dynamic allocation schedule for a statistical account, the <strong>Destination</strong> subtab includes the <strong>Update Sample Weights</strong> button. Clicking this button calculates and displays the <strong>Sample Weight</strong> and <strong>Balance</strong> values for each destination line.</p>
          </div>
          <br>
        </li>
      </ul>
      <p>After you know the answer to these questions, follow the steps below to set up your allocation schedule.</p>
      <p>When you use NetSuite OneWorld in conjunction with the Expense Allocation feature, you can create an intercompany allocation schedule. Intercompany schedules allocate a balance from one source subsidiary to multiple destination subsidiaries for costs that are shared between subsidiaries on a regular basis such as rent utilities. For more information, see <a rev="section_N1484654" href="./section_N1484654.html">Creating Intercompany Allocation Schedules</a>.</p>
      <div class="nshelp_procedure">
        <h4 id="procedure_N1483932">To create an expense allocation schedule:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to <span id="dyn_help_taskpath" dynid="EDIT_ALLOCATION">&nbsp;</span>.</p>
          </li>
          <li>
            <p>In the Primary Information section:</p>
            <ol type="a" class="nshelp_substeps">
              <li>
                <p>Enter a name for this allocation schedule.</p>
              </li>
              <li>
                <p>If you use NetSuite OneWorld, select the subsidiary with which this expense allocation schedule should be associated.</p>
                <div class="nshelp_note">
                  <h3>Note</h3>
                  <p class="nshelp_first">If the subsidiary you select is assigned to one or more shared vendor records, you can specify that the source account and destination account belong to any of the vendors to which the selected subsidiary is assigned. To do this, on the <strong>Source</strong> and <strong>Destination</strong> subtabs respectively, select the shared vendor from the <strong>Name</strong> field. For more information about shared vendor records, see <a rev="section_4180576581" href="./section_4180576581.html">Sharing Vendors with Multiple Subsidiaries</a>.</p>
                </div>
                <br>
              </li>
              <li>
                <p>If you use Multi-Book Accounting, select an accounting book from the list.</p>
                <p>You may select the primary book or any secondary book to which you have access.</p>
              </li>
              <li>
                <p>In the <strong>Frequency</strong> field, choose how often you want to reallocate expenses from this account.</p>
                <p>You are not required to enter a date in the <strong>Next Date</strong> field if you choose&nbsp;<strong>End of Period</strong>&nbsp;as the allocation frequency.</p>
                <p>Only those allocation schedules where the selected <strong>Frequency</strong> is <strong>Run by Batch</strong> can be included in an allocation batch. For more information, see <a rev="section_3867806074" href="./section_3867806074.html">Creating an Allocation Batch</a>.</p>
              </li>
              <li>
                <p>In the <strong>Next Date</strong> field, enter the date of the next scheduled allocation.</p>
                <p>You can control the period of time that NetSuite uses to calculate the balance (weight timeline) by specifying the timeline end date through this field. This end date can be the date on which the schedule runs (system date), or a past or future date. When you select the weight basis (specific date, period to date, quarter to date, or year to date), your weight timeline is relative to the date you enter in the <strong>Next Date</strong> field.</p>
              </li>
              <li>
                <p>In the <strong>Subsequent Date</strong> field, enter the date of the following allocation.</p>
              </li>
              <li>
                <p>If you chose <strong>Twice a Month</strong> in the <strong>Frequency</strong> field, enter the day of the month that the second allocation occurs.</p>
              </li>
              <li>
                <p>Choose one of the following:</p>
                <ul class="nshelp">
                  <li>
                    <p><strong>Remind Forever</strong> – Choose this option if you want to indefinitely reallocate based on this schedule.</p>
                  </li>
                  <li>
                    <p><strong>Number Remaining</strong> – Choose this option if you want to limit the number of times this schedule reallocates expenses. Enter the number of reallocations you want this schedule to make.</p>
                  </li>
                </ul>
              </li>
              <li>
                <p>Select or clear the <strong>Inactive</strong> box to enable or deactivate the allocation.</p>
                <p>This option does not delete the allocation schedule.</p>
              </li>
              <li>
                <p>In the <strong>Allocation Mode</strong> field, choose whether the schedule is a fixed rate or dynamic allocation.</p>
                <p>If you choose <strong>Dynamic Allocation</strong>, complete the following fields:</p>
                <ol class="nshelp">
                  <li>
                    <p>In the <strong>Weight Source</strong> field, choose the statistical account upon which this dynamic allocation schedule is based.</p>
                    <p>The <strong>Unit Type</strong> field displays the unit of measure type associated with the selected statistical account.</p>
                    <p>The <strong>Unit of Measure</strong> field displays the base unit assigned to the <strong>Unit Type</strong>.</p>
                  </li>
                  <li>
                    <p>In the <strong>Date Basis</strong> field, choose the method by which the system should sum the statistical journals for weight calculation.</p>
                    <p>Available options include:</p>
                    <ul class="nshelp">
                      <li>
                        <p><strong>As of Date</strong> – The system sums all statistical journals from the beginning date to the day before the date you enter in the <strong>Next Date</strong> field.</p>
                      </li>
                      <li>
                        <p><strong>Period to Date</strong> – The system sums all statistical journals from the first day of the Accounting Period to the day before the date you enter in the <strong>Next Date</strong> field.</p>
                      </li>
                      <li>
                        <p><strong>Quarter to Date</strong> – The system sums all statistical journals from the first day of the quarter to the day before the date you enter in the <strong>Next Date</strong> field.</p>
                      </li>
                      <li>
                        <p><strong>Year to Date</strong> – The system sums all statistical journals from the first day of the year to the day before the date you enter in the <strong>Next Date</strong> field.</p>
                      </li>
                    </ul>
                  </li>
                </ol>
              </li>
            </ol>
          </li>
          <li>
            <p>Complete the <strong>Source</strong> subtab:</p>
            <ol type="a" class="nshelp_substeps">
              <li>
                <p>If you want to create an offsetting credit for the amount you are allocating, select the account for the offsetting credit to post to in the <strong>Credit Account</strong> field.</p>
                <ul class="nshelp">
                  <li>
                    <p>If you <strong>do</strong> choose a Credit Account:</p>
                    <p>The schedule creates an offsetting credit to balance the allocation so the amount remains in the source account and is not transferred out of the source account by the allocation journal entry. The schedule adds the amount to the destination account for reporting purposes and leaves the actual balance in the source account.</p>
                  </li>
                  <li>
                    <p>If you <strong>do not</strong> choose a Credit Account:</p>
                    <p>The schedule zeroes the balance in the source account when the amount is transferred into the destination account by the allocation journal entry because an offsetting credit is not created.</p>
                  </li>
                </ul>
              </li>
              <li>
                <p>If you are creating an offsetting credit for the credit account selected, associate the offsetting credit with a project or entity by selecting them in the <strong>Credit Name</strong> field.</p>
              </li>
              <li>
                <p>If you are creating an offsetting credit for the credit account selected, associate the offsetting credit with a department, class, and location by selecting them in the <strong>Credit Location</strong>, <strong>Credit Class</strong>, and <strong>Credit Department</strong> fields.</p>
                <p>To associate an offsetting credit with a department, class, and location, the <strong>Allow Per-Line Classifications on Journals</strong> preference must be enabled. See <a rev="bridgehead_N1469649" href="./bridgehead_N1469649.html">Class, Department, Location Journal Entry Preferences</a>.</p>
              </li>
              <li>
                <p>In the <strong>Account</strong> column, choose the account or the type of account you are allocating with this schedule.</p>
                <p>If you choose an account type instead of a specific account, all accounts of this type are allocated with this schedule.</p>
              </li>
              <li>
                <p>In the <strong>Name</strong> column, select a customer, employee, project, or vendor to associate the allocation from the chosen account (d).</p>
              </li>
              <li>
                <p>Choose the department, class, location, or custom segment to associate with allocation from the chosen account (d).</p>
              </li>
              <li>
                <p>Click <strong>Add</strong>.</p>
              </li>
              <li>
                <p>Repeat these steps for each account that you want to allocate with this schedule.</p>
              </li>
            </ol>
          </li>
          <li>
            <p>Complete the <strong>Destination</strong> subtab:</p>
            <ol type="a" class="nshelp_substeps">
              <li>
                <p>If you want to allocate expenses into custom segments, departments, classes, and locations by percentage, check the <strong>Values are percentages</strong> box.</p>
              </li>
              <li>
                <p>If you want to allocate to custom segments, departments, classes, and locations within the source account, check the <strong>Use source/credit accounts</strong> box. If you check this box, you cannot choose destination accounts because the allocation distributes the expenses among custom segments, departments, classes, and locations rather than different accounts.</p>
                <p>If you are creating a dynamic allocation schedule for a statistical account, the <strong>Destination</strong> subtab includes <strong>Update Sample Weights</strong>. Clicking this button calculates and displays the sample weight and balance for each destination line.</p>
                <p>From the <strong>Name</strong>, <strong>custom segment</strong>, <strong>Department</strong>, <strong>Location</strong>, and <strong>Class</strong> fields you can select <strong>—Auto Populate—</strong>. Rather than entering a fixed value, NetSuite searches all existing values at the time the allocation journal is generated and completes these fields according to the gathered information. You can define four fields as <strong>—Auto Populate—</strong>.</p>
                <div class="nshelp_note">
                  <h3>Note</h3>
                  <p class="nshelp_first">If <strong>Name</strong>, <strong>custom segment</strong>, <strong>Department</strong>, <strong>Location</strong>, and <strong>Class</strong> are not defined at the line level on the field set to <strong>—Auto Populate—</strong>, NetSuite does not display the line because there is no assignment.</p>
                </div>
                <br>
              </li>
              <li>
                <p>If you are entering values as percentages, you can use the following buttons to distribute the allocated amounts:</p>
                <ul class="nshelp">
                  <li>
                    <p><strong>Normalize</strong> – This button makes total weight add up to 100%.</p>
                  </li>
                  <li>
                    <p><strong>Even Spread</strong> – This button makes all the weights equal.</p>
                  </li>
                </ul>
              </li>
              <li>
                <p>If you are allocating to other accounts, select an account in the <strong>Account</strong> column.</p>
              </li>
              <li>
                <p>In the <strong>Name</strong> column, select a customer, employee, project, or vendor to associate the allocation to the account on this line.</p>
              </li>
              <li>
                <p>Choose the location, department, class, or custom segment to which you want to allocate expenses.</p>
              </li>
              <li>
                <p>In the <strong>Weight</strong> column, enter:</p>
                <ul class="nshelp">
                  <li>
                    <p>a percentage, if you checked the <strong>Values are percentages</strong> box</p>
                  </li>
                  <li>
                    <p>a flat amount that is divided by the total amount of all entered lines to determine allocation</p>
                    <p>For example, if you enter 100, 300, 400 on three lines, the system would allocate the source amount to the destination lines based on the calculated percentage: 12.5% (100/800), 37.5% (300/800), 50% (400/800).</p>
                    <div class="nshelp_note">
                      <h3>Note</h3>
                      <p class="nshelp_first">If you are creating a dynamic allocation schedule for a statistical account, NetSuite calculates the statistical weight using the flat amount for each destination line in the allocation schedule, rather than dividing by the total amount entered in all of the destination lines. The statistical weight is calculated when the allocation schedule is running and then used in allocation journal generation.</p>
                    </div>
                    <br>
                  </li>
                </ul>
              </li>
            </ol>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <p>After you create an expense allocation schedule, journal entries are created whenever you have a scheduled allocation. For more information, read <a rev="section_N1484411" href="./section_N1484411.html">Creating Expense Allocation Journal Entries</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N1468455" href="./chapter_N1468455.html">Working with Journal Entries</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1483457" href="./section_N1483457.html">Expense Allocation Overview</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1484654" href="./section_N1484654.html">Creating Intercompany Allocation Schedules</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1484411" href="./section_N1484411.html">Creating Expense Allocation Journal Entries</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_3869578910" href="./section_3869578910.html">Viewing the Details of Allocation Schedules</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1485952" href="./section_N1485952.html">Assigning Expense Allocations to Projects and Entities</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_3867806074" href="./section_3867806074.html">Creating an Allocation Batch</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

