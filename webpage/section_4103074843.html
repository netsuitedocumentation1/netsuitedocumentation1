<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2723865" href="./book_N2723865.html">SuiteFlow (Workflow)</a>
            <a rev="chapter_4101515562" href="./chapter_4101515562.html">Working with Workflows</a>
            <a class="nshelp_navlast" rev="section_4103074843" href="./section_4103074843.html">Working with Conditions</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4103074843" class="nshelp_title">Working with Conditions</h1>
      <p>You can use conditions on workflow initiation, workflow actions, and workflow transitions. Use conditions to limit the situations in which a workflow initiates or when actions and transitions execute. Use the Condition Builder or the Formula Builder to create conditions with field/value comparisons, expressions, or formulas. For an overview of conditions in SuiteFlow, see <a rev="section_4071954369" href="./section_4071954369.html">Workflow Conditions</a>.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">A&nbsp;single workflow initiation, action, or transition condition can contain multiple conditions, for example, a field value comparison, a formula, and a saved search. All conditions&nbsp;must evaluate to true for a workflow instance to initiate or an action or transition to execute.</p>
      </div>
      <br>
      <p>The following table describes where you can get more information about working with conditions:</p>
      <table class="alternate_rows" style="min-width:524px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:281px;" class="bolight_bgdark" valign="top">
							<p>Task</p>
						</th>
            <th style="min-width:225px;" class="bolight_bgdark" valign="top">
							<p>For more information, see ...</p>
						</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:281px;" class="bolight_bgwhite">
							<p>Using the Condition Builder to create a condition</p>
						</td>
            <td style="min-width:225px;" class="bolight_bgwhite">
							<p><a rev="section_4103075208" href="./section_4103075208.html">Defining a Condition with the Condition Builder</a></p>
						</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
							<p>Using the Condition Builder to create an expression</p>
						</td>
            <td class="bolight_bglight">
							<p><a rev="section_4103076651" href="./section_4103076651.html">Defining a Condition with Expressions</a></p>
						</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
							<p>Using the Formula Builder</p>
						</td>
            <td class="bolight_bgwhite">
							<p><a rev="section_4103077064" href="./section_4103077064.html">Defining a Condition with Formulas</a></p>
						</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
							<p>Using pre-edit record values in a condition (example)</p>
						</td>
            <td class="bolight_bglight">
							<p><a rev="section_4103799808" href="./section_4103799808.html">Referencing Old (Pre-edit) Values in a Workflow</a></p>
						</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
							<p>Using conditions with the customer credit hold field (example)</p>
						</td>
            <td class="bolight_bgwhite">
							<p><a rev="section_4103800262" href="./section_4103800262.html">Defining Conditions for Customer Credit Hold Field</a></p>
						</td>
          </tr>
        </tbody>
      </table>
      <h2 id="bridgehead_4128904521">Condition Components</h2>
      <p>Each condition you define with the Condition Builder consists of three separate components:</p>
      <ol class="nshelp">
        <li>
          <p>Value to be compared (left side of the condition). In the Condition Builder, use the left <strong>Record</strong> (optional) and <strong>Field</strong> columns to identify the field to be compared.</p>
        </li>
        <li>
          <p>Compare type. The comparison operator identifies the type of comparison to perform. The types of operators depend on the field values to be compared. Use the <strong>Compare Type</strong> column.</p>
        </li>
        <li>
          <p>Value to be compared against (right side of the condition). In the Condition Builder, use the <strong>Value</strong>, <strong>Selection</strong>, or <strong>Record</strong> and <strong>Value Field</strong> columns to identify the fields to be compared against.</p>
          <p>These three options are mutually exclusive. You cannot combine multiple values to be compared against in a single condition. If you want to use multiple value columns in a single condition, combine the conditions with expressions. See <a rev="section_4103076651" href="./section_4103076651.html">Defining a Condition with Expressions</a>.</p>
        </li>
      </ol>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteFlowWorkflow/ConditionBuilderCallouts.png" width="630px" height="193px">
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4103075208" href="./section_4103075208.html">Defining a Condition with the Condition Builder</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4103076651" href="./section_4103076651.html">Defining a Condition with Expressions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4103077064" href="./section_4103077064.html">Defining a Condition with Formulas</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

