<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N2823893" href="./book_N2823893.html">SuiteBuilder (Customization)</a>
            <a rev="chapter_N2826978" href="./chapter_N2826978.html">Custom Fields</a>
            <a rev="section_4388569541" href="./section_4388569541.html">Advanced Features for Custom Fields</a>
            <a class="nshelp_navlast" rev="section_N2832010" href="./section_N2832010.html">Creating Dynamic Defaults and Dynamic Hyperlinks</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2832010" class="nshelp_title">Creating Dynamic Defaults and Dynamic Hyperlinks</h1>
      <p>When working with free-form text, text area, rich text or hypertext fields, in the Default Value field on the Validation &amp; Defaulting subtab, you can include NetSuite tags in the default definition. NetSuite tags are populated with field values when the page is loaded or saved.</p>
      <p>Dynamic defaults can be used in the hyperlink fields to include information from the record or the current session in the URL for the website.</p>
      <p>To include NetSuite tags in the default definition of a field, enclose each tag within curly braces, defining field tags in a dynamic default as <strong>{tag}</strong>, where <strong>tag</strong> is the ID of the field. Each field in NetSuite has a unique ID and therefore a unique tag definition.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Because field IDs are incorporated into tag definitions for fields, when creating custom fields, enter meaningful IDs for each custom field and use consistent naming conventions that meet your business needs. The default NetSuite IDs are meaningless, and in your dynamic defaults it will be difficult to know exactly what the field references.</p>
      </div>
      <br>
      <p>Dynamic defaults are evaluated and each NetSuite tag is substituted on page load and page save. However, if you check the <strong>Store Value</strong> box, the tag substitution values are saved when the page is created as a true default. The default value is saved and is <strong>not</strong> dynamically changed when fields on the page change, letting you create a dynamic default that retains its initial value. The field must be edited manually, or updated with custom code to change its initial value.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">If you need to ensure that NetSuite tags defined in a dynamic default are substituted on each page load and save, clear the <strong>Store Value</strong> box.</p>
      </div>
      <br>
      <h3 id="bridgehead_N2832058">NetSuite Tags</h3>
      <p>Currently any field on the page that has a custom code ID can be used in a NetSuite tag. You can find the code ID for standard NetSuite fields in the <a href="../../help/helpcenter/en_US/srbrowser/Browser2016_2/script/record/account.html" target="_blank">SuiteScript Records Browser</a>.</p>
      <p>To determine the code ID of custom fields on your forms, go to the Custom Field list page for the field type, for example, <span id="dyn_help_taskpath" dynid="LIST_CUSTEVENTFIELD">&nbsp;</span>. The code ID is displayed in the ID column.</p>
      <p>There are also some special tags that you can use:</p>
      <ul class="nshelp">
        <li>
          <p><strong>{useremail}</strong> — Email of the user currently logged in</p>
        </li>
        <li>
          <p><strong>{today}</strong> — Current date</p>
        </li>
        <li>
          <p><strong>{nlversion}</strong> — The full internal NetSuite release number</p>
        </li>
        <li>
          <p><strong>{nlsessionid}</strong> — The browser's session ID, which could be used when creating a hyperlink for passing a session to a web service</p>
        </li>
        <li>
          <p><strong>{nluser}</strong> — ID of the user currently logged in</p>
        </li>
        <li>
          <p><strong>{nlrole}</strong> — Role ID of the user currently logged in</p>
        </li>
      </ul>
      <h3 id="bridgehead_N2832147">Dynamic Hyperlinks</h3>
      <p>For hyperlink fields, you can also create a link to a website by defining dynamic defaults. Dynamic defaults are especially useful when the exact URL is unknown until information is collected for the record. You may also want to use information specific to the current logged in session as part of a URL parameter. When creating a dynamic hyperlink, enter the http address as usual followed by <strong>?=</strong> and the desired NetSuite tags embedded in curly braces.</p>
      <p>For example, suppose that you want to include an address lookup feature on a customer form. Create a custom Entity field with the following parameters specified:</p>
      <ul class="nshelp">
        <li>
          <p>Label: Map</p>
        </li>
        <li>
          <p>ID: _map</p>
        </li>
        <li>
          <p>Type: Hyperlink</p>
        </li>
        <li>
          <p>Store Value: Not checked</p>
        </li>
        <li>
          <p>Applies To: Customer</p>
        </li>
        <li>
          <p>Display / Subtab: Main</p>
        </li>
        <li>
          <p>Display / Link Text: Click Here for Google Map</p>
        </li>
        <li>
          <p>Validation &amp; Defaulting / Default Value:</p>
          <p>http://maps.google.com/maps?q={billaddr1}%20{billcity}%20{billstate}%20{billzip}</p>
        </li>
        <li>
          <p>Validation &amp; Defaulting / Formula: Not checked (after you save the field, return to the custom field configuration page and ensure that Formula is cleared)</p>
        </li>
      </ul>
      <p>The default value includes NetSuite tags that identify the specific address of the current customer. These tags are resolved when the page is loaded so that the URL will direct the user to the customer's address as defined in the current customer record.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">When creating dynamic hyperlinks, ensure that NetSuite tags embedded in the default value definition represent required fields. If the fields are <strong>not</strong> required, and the associated form does <strong>not</strong> include a value for the tag, then the resulting URL will be invalid.</p>
      </div>
      <br>
      <h4 id="bridgehead_1487762215">Setting the Store Value Field</h4>
      <p>On page load and page save, dynamic defaults are evaluated and each NetSuite tag is substituted. For these substitutions to be made each time, the&nbsp; Store Value &nbsp;box for the custom field must be cleared.</p>
      <p>If you check the&nbsp;Store Value box, the tag substitution is performed one time, when the custom field is created. The value is saved and does not change dynamically when the fields on the page are updated.</p>
      <p>&nbsp;To ensure that NetSuite tags defined in a dynamic default are substituted every time a page is loaded and saved, clear the&nbsp;Store Value&nbsp;box.</p>
      <h4 id="bridgehead_1487762294">Setting the Formula Field</h4>
      <p>When entering a default value for a field, you can enter a formula. When you check the&nbsp; Formula box, the contents of the&nbsp; Default Value field are treated as an SQL expression, executing&nbsp;<strong>SELECT &lt;formula text&gt; FROM dual</strong>&nbsp;in the database to obtain the results. Formulas that contain tags will have the values substituted before the formula is executed.</p>
      <p>However, if a value contains tags, it is not necessarily a formula. Ensure that you are entering a formula before checking the&nbsp; Formula &nbsp;box. For example, if you enter&nbsp;<strong>Welcome, {firstname}</strong>&nbsp;as the default for a text area field, the content is substituted correctly only if the&nbsp;Formula box is cleared.</p>
      <p>If you are entering a dynamic default for a hyperlink and you clear the Formula&nbsp;box, the following default value results in a valid URL: http://maps.google.com/maps?q={billaddr1}%20{billcity}%20{billstate}%20{billzip}</p>
      <p>However, if the Formula&nbsp;box is checked, the preceding URL results in an Error: Invalid Expression message. To use a formula for the example above, enter the formula like this: 'http://maps.google.com/maps?q=’ || {billaddr1} || '%20' || {billcity} || '%20' || {billstate} || '%20' || {billzip}</p>
      <p>When you are entering information in the&nbsp;Default Value field, the Formula&nbsp;box may be checked automatically. If you see an Error: Invalid Expression error or a default is not appearing as you intended, verify the Formula&nbsp;setting.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4388569541" href="./section_4388569541.html">Advanced Features for Custom Fields</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_3908498467" href="./section_3908498467.html">Encrypting Custom Field Stored Values</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_3746191995" href="./section_3746191995.html">Creating Custom Fields with Values Derived from Summary Search Results</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2832369" href="./section_N2832369.html">Creating Formula Fields</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2833020" href="./section_N2833020.html">SQL Expressions</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

