<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="preface_3714107248" href="./preface_3714107248.html">Projects</a>
            <a rev="part_3746040281" href="./part_3746040281.html">Project Management</a>
            <a class="nshelp_navlast" rev="section_N1192913" href="./section_N1192913.html">Project Tasks</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N1192913" class="nshelp_title">Project Tasks</h1>
      <p><span id="dyn_help_feature" dynid="ADVANCEDJOBS">&nbsp;</span>   </p>
      <p>When you use the Project Management feature, you can create project task records. Task records track activities that need to be completed. Project tasks represent individual actions within a set of actions that must be completed to achieve a goal. The goal is achieved when all the tasks for the project are completed.</p>
      <p>When you are creating your project record, create a project task record for each activity that you will need to accomplish to complete the project.</p>
      <p>Project tasks cannot be created independently, they must be associated with project records. Project tasks are used to facilitate project planning and are created only on the project record.</p>
      <p>Tasks created with Project Management can also automate information gathering about each task. For example, you can track time budgeted and remaining for a task and calculate the percentage of a task that has been completed.</p>
      <h2 id="bridgehead_N1192945">Project Task Records</h2>
      <p>Project tasks list each of the actions you must complete to successfully achieve the goal. Enter a task record for each project task that must be completed.</p>
      <p>For example, to track a basic office furniture sales and delivery project, you could create a task record for each of the following:</p>
      <ul class="nshelp">
        <li>
          <p>Order Items From Supplier</p>
        </li>
        <li>
          <p>Assemble Items</p>
        </li>
        <li>
          <p>Furniture Delivery and Installation</p>
        </li>
        <li>
          <p>Bill Customer For Delivery and Items</p>
        </li>
      </ul>
      <p>After you have created a project record, you can create task records for each task required to complete the project. On the Schedule subtab of a project record, click New Project Task to create a new task for a project.</p>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/Projects/NewProjTaskButton.png" width="526px" height="170px">
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The Schedule subtab appears only after you save a project record.</p>
      </div>
      <br>
      <p>The previous example is a very basic four-task project. However, many projects are more complicated and require more details to be tracked. When many details must be entered on a task record, it is best to keep all details organized to be sure the task runs smoothly.</p>
      <p>The task record you create will contain all the information you need to know about the task, such as the kind of task, duration, dependency on other tasks, start and finish dates, and assigned resources.</p>
      <p>Assign resources and define the service type, cost, and estimated work. Then, making calculations for each resource assigned to the task, this information forms the basis for pricing project work and determining the expected gross margin. You can assign multiple resources to a task.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Only non-fulfillable or receivable service items can be selected on project tasks.</p>
      </div>
      <br>
      <p>The estimated work for the task must be specified in hours. Similarly, service items to be used on the project task must be priced in hours.</p>
      <p>For more details about creating a task record, read <a rev="section_N1194983" href="./section_N1194983.html">Creating a Project Task Record</a>.</p>
      <h3 id="bridgehead_N1194466">Organizing Tasks</h3>
      <p>Some tasks are actually a goal that has sub-tasks itself. To keep these tasks organized, group them based on which ones should be completed together and set up task hierarchies.</p>
      <h3 id="bridgehead_3739383427">Milestones</h3>
      <p>Project milestones are used to mark a point in your project, usually completion of a set of tasks or as a project health check to determine if you’re on schedule.</p>
      <p>Project milestones cannot have estimated hours, assignees, or a Finish No Later Than (FNLT) constraint. When viewing a project Gantt chart or schedule, milestone tasks are differentiated from regular project tasks.</p>
      <p>For more information, see <a rev="section_3740066806" href="./section_3740066806.html">Creating Milestone Tasks</a>.</p>
      <h3 id="procedure_N1194478">Parent Tasks and Work Tasks</h3>
      <p>When you are creating project task records, you can organize tasks in a hierarchy of parent tasks and subordinate tasks to structure the component parts of a project. Tasks can be one of the following:</p>
      <ul class="nshelp">
        <li>
          <p>Work task – A task record that tracks actual project activity, such as time worked.</p>
        </li>
        <li>
          <p>Parent task – A task record that only tracks cumulative information about subordinate tasks that are required to complete a project.</p>
        </li>
      </ul>
      <p>On task records, you can select a parent in the Parent Task field to set up a hierarchy of tasks and subordinate tasks. Then, parent tasks are summary tasks only, have no resources assigned, and only track cumulative data about subordinate tasks.</p>
      <p>For example, you can create the task Project Planning and Estimate Preparation. Then, create these tasks:</p>
      <ul class="nshelp">
        <li>
          <p>Furniture Layout and Design</p>
        </li>
        <li>
          <p>Prepare Presentation</p>
        </li>
        <li>
          <p>Generate Estimates</p>
        </li>
      </ul>
      <p>On each of the three tasks, you select Project Planning and Estimate Preparation as the Parent task. Then, these tasks are grouped together and child tasks are indented below the parent task, as shown below.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/Projects/Grouped_childParent_Tasks.png" width="630px" height="282px">
      <p>The parent task record shows data for all child records. Parent tasks cannot have resources assigned to them, they must be assigned to the child tasks.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">When creating a parent task, the record may initially be saved as a milestone record. After you have added child tasks, the task is converted to a parent task. For more information, see <a rev="section_N1192913" href="#bridgehead_3739383427">Milestones</a></p>
      </div>
      <br>
      <p>Another way to organize tasks when you enter task records is to show whether they are dependent on other tasks to be completed. This is done by setting up information about predecessors.</p>
      <h3 id="procedure_N1194583">Predecessors</h3>
      <p>Predecessor settings define the dependencies for a task. Dependencies are timing relationships among a group of tasks.</p>
      <ul class="nshelp">
        <li>
          <p>Finish-to-Start (FS) – Task starts when preceding task finishes. Start date is adjusted based on the preceding task’s finish date.</p>
        </li>
        <li>
          <p>Start-to-Start (SS) – Task starts after preceding task starts. Start date is adjusted based on the preceding task’s start date.</p>
        </li>
        <li>
          <p>Start-to-Finish (SF) – Task finishes after the preceding task starts. Start date is adjusted based on the preceding task’s start date.</p>
        </li>
        <li>
          <p>Finish-to-Finish (FF) – Task finishes after the preceding task finishes. Start date is adjusted based on the preceding task’s finish date.</p>
        </li>
      </ul>
      <p>Commonly, a completed project plan is a group of milestones, work tasks, and parent tasks, each parent being one phase of the total plan. The Schedule subtab of the project shows an organized view of the complete plan, as shown below:</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/Projects/ProjInitialTemplateSchedSubtab.png" width="630px" height="256px">
      <p>When entering predecessors, you can also add lag time between your tasks. Lag time is a delay between tasks that have a dependency. You can enter lag time to adjust your project schedule. To enter lag time, enter the number of days in the Lag Days field on the Predecessor subtab.</p>
      <h3 id="bridgehead_3898990487">Copying Tasks</h3>
      <p>You can copy project tasks between projects and within projects. When copying a project task, you can choose to also copy the task assignments, budgets, and any child tasks. On the Schedule subtab of project records, click Copy next to the project task you want to copy. For more information, see <a rev="section_3897401475" href="./section_3897401475.html">Copying Project Tasks</a>.</p>
      <h3 id="procedure_N1194636">Task Views</h3>
      <p>You can choose from several ways to view a complete project in the View field. You can choose a Planning view, Tracking view or Variance view. For more information about these view options, read <a rev="section_N1202036" href="./section_N1202036.html">Project Work Breakdown Structures (WBS)</a>.</p>
      <p>To view a list of projects tasks by assignee, go to Activities &gt; Scheduling &gt; Project Tasks.</p>
      <h3 id="bridgehead_N1194659">Work Breakdown Structures</h3>
      <p>Creating, viewing, and organizing tasks for a project are all part of utilizing the <a rev="section_N1202036" href="./section_N1202036.html">Project Work Breakdown Structures (WBS)</a>.</p>
      <p>Work Breakdown Structure (WBS) is a term to describe the means to schedule and manage project tasks. It organizes the tasks as parts of the project as a whole and defines how they should work together. The WBS shows on the Schedule subtab of the project record and helps you examine the overall scope, progress and cost for a project.</p>
      <h3 id="bridgehead_N1194685">Task Baselines</h3>
      <p>After a project has input of necessary tasks, task durations, task dependencies, resource assignments, and cost estimates, take a baseline snapshot to compare intended activity to actual activity. Then, when you record the actual timing of tasks, actual resource time investment and actual costs, you can make a comparison.</p>
      <h3 id="bridgehead_N1194697">Resource Time Entry on Project Tasks</h3>
      <p>The project record tracks the estimated time entered for each resource on a task. Then, when the resource enters time against the project, the actual time worked is tracked.</p>
      <p>The Time subtab on the task record displays the planned time entries.</p>
      <h3 id="bridgehead_N1194713">CRM Tasks</h3>
      <p>Other task records you can choose to create are CRM task records. CRM tasks are "to do" activities that need to be completed. For example, a CRM task might be an upcoming meeting or phone call with a potential new client.</p>
      <p>Like project tasks, the record for each CRM task tracks what must be done and who needs to do it. Unlike project task records, CRM task records can be independent and do not need to be associated with a project.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">It is recommended that you use project task records, not CRM task records to track information for projects. For more information, read:</p>
      </div>
      <br>
      <ul class="nshelp">
        <li>
          <p>
            <a rev="section_N506499" href="./section_N506499.html">Working with CRM Tasks</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N1192913" href="#bridgehead_N1192945">Project Task Records</a>
          </p>
        </li>
      </ul>
      <h3 id="bridgehead_N1194777">CRM Tasks and Project Management</h3>
      <p>CRM tasks can be associated with a project, but are not considered part of the project's cost and time data unless they are explicitly included using the Include CRM Tasks in Project Totals check box. CRM tasks associated with a project do not display in the project schedule. For more information, read <a rev="section_N1201405" href="./section_N1201405.html">Including CRM Tasks in Project Totals</a>.</p>
      <h3 id="bridgehead_N1194797">Viewing Project Tasks on Your Dashboard</h3>
      <p>Add the Project Tasks portlet to your dashboard to display project tasks assigned to you or other resources. You can customize the portlet to display project tasks in a way that is meaningful to you. For example, customize the portlet to display a list of your project tasks filtered by project. Or if you are a senior project manager, display a list of late tasks across all projects. This portlet is available only if you use the Project Management feature. From the portlet you can quickly access a task to view or edit task details.</p>
      <p>If you use the Inline Editing feature, you can update the status and priority of a project task in the portlet. Using Inline Editing in the portlet to make changes works the same as using Inline Editing in a list view. Since the number of project task fields displayed in the portlet is limited, click Customize View, or Edit View on a custom task view, and reorder the fields, if necessary, to expose the status and priority fields.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The Priority field is not exposed on the standard project task form. To display project task priorities as a column in the portlet, you must view a custom project task form in the portlet that includes the Priority field.</p>
      </div>
      <br>
      <p>The portlet is available for full-access and Employee Center users who have View access to project tasks. If you want to view and update your CRM tasks or create new ones, then add the Tasks portlet to your dashboard.</p>
      <p>Use the portlet filter options to select the range of project tasks to display in the portlet. For more information, read <a rev="section_N509646" href="./section_N509646.html">Tasks and Project Tasks Portlets on Your Dashboard</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1194983" href="./section_N1194983.html">Creating a Project Task Record</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1195767" href="./section_N1195767.html">Project Task Attributes Table</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1199176" href="./section_N1199176.html">Identifying Parent Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1199494" href="./section_N1199494.html">Scheduling Project Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1200642" href="./section_N1200642.html">Assigning Resources to Project Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1200868" href="./section_N1200868.html">Importing Project Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1201405" href="./section_N1201405.html">Including CRM Tasks in Project Totals</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

