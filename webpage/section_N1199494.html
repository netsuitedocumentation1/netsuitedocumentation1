<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="preface_3714107248" href="./preface_3714107248.html">Projects</a>
            <a rev="part_3746040281" href="./part_3746040281.html">Project Management</a>
            <a rev="section_N1192913" href="./section_N1192913.html">Project Tasks</a>
            <a class="nshelp_navlast" rev="section_N1199494" href="./section_N1199494.html">Scheduling Project Tasks</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N1199494" class="nshelp_title">Scheduling Project Tasks</h1>
      <p><span id="dyn_help_feature" dynid="ADVANCEDJOBS">&nbsp;</span>   </p>
      <p>NetSuite prepares a system-calculated schedule for each project based on project start date, task durations, predecessors, constraints, and resource work calendars. The schedule drives planning, billing, and management for the entire project.</p>
      <p>The project Start Date sets the date from which the project schedule is calculated.</p>
      <p>To view the schedule for a project, go to Lists &gt; Relationships &gt; Projects and click View next to the project. On the project record, the Schedule subtab is the top subtab.</p>
      <h3 id="bridgehead_N1199520">Resource Assignment and Project Scheduling</h3>
      <p>Project scheduling is also based on resource and work data entered on task records. The schedule is based on the duration of the tasks. Task duration is calculated as [estimated work x units] for all task resource rows and helps determine the start and end dates for each task.</p>
      <p>For example, a task requires 16 hours of work. Two resources are assigned to the task, each set to work at 100% capacity and for 8 estimated hours. Each resource is assigned to the default work calendar of eight hours per day, Monday through Friday. The task is scheduled across two calendar days, so the project work schedule is 2 days in duration. The schedule also takes into account any time off requested by the assigned resources.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The task duration calculation is also based on the task relationships. See <a rev="section_N1199494" href="#bridgehead_N1199631">Adaptive Scheduling</a> below.</p>
      </div>
      <br>
      <p>The end date of a project task is calculated by assessing the number of hours assigned to each task resource. For each assignment, the resource's work calendar is used to add the specified number of hours to the start date-time to arrive at an end date. If the start date of your project changes, NetSuite automatically updates the end date based on the scheduled tasks. If time off is submitted after the creation of the schedule, the project must be recalculated for adjustments to occur.</p>
      <p>The latest end date of all assigned resources is the project task's end date.</p>
      <p>The total number of hours for the project task is calculated by summing the hours assigned to each resource.</p>
      <h3 id="bridgehead_N1199562">Work calendars</h3>
      <p>Work calendars define the work capacity for resources. Then, that capacity determines when tasks can be scheduled. For details, read <a rev="section_N1189731" href="./section_N1189731.html">Project Resource Work Calendars</a>.</p>
      <h3 id="bridgehead_N1199583">Creating Planned Time Entries</h3>
      <p>When you create a project task, you enter the resource work capacity as percentage of available scheduling time in the Units column. NetSuite uses the work capacity and work calendars for resources assigned to project tasks to create the project schedule and generate planned time entries. If you create planned time entries for a project, NetSuite limits the number of time entries that can be created.</p>
      <p>In order to prevent projects with excessive number of tasks and time entries, the following rules apply:</p>
      <h3 id="bridgehead_N1199600">Project tasks</h3>
      <p>When assigning a resource to a project task, the resource capacity or units must be 5% or greater. The estimated work for the resource must be 2080 hours or less.</p>
      <p class="indent_1">Depending on the work calendar parameters, possible planned time entries vary. The minimum planned time entry possible depends on the hours per day in the work calendar. For resources assigned to a work calendar with eight hour work day, the minimum planned time entry cannot be less than 24 minutes. The smallest planned time entry possible is 3 minutes (5% of 1 hour, the minimum hours per day allowed in work calendar).</p>
      <p>The maximum number of planned time entries per task that can be created for a resource is 260. This is the maximum number of work days a resource can be assigned to work on a task.</p>
      <h3 id="procedure_N1199620">Projects</h3>
      <p>The total number of planned time entries for all project resources must be 5200 or less. The total amount of work days scheduled for a project cannot exceed 20 person years of work.</p>
      <h3 id="bridgehead_N1199631">Adaptive Scheduling</h3>
      <p>Project tasks are capable of adaptive scheduling. This means, when the current project schedule is viewed, the project schedule accurately reflects necessary changes to the project.</p>
      <p>For example, when a project plan is initially created based on task dependencies and resource work calendars, the schedule represents an idealized estimate. This initial estimate does not reflect any actual project work if no actual time has been entered against the project from resources working on the project.</p>
      <p>After work on the project begins and progresses, resources enter time worked on the project. After resources enter time against a project, some aspects of project tasks may begin to shift. Aspects that may change include the following:</p>
      <ul class="nshelp">
        <li>
          <p>project costs</p>
        </li>
        <li>
          <p>start and end dates</p>
        </li>
        <li>
          <p>work and actual work</p>
        </li>
      </ul>
      <p>For example, the work for TaskOneA is 40 hours: 8 hours per day, Monday through Friday. If a resource assigned to the task enters 4 hours for Monday, then the schedule automatically recalculates so that the project plan shows 4 hours of actual time worked on Monday, 8 hours of planned work scheduled Tuesday through Friday, and 4 hours of planned work scheduled the following Monday for a total of 40 hours.</p>
      <p>Tasks with predecessor relationships are set to start based on the start and finish of other tasks. Then, if the duration of one task must change, all tasks related to it may be recalculated to show an updated duration and new start and end dates. For more information, read <a rev="section_N1199494" href="#procedure_N1200446">Predecessor-successor relationships</a> below.</p>
      <p>The Time subtab on task records shows Planned Time and contributes to a real-time picture of project schedule.</p>
      <ul class="nshelp">
        <li>
          <p>If resources complete the project task early, it pulls in projected end date of the project.</p>
        </li>
        <li>
          <p>If resources cannot complete tasks as quickly as anticipated, the end date is pushed out accordingly.</p>
        </li>
      </ul>
      <h3 id="bridgehead_N1200398">Task setup and scheduling</h3>
      <p>The characteristics of a project task are largely derived from its relationship to other tasks for the project. Task relationships can be one of the following:</p>
      <ul class="nshelp">
        <li>
          <p>parent-child relationships</p>
        </li>
        <li>
          <p>predecessor-successor relationships</p>
        </li>
      </ul>
      <p>Tasks for a project can be arranged in a hierarchy by assigning parent-child relationships. For example, you may create Task One. Then, you create tasks TaskOneA, TaskOneB, and TaskOneC, and assign Task One as the parent task for all three tasks. <a rev="section_N1199176" href="./section_N1199176.html">Identifying Parent Tasks</a> are tasks that have child tasks assigned to it.</p>
      <p>The parent task, Task One, is also known as a summary task. The data values shown on a summary task are the rolled up values of its children. For example, a summary task's start date is the earliest start date of its child tasks, and its end date is the latest end date of its child tasks. The summary task's work is the sum of the work of its leaves.</p>
      <h3 id="procedure_N1200446">Predecessor-successor relationships</h3>
      <p>In addition to a hierarchical structure, project tasks relationships can also be defined in terms of dependency. For each task, you can define how that task relates to other project tasks based on when the task should start. Each task is a predecessor or a successor, even if the task runs concurrent to other tasks.</p>
      <p>For example, the completion of TaskOneB requires components that are assembled during TaskOneA. Therefore, TaskOneA is a predecessor of TaskOneB. TaskOneB cannot begin until TaskOneA is completed.</p>
      <p>Dependency types that can be assigned on tasks are</p>
      <ul class="nshelp">
        <li>
          <p>Finish-to-Start (FS) – Task starts when preceding task finishes. Start date is adjusted based on the preceding task’s finish date.</p>
        </li>
        <li>
          <p>Start-to-Start (SS) – Task starts after preceding task starts. Start date is adjusted based on the preceding task’s start date.</p>
        </li>
        <li>
          <p>Start-to-Finish (SF) – Task finishes after the preceding task starts. Start date is adjusted based on the preceding task’s start date.</p>
        </li>
        <li>
          <p>Finish-to-Finish (FF) – Task finishes after the preceding task finishes. Start date is adjusted based on the preceding task’s finish date.</p>
        </li>
      </ul>
      <p>Some data on a project task are calculated using input from its dependency relationships. The start date of a project task is the latest end date of all predecessors in the start-to-finish dependency case and is the last start date of all its predecessors in the start-to-start case.</p>
      <p>Project tasks that have no predecessors start on the start date of the project.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1192913" href="./section_N1192913.html">Project Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1192913" href="./section_N1192913.html#bridgehead_N1192945">Project Task Records</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1194983" href="./section_N1194983.html">Creating a Project Task Record</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1195767" href="./section_N1195767.html">Project Task Attributes Table</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1199176" href="./section_N1199176.html">Identifying Parent Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1200642" href="./section_N1200642.html">Assigning Resources to Project Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1200868" href="./section_N1200868.html">Importing Project Tasks</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N1201405" href="./section_N1201405.html">Including CRM Tasks in Project Totals</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

