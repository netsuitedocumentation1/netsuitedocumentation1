loadjscssfile("./assets/pagestyles.nl", "css");
loadjscssfile("./assets/jquery-ui.min.css?NS_VER=2016.2.0&amp;minver=162", "css");
loadjscssfile("./assets/720962082.css?NS_VER=2016.2.0&amp;minver=162", "css");
loadjscssfile("./assets/2007714384.css?NS_VER=2016.2.0&amp;minver=162", "css");
loadjscssfile("./assets/4627907450.css?NS_VER=2016.2.0&amp;minver=162", "css");
loadjscssfile("./assets/814566804.css?NS_VER=2016.2.0&amp;minver=162", "css");
loadjscssfile("./assets/103937573.css?NS_VER=2016.2.0&amp;minver=162", "css");
loadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", "css");

loadScript("./assets/jquery-1.11.3.min.js?NS_VER=2016.2.0&amp;minver=162", function(){
    loadjQueryDependentLibs();
});

window.onload = function(){
	redirectImages();
	removeBadElements();
	loadStructuredData();
};

function removeBadElements(){
	//Removes Print button
	$('.nshelp_navbuttons').remove();
	
	//Fixes Records Browser Links
	if($('a[href*="srbrowser"]').attr('href',$('a[href*="srbrowser"]').attr('href')) && $('a[href*="srbrowser"]').attr('href',$('a[href*="srbrowser"]').attr('href')).length){
		$('a[href*="srbrowser"]').attr('href',$('a[href*="srbrowser"]').attr('href').replace('../../', 'http://www.netsuite.com/'));
	}
}

function redirectImages(){
	var imgs = document.querySelectorAll( 'img' );
	for(var i in imgs){
		var imgurl = imgs[i].src;
		if(!imgurl || !imgurl.includes('helpcenter')){continue;}
		if(imgurl.includes('icon_home.gif')){
			imgs[i].src = './assets/' + imgurl.split('/').pop();
		}
	}
}

function loadScript(url, callback){
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState){
        // handle IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" || script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {
        // handle other browsers
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function loadjQueryDependentLibs(){
	
	loadjscssfile("./assets/jquery_isolation.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/jquery-ui.min.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/NLUtil.jsp?NS_VER=2016.2.0&amp;minver=162&amp;locale=en_US", "js");
	loadjscssfile("./assets/NLUtil.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/NLUIWidgets.jsp?NS_VER=2016.2.0&amp;minver=162&amp;locale=en_US", "js");
	loadjscssfile("./assets/807108029.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/129055756.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/NLPopup.jsp?NS_VER=2016.2.0&amp;minver=162&amp;locale=en_US", "js");
	loadjscssfile("./assets/NLTree.jsp?NS_VER=2016.2.0&amp;minver=162&amp;locale=en_US", "js");
	loadjscssfile("./assets/3148774931.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/NLAppUtil.jsp?NS_VER=2016.2.0&amp;minver=162&amp;locale=en_US", "js");
	loadjscssfile("./assets/166585402758.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/32951477668.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/11494920032.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/NLHelpCenter.jsp?NS_VER=2016.2.0&amp;minver=162&amp;locale=en_US&amp;helplang=en_US", "js");
	loadjscssfile("./assets/14698099003.js?NS_VER=2016.2.0&amp;minver=162", "js");
	loadjscssfile("./assets/395372696.js?NS_VER=2016.2.0&amp;minver=162", "js");
	
}

function loadjscssfile(filename, filetype) {
	filetype = filetype || filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
	if (filetype == "js") { //if filename is a external JavaScript file
		var fileref = document.createElement('script')
		fileref.setAttribute("type", "text/javascript")
		fileref.setAttribute("src", filename)
	} else if (filetype == "css") { //if filename is an external CSS file
		var fileref = document.createElement("link")
		fileref.setAttribute("rel", "stylesheet")
		fileref.setAttribute("type", "text/css")
		fileref.setAttribute("href", filename)
	}
	if (typeof fileref != "undefined"){
		document.getElementsByTagName("head")[0].appendChild(fileref)
	}
	console.log(filename + ' plugin loaded');
}

function loadStructuredData() {
	var title = document.getElementsByClassName('nshelp_title')[0].innerHTML;
	var elementString = '<script type="application/ld+json">{ "@context": "http://schema.org", "@type": "NewsArticle", "headline": "%ARTICLE_TITLE%"}</script>'.replace('%ARTICLE_TITLE%', title);
	document.body.insertAdjacentHTML( 'beforeend', elementString);
}