




















        

var CATEGORY_PAGE_DIV_ID = "category_page";
var SECTION_TITLE_PAGE_DIV_ID = "section_title_page";
var DEFAULT_TOPIC_ALIAS = "DOC_FRONTPAGE";
var TRAINING_VIDEOS = "DOC_TRAININGVIDEOS";
var HELP_SERVER_HOST_NAME = NLGetCurrentScriptFileHostName();
var IMG_TOC_SHOW = "/images/help/toc-show.png";
var IMG_TOC_HIDE = "/images/help/toc-hide.png";

var sTEXT_AREA_HELPER_TEXT = "Please provide suggestions to improve this help topic.";
var sFeedbackHtml = "<table  id='outerfeedback' cellpadding=0 cellspacing=0 border=0 style='font-family: Open Sans,Helvetica,sans-serif; font-size: 14px;' width=620>" +
"   <tbody>"+
"        <tr>"+
"            <td id='innerfeedback'>"+
"                <table class ='nshelp_feedback' cellpadding='2' cellspacing='0' border='0' width='100%'>"+
"                    <tbody>"+
"                        <tr>"+
"                            <td class='nshelp_feedback_title' colspan='5' align='left'>"+
"                                <b>How helpful was this topic?</b>"+
"                            </td>"+
"                        </tr>"+
"                        <tr class='nshelp_feedback_stars' >"+
"                            <td class='nshelp_feedback_star' align='center' nowrap=''>"+
"                                <input type='radio' name='scores' id='helpscore4' value='4' style='border:0px;'>"+
"                                <label for='helpscore4'>Five Stars</label>"+
"                            </td>"+
"                            <td class='nshelp_feedback_star' align='center' nowrap=''>"+
"                                <input type='radio' name='scores' id='helpscore3' value='3' style='border:0px;'>"+
"                                <label for='helpscore3'>Four Stars</label>"+
"                            </td>"+
"                            <td class='nshelp_feedback_star' align='center' nowrap=''>"+
"                                <input type='radio' name='scores' id='helpscore2' value='2' style='border:0px;'>"+
"                                <label for='helpscore2'>Three Stars</label>"+
"                            </td>"+
"                            <td class='nshelp_feedback_star' align='center' nowrap=''>"+
"                                <input type='radio' name='scores' id='helpscore1' value='1' style='border:0px;'>"+
"                                <label for='helpscore1'>Two Stars</label>"+
"                            </td>"+
"                            <td class='nshelp_feedback_star' align='center' nowrap=''>"+
"                                <input type='radio' name='scores' id='helpscore0' value='0' style='border:0px;'>"+
"                                <label for='helpscore0'>One Star</label>"+
"                            </td>"+
"                        </tr>"+
"                        <tr>"+
"                            <td class='nshelp_feedback_comments' colspan='5'>"+
"                                <br>"+
"                                <textarea id='nshelp_feedback_comments' name='nshelp_feedback_comments' cols='60' rows='3'  placeholder='" + sTEXT_AREA_HELPER_TEXT+ "'></textarea>"+
"                            </td>"+
"                        </tr>"+
"                        <tr>"+
"                            <td class='button' colspan='5' align='left'>"+
"                                <table id='tbl_submit_feedback' cellpadding='0' cellspacing='0' border='0' class='uir-button' style='margin-right:6px; cursor:hand;'>"+
"                                    <tbody>"+
"                                        <tr id='tr_submit_feedback' class='pgBntG'>"+
"                                            <td id='tdleftcap_feedback'>"+
"                                                <img src='/images/nav/ns_x.gif' class='bntLT' border='0' height='50%' width='3'>"+
"                                                <img src='/images/nav/ns_x.gif' class='bntLB' border='0' height='50%' width='3'>"+
"                                            </td>"+
"                                            <td id='nshelp_tdbody_feedback' height='20' valign='top' nowrap='' class='bntBgB'>"+
"                                                <input type='button' style='' class='rndbuttoninpt bntBgT' value='Submit Feedback' id='go' name='submit_feedback'"+
"                                                    onclick='top.submitFeedback(document);'"+
"                                                    onmousedown='this.setAttribute('_mousedown','T'); setButtonDown(true, false, this);'"+
"                                                      onmouseup='this.setAttribute('_mousedown','F'); setButtonDown(false, false, this);'"+
"                                                      onmouseout='if(this.getAttribute('_mousedown')=='T') setButtonDown(false, false, this);'"+
"                                                    onmouseover='if(this.getAttribute('_mousedown')=='T') setButtonDown(true, false, this);'"+
"                                                    _mousedown='F'>"+
"                                            </td>"+
"                                            <td id='tdrightcap_go'>"+
"                                                <img src='/images/nav/ns_x.gif' height='50%' class='bntRT' border='0' width='3'>"+
"                                                <img src='/images/nav/ns_x.gif' height='50%' class='bntRB' border='0' width='3'>"+
"                                            </td>"+
"                                        </tr>"+
"                                    </tbody>"+
"                                </table>"+
"                            </td>"+
"                        </tr>"+
"                    </tbody>"+
"                </table>"+
"            </td>"+
"        </tr>"+
"   </tbody>"+
"</table>";

var sSearchFeedbackHtml = "<table id='outerfeedback' cellpadding='0' cellspacing='0' border='0' style='font-size: 14px;' width='620'>"+
"<tbody>"+
"   <tr>"+
"       <td id='innerfeedback'>"+
"            <table class ='nshelp_feedback' cellpadding='2' cellspacing='0' border='0' width='100%'>"+
"                <tbody>"+
"                    <tr>"+
"                        <td class='nshelp_feedback_title' colspan='3' align='left' nowrap=''>"+
"                            <b>Did these search results help answer your question?</b>"+
"                        </td>"+
"                    </tr>"+
"                    <tr class='nshelp_feedback_stars'>"+
"                        <td class='nshelp_feedback_star' align='right' nowrap=''>"+
"                            <input type='radio' name='scores' id='helpscore1' value='1'>"+
"                            <label for='helpscore0'>Yes</label>"+
"                        </td>"+
"                        <td class='nshelp_feedback_star' align='left' nowrap=''>"+
"                            <input type='radio' name='scores' id='helpscore0' value='0'>"+
"                            <label for='helpscore2'>No</label>"+
"                        </td>"+
"                    </tr>"+
"                    <tr>"+
"                        <td class='button' colspan='3' align='left'>"+
"                            <table id='tbl_submit_feedback' cellpadding='0' cellspacing='0' border='0' class='uir-button' style='margin-right:6px; cursor:hand;'>"+
"                                <tbody>"+
"                                    <tr id='tr_submit_feedback' class='pgBntG'>"+
"                                        <td id='tdleftcap_feedback'>"+
"                                            <img src='/images/nav/ns_x.gif' class='bntLT' border='0' height='50%' width='3'>"+
"                                            <img src='/images/nav/ns_x.gif' class='bntLB' border='0' height='50%' width='3'>"+
"                                        </td>"+
"                                        <td id='nshelp_tdbody_feedback' height='20' valign='top' nowrap='' class='bntBgB'>"+
"                                            <input type='button' style='' class='rndbuttoninpt bntBgT' value='Submit Feedback' id='submit_feedback' name='submit_feedback'"+
"                                                                                  onclick='top.submitFeedback(document);'"+
"                                                                                  onmousedown='this.setAttribute('_mousedown','T'); setButtonDown(true, false, this);'"+
"                                                                                  onmouseup='this.setAttribute('_mousedown','F'); setButtonDown(false, false, this);'"+
"                                                                                  onmouseout='if(this.getAttribute('_mousedown')=='T') setButtonDown(false, false, this);'"+
"                                                                                  onmouseover='if(this.getAttribute('_mousedown')=='T') setButtonDown(true, false, this);'"+
"                                                                                  _mousedown='F'>"+
"                                        </td>"+
"                                        <td id='tdrightcap_go'>"+
"                                            <img src='/images/nav/ns_x.gif' height='50%' class='bntRT' border='0' width='3'>"+
"                                            <img src='/images/nav/ns_x.gif' height='50%' class='bntRB' border='0' width='3'>"+
"                                        </td>"+
"                                    </tr>"+
"                                </tbody>"+
"                            </table>"+
"                        </td>"+
"                   </tr>"+
"                 </tbody>"+
"             </table>"+
"           </td>"+
"       </tr>"+
"   </tbody>"+
"</table>";


var isIE = document.all ? true : false;
var pageBody = null;
var headerbarFrame = null;
var helpContentArea = null;
var toc = null;
var dragbar = null;
var dragTD = null;
var tocPaneWidth = 250;
var startingTocPaneWidth = 250;
var bTocHidden = false;
var bDragging = false;
var setDragPrefTimer = 0;
var setShowLeftPaneTimer = 0;
var tocToggleImage = null;

var BASE_COL_WIDTH = ", 15, *";
var HELPCENTER_BODY_AREA_ID = "helpcenter_body";
var HEADER_ID = "div__header";
var TOC_ID = "helpcenter_toc";
var DRAGBAR_ID = "helpcenter_dragpane";
var CONTENT_AREA_ID = "helpcenter_content";
var DRAG_TD_ID = "dragtd";
var SEARCH_FIELD_ID = "searchfld";
var SEARCH_ICON_WIDTH = 32;
var MINIMUM_LEFT_PANE_WIDTH = 40;
var isMobileClient = false;
function writeTabName(taskId, htmlId)
{
    addQueryEntry("tab", taskId, htmlId);
}

function writeTaskCategory(taskId, htmlId)
{
    addQueryEntry("cat", taskId, htmlId);
}

function writeTaskCategoryHead(taskId, htmlId)
{
    addQueryEntry("head", taskId, htmlId);
}

function writeTaskPath(taskId, htmlName)
{
    addQueryEntry("tp", taskId, htmlName);
}

function writeChecker(checkerName, htmlId)
{
    addQueryEntry("check", checkerName, htmlId);
}
function writeScript(scriptName, htmlId)
{
    addQueryEntry("script", scriptName, htmlId);
}

function writeFeatureNotAvaiable(featureName, htmlId)
{
    addQueryEntry("f", featureName, htmlId);
}

function writeMessage(messageName, htmlId)
{
    addQueryEntry("m", messageName, htmlId);
}

function writeNewBurst(dDate, htmlId)
{
    addQueryEntry("n", dDate, htmlId);
}

function addQueryEntry(queryType, query, htmlId)
{
    if (typeof htmlId == "undefined" || htmlId == null)
    {
        if (typeof window.kbspanId == "undefined")
            window.kbspanId = 0;
        htmlId = "kb" + window.kbspanId++;
    }
    //document.write("<span id='" + htmlId + "'></span>");
    if (typeof window.queryEntries == "undefined" || window.queryEntries == null)
        window.queryEntries = new Array();

    var queryEntry = new Array();
    queryEntry[0] =  queryType;
    queryEntry[1] =  htmlId;
    queryEntry[2] = query;

    window.queryEntries[window.queryEntries.length] = queryEntry;
}


function nlGetDynamicContent(theDocument, basehelpproject, callback)
{
    if (typeof window.queryEntries == "undefined" || window.queryEntries == null)
        return false;

    var queryString="";
    var queryEntryMap = new Object();
    // format query string
    for (var i = 0; i < window.queryEntries.length; i++)
    {
        var queryEntry = window.queryEntries[i];
        var key = queryEntry.join(',');
        if (queryEntryMap[key])
            continue;
        queryEntryMap[key] = true;
        if (queryString.length > 0)
            queryString += ";";
        queryString += key;
    }
    var projectName = basehelpproject ? basehelpproject : top.window.basehelpproject;
    queryString = 'method=dynamiccontent&_project=' + projectName + '&t=0&dcquery=' + queryString;

    var url = HELP_SERVER_HOST_NAME + "/app/help/genhelp.nl?ct=js&" + queryString;
    top.dynContentDocument = theDocument;

    loadDynamicContentScripts(url, callback);

    return true;
}

function loadDynamicContentScripts(sUrl, callback)
{
    NS.jQuery.getScript(sUrl, callback);
}

function doHandleDCSJSResponse( response, theDocument )
{
    doHandleDCResponse(response, theDocument);
}

function doHandleDCXMLResponse( response, theDocument )
{
    var sText = response.getBody();
    doHandleDCResponse(sText, theDocument);
}

function doHandleDCResponse( response, theDocument )
{
    if(!theDocument)
        theDocument = top.dynContentDocument;

    var doc = top.nsStringToXML(response);

    var items = doc.getElementsByTagName('item');
    if (processReturnStatus(doc) != STATUS_HELP_OK)
        return;

	for(var i=0;i<items.length;i++)
    {
        var id = items[i].getElementsByTagName('id')[0].firstChild.nodeValue;
        var type = items[i].getElementsByTagName('type')[0].firstChild.nodeValue;
		var htmlElements = theDocument.getElementsByTagName('span');
		if (htmlElements != null)
        {
            for(var j=0;j<htmlElements.length;j++)
            {
                var htmlElement = htmlElements[j];
                if (htmlElements[j].name != id)
                    continue;
                var msg = htmlDecode(items[i].getElementsByTagName('value')[0].firstChild.nodeValue);
                if(type == "f" )
                {
                    if (msg != null)
                    {
                        var msgArray = msg.split("~");
                        if (msgArray.length == 2)
                            htmlElement.innerHTML = "<BR>"+uir_getAlertBoxHtml(null, msgArray[0] + " " + msgArray[1], NLAlertDialog.TYPE_MEDIUM_PRIORITY, null, null, null, HELP_SERVER_HOST_NAME);
                    }
                }
                else if(type=="m")
                {
                    var msgArray = msg.split("~");
                    if (msgArray.length == 2)
                        htmlElement.innerHTML = "<BR>"+uir_getAlertBoxHtml(null, msgArray[0], msgArray[1], null, null, null, HELP_SERVER_HOST_NAME);
                }
                else if(type == "script" )
                {
                    if (msg != null)
                    {
                        var msgArray = msg.split("~");
                        if (msgArray.length == 3)
                            htmlElement.innerHTML = "<BR>"+uir_getAlertBoxHtml(msgArray[0], msgArray[1], msgArray[2], null, null, null, HELP_SERVER_HOST_NAME);
                    }
                }
                else if(type=="n")
                {
                    htmlElement.innerHTML = "<img src='/images/help/newburst.gif' border=0>&nbsp;";
                }
                else
                    htmlElement.innerHTML = htmlDecode(msg);
            }
        }
    }
	// once we have executed dynamic content for this page, clear it out so that it doesn't build up as the user visits other pages
	window.queryEntries = null;
}

function htmlDecode(text)
{
    text = text.replace(/&gt;/g,">");
    text = text.replace(/&lt;/g,"<");
    return text;
}

function requestHelpURL(url, postdata, async)
{
	try
    {
        var request = new top.NLXMLHttpRequest();
		var nsResponse = request.requestURL( url, postdata, null, async )
		return nsResponse;
    }
    catch( e )
    {
	   //throw new nlapiScriptError('REQUEST_URL_ERROR',e);
    }
}

function getUserRole()
{
    var async = false;
    var response = requestHelpURL( "/app/help/genhelp.nl?method=userrole","", async);
    var role = "";
    if ( response != null )
        role = doHandleUserRoleResponse( response );

    return role;
}

// new function for get TopicAlias
function getUserGuidesTopicAlias()
{
    return "userGuides";
}
function getSuiteAppsTopicAlias()
{
    return "DOC_NetSuiteSuiteApps";
}
function getReleaseNotesTopicAlias()
{
    return "NEW_RELEASE";
}
function getTrainingVideos()
{
    return TRAINING_VIDEOS;
}

function getFrontPageTopicAlias(){
    return DEFAULT_TOPIC_ALIAS;
}

function doHandleUserRoleResponse( response )
{
    var sText = response.getBody();
    var doc = top.nsStringToXML(sText);

    if (processReturnStatus(doc) != STATUS_HELP_OK)
        return null;

    var role = doc.getElementsByTagName('role')[0].firstChild.nodeValue;
    if (role == null || role == "undefined")
        role = "";

    return role;
}

function nlPopupQuickLooks()
{
    var role = getUserRole();
    if (role != null)
        nlPopupGuides (role);
}

function trim(str)
{
    return str.replace(/^\s+/,"").replace(/\s+$/,"");
}

var STATUS_HELP_OK = 0;
var STATUS_HELP_SESSION_TIMEOUT = -1;
function processReturnStatus(doc)
{
    var statusCode = STATUS_HELP_OK;
    if (doc != null && typeof doc != "undefined")
    {
        statusCode = doc.getElementsByTagName('statusCode')[0].firstChild.nodeValue;
        if (statusCode == STATUS_HELP_SESSION_TIMEOUT)
        {
            alert(ERR_HELP_SESSION_TIMEOUT_MSG);

            var loginUrlWithDeepLink = getNSDomain() + "/app/crm/support/ptaredirector.nl?dl=";
            loginUrlWithDeepLink = loginUrlWithDeepLink + window.location;
            displayPage(loginUrlWithDeepLink);
        }
    }

    return statusCode;
}

function getNSDomain()
{
    if (ns_domain)
    {
        return ns_domain;
    }
    else
    {
        // just a safety network if the ns_domain is not set
        return "https://system.netsuite.com";
    }
}

// Help 3.0

function getHelpDocumentHeight()
{
	if (window.innerHeight)
		return window.innerHeight;
	else
		return document.body.clientHeight;
}
function getHelpDocumentWidth()
{
	if (window.innerWidth)
		return window.innerWidth ;
	else
		return document.body.clientWidth;
}

function initDragPane()
{
	// initialize the basic variables for easy reference later
	headerbarFrame = document.getElementById(HEADER_ID);
	toc = document.getElementById(TOC_ID);
	dragbar = document.getElementById(DRAGBAR_ID);
	helpContentArea = document.getElementById(CONTENT_AREA_ID);

	// set up the drag bar event handers
	dragTD = document.getElementById(DRAG_TD_ID);
	dragTD.onselectstart = new Function("return false;");
	dragTD.onmousedown = new Function("window.startDrag(); return false;");
	helpContentArea.style.visibility = "visible";

    pageBody = document.getElementById(HELPCENTER_BODY_AREA_ID);
}

function isSearchResultsPage()
{
    if (window.pagetype)
        return (window.pagetype === 3);

    return false;
}

function isPageNotFoundPage()
{
    if (window.pagetype)
        return (window.pagetype === 2);

    return false;
}

function isCaseSubmissionPage()
{
    if (window.pagetype)
        return (window.pagetype === 4);

    return false;
}

function isPleaseSearchPage()
{
    if(window.pagetype)
        return (window.pagetype === 5);

    return false;
}


function getSearchTermFromUrl()
{
    var fieldBox = document.getElementById('searchfld');

    if (!fieldBox)
        return "";

    return fieldBox.value;
}

// called instead of contentPaneOnLoad if this script is accessed from SuiteAnswers
function contentPaneOnLoadSA(projectName)
{
    STATUS_HELP_OK = 0;
    NS.jQuery = $;
    HELP_SERVER_HOST_NAME = ns_domain_url;
    processDynamicContent(projectName);
}

function contentPaneOnLoad(projectName)
{
    var dynamicContentPresent = processDynamicContent(projectName, processAnchor);
    if (!dynamicContentPresent) {
        processAnchor();
    }

    // hovering mouse cursor over the 'main' header
    NS.jQuery('.ns-menuitem.nshelp_submenu').on('mouseover mouseleave click',function(e) {
        $(this).toggleClass('ns-active', e.type === 'mouseover');
    });

    var currentLocation = top.window.location.href;
    var isOnPageNotFound = isPageNotFoundPage();
    if( currentLocation && ! isOnPageNotFound &&
		document.getElementById(CATEGORY_PAGE_DIV_ID) == null &&
		document.getElementById(SECTION_TITLE_PAGE_DIV_ID) == null )
    {
        if(!isCaseSubmissionPage() && !isPleaseSearchPage())
        {
            addFeedbackDiv();
        }

        if (!bTocHidden && !(isSearchResultsPage() || isMobileClient))
        {
            // wait until the TOC is loaded and then select current page
            tree_helpcenter.setOnloadAction("navigateTreeToCurrentFile();");
        }
	}
}

function processDynamicContent(projectName, callback)
{
    var spans = document.getElementsByTagName("span");
    for(var i=0; i<spans.length; i++)
	{
		if(spans[i].getAttribute("dynid") != null && spans[i].getAttribute("dynid").length>0)
		{
			var sHtmlId = spans[i].id;
            if(sHtmlId.indexOf("message") > 0)
                writeMessage(spans[i].getAttribute("dynid"), sHtmlId);
            else if(sHtmlId.indexOf("feature") > 0)
				writeFeatureNotAvaiable(spans[i].getAttribute("dynid"), sHtmlId);
            else if(sHtmlId.indexOf("checker") > 0)
                writeChecker(spans[i].getAttribute("dynid"), sHtmlId);
            else if(sHtmlId.indexOf("script") > 0)
                writeScript(spans[i].getAttribute("dynid"), sHtmlId);
			else if(sHtmlId.indexOf("tabname") > 0)
				writeTabName(spans[i].getAttribute("dynid"), sHtmlId);
			else if(sHtmlId.indexOf("taskcategory") > 0)
				writeTaskCategory(spans[i].getAttribute("dynid"), sHtmlId);
            else if(sHtmlId.indexOf("categoryheading") > 0)
                writeTaskCategoryHead(spans[i].getAttribute("dynid"), sHtmlId);
			else if(sHtmlId.indexOf("newburst") > 0)
				writeNewBurst(spans[i].getAttribute("dynid"), sHtmlId);
            else if(sHtmlId.indexOf("taskpath") >0)
                writeTaskPath(spans[i].getAttribute("dynid"), sHtmlId);

            
            
            spans[i].id = sHtmlId + ":" + spans[i].getAttribute("dynid");
            
            spans[i].name = sHtmlId + ":" + spans[i].getAttribute("dynid");
        }
	}

    return nlGetDynamicContent(document, projectName, callback);
}

function processAnchor()
{
    if (!window.location.hash || window.location.hash.length < 2)
        return;

    var elementToCenterAt = document.getElementById(window.location.hash.slice(1));
    if (elementToCenterAt)
    {
        elementToCenterAt.scrollIntoView();
    }
}

function processSearchResultsPage()
	{
    var anchors = document.getElementsByTagName("a");
		for(var j=0; j<anchors.length; j++)
		{
			if(anchors[j].href != null && anchors[j].href.indexOf('shopping.') > 0)
			{
				if(anchors[j].nextSibling.nextSibling.className == 'isthiskb')
					anchors[j].nextSibling.nextSibling.style.visibility = 'visible';
			}
		}
	}

function addFeedbackDiv()
{
    if (isMobileClient) {
        return;
    }

    var isOnSearchResultsPage = isSearchResultsPage();
    var feedbackDiv = document.createElement("div");
    feedbackDiv.align = "center";
    feedbackDiv.style.paddingTop = "20px";
    feedbackDiv.style.paddingBottom = "10px";
    feedbackDiv.id = "helpcenter_feedback";
    feedbackDiv.innerHTML = isOnSearchResultsPage ? sSearchFeedbackHtml : sFeedbackHtml;

    var body = document.getElementById(CONTENT_AREA_ID);
    body.appendChild(feedbackDiv);
}

function showButtonToCaseSubmission()
{
    var link = "<div class=\"to_case\" id=\"to_case\"><a href=\"/app/help/helpcenter.nl?fid=case_submission\">...or continue to create a case online >>></a></div>"
    NS.jQuery(".search_results").append(link);
}

function addFeedbackAndShowButton()
{
    addFeedbackDiv();
    if(window.pagetype == 6)
        showButtonToCaseSubmission();
}
function navigateTreeToCurrentFile()
{
    if (isMobileClient) {
        return;
    }

    var fid = getFileAliasForCurrentPage();
    fid = constructFidWithExtension(fid);

    navigateTreePath(fid, true);
}

function showLocateButton(doc, bShow)
{
	var locateButton = doc.getElementById("locateicon_a");
	var locateText = doc.getElementById("locatetext_a");

	if(locateButton != null)
		locateButton.style.display = bShow ? "" : "none";
	if(locateText != null)
		locateText.style.display = bShow ? "" : "none";
}

function loadHelpCenterWithConcreteTopic(sTopic)
{
    var target =  HELP_SERVER_HOST_NAME + "/app/help/helpcenter.nl?topic=" + sTopic;
    displayPage(target);
	}

function loadHelpCenterWithConcreteFid(sFid)
{
    sFid = constructFidWithExtension(sFid);
    var finalPageUrl = HELP_SERVER_HOST_NAME + "/app/help/helpcenter.nl?fid=" + sFid;
    displayPage(finalPageUrl);
}

function startDrag()
{
	clearTimeout(setDragPrefTimer);
	if(bTocHidden)
    {
        tocPaneWidth = 0;
        bTocHidden = false;
        toggleShowHideLinks(true);
        setShowLeftPaneTimer = setTimeout("setDelayedPref('HELP_LEFTPANESHOW', 'T');", 1000);
    }

	startingTocPaneWidth = tocPaneWidth;
	top.bDragging = true;
	setCursorMode("W-resize");
}

function manageDrag(mousePosition)
{
    var width = mousePosition;
	if(width < MINIMUM_LEFT_PANE_WIDTH)
		tocPaneWidth = 0;
	else
		tocPaneWidth = Math.min( width, 700);

    setTocBarWidth();
}

function setTocBarWidth()
{
    if(bTocHidden)
	{
        var newTocWidth = 0;
        var newContentAreaWidth = pageBody.offsetWidth - dragbar.offsetWidth - 1;
	}
    else
    {
        var newTocWidth = tocPaneWidth;
        var newContentAreaWidth = pageBody.offsetWidth - tocPaneWidth - dragbar.offsetWidth - 1;
}

    toc.style.width = newTocWidth + "px";
    helpContentArea.style.width = newContentAreaWidth + "px";
}

function setCursorMode(mode)
{
	try
	{
		document.body.style.cursor = mode;
	}
	catch (e)
	{
		// do nothing
	}
}

function endDrag()
{
	if(top.bDragging)
	{
		clearTimeout(setDragPrefTimer);
		if(tocPaneWidth < (MINIMUM_LEFT_PANE_WIDTH+1))
		{
            showHideTocPane();
            tocPaneWidth = startingTocPaneWidth;
		}
		else
		{
			setDragPrefTimer = setTimeout("setDelayedPref('HELP_LEFTPANEWIDTH',"+tocPaneWidth+");", 1000);
			}
		}
	top.bDragging = false;
	setCursorMode("default");
	}

function setDelayedPref(pref,value)
{
	var queryString = "pref="+pref+"&val="+value;
	var response = requestHelpURL( "/app/help/nshelp.nl?_setprefs=T&" + queryString, queryString, true);
}

function showHideTocPane()
{
	clearTimeout(setShowLeftPaneTimer);
	clearTimeout(setDragPrefTimer);

    toggleShowHideLinks(bTocHidden);
    bTocHidden = !bTocHidden;
	setShowLeftPaneTimer = setTimeout("setDelayedPref('HELP_LEFTPANESHOW',"+(bTocHidden ? "'F'" : "'T'")+");", 1000);

    setTocBarWidth();

    if (!bTocHidden)
	{
        navigateTreeToCurrentFile();
	}
}

function toggleShowHideLinks(bShow)
{
	if(tocToggleImage != null)
		tocToggleImage.src = (bShow ? IMG_TOC_HIDE : IMG_TOC_SHOW);
}

function setTocPaneWidthAndDisplay(iWidth, bHideToc)
{
	tocPaneWidth = iWidth;
    bTocHidden = bHideToc;

    positionHideTocImage(document, bHideToc);
    setTocBarWidth();
}

document.onselectstart = function(evnt)
{
	if(bDragging)
		return false;
};

function getHelpMouseX(evnt)
{
    return  isIE ? (window.event != null ? window.event.clientX : evnt.clientX) : evnt.pageX;
}

function getHelpMouseY(evnt)
{
    return  isIE ? (event != null ? event.clientY : evnt.clientY) : evnt.pageY;
}

function setSearchFieldValueStartSearch(sValue)
{
	getSearchField(document).value=sValue;
	initiateSearch();
}

var searchFieldElem = null;
function getSearchField(doc)
{
	if(searchFieldElem == null)
		searchFieldElem = doc.getElementById(SEARCH_FIELD_ID);
	return searchFieldElem;
}

function displayPage(sUrl)
{
    top.window.location = sUrl;
}

function initiateSearch(sDefaultMessage, sErrorMessage)
{
	var searchField = getSearchField(document);
	var searchString = trim(searchField.value);
	if(searchString == null || searchString.length < 1)
	{
		alert(sErrorMessage);
		searchField.className = "input";
		searchField.value='';
		searchField.focus();
		searchField.select();
	}
	else
	{
		var searchResultsUrl = "/app/help/helpsearch.nl?q="+escape(searchString).replace(/\+/g, '%2B');
        if (window.pagetype == 5 || window.pagetype == 6)
        {
            window.pagetype = 6;
        }
        else
        {
            window.pagetype = 3;
        }
        displayPageInContentArea(searchResultsUrl, addFeedbackAndShowButton);
	}
}

function searchGoToPage(sParameters)
{
    var searchResultsUrl = "/app/help/helpsearch.nl?" + sParameters;
    displayPageInContentArea(searchResultsUrl, addFeedbackDiv);
}

function displayPageInContentArea(sUrl, callback)
{
    try
{
        NS.jQuery('#' + CONTENT_AREA_ID).load(sUrl, callback);
}
    catch(err)
{
        displayNotFoundPage();
}
}

function displayNotFoundPage()
{
    loadHelpCenterWithConcreteTopic("DOC_PAGENOTFOUND");
    window.pagetype = 2;
}

function isSearchArea(sDefaultMessage, sErrorMessage, event)
{
    var offsetx = event.offsetX || (event.pageX - event.target.offsetLeft);
    var searchInputWidth = document.getElementById(SEARCH_FIELD_ID).offsetWidth;
    if(offsetx + SEARCH_ICON_WIDTH > searchInputWidth)
    {
        initiateSearch(sDefaultMessage, sErrorMessage);
    }
}

function printContentFrame()
{
    if (helpContentArea)
    {
        window.print();
    }
}

function getFileAliasForCurrentPage()
{
    // file aliases are stored in <h1 class="nshelp_title" id="FILE_ALIAS"/> tags
    return NS.jQuery('h1.nshelp_title[id]').attr('id');
}

function constructFidWithExtension(fid)
{
    if (fid == null)
        return null;

    // file alias without extension
    if (!fid.match('.+\.(html|htm|pdf)$'))
        fid = fid + ".html";

    return fid
}

function navigateTreePath(sFileName, bExpandToc)
{
	if(bTocHidden && bExpandToc)
		showHideTocPane();

	var queryString = "_htmlfile="+sFileName;
    var response = requestHelpURL( "/app/help/genhelp.nl?method=gennodepath&_project=" + window.basehelpproject + "&" + queryString, queryString, false);

	if ( response != null )
        tree_helpcenter.focus(getTreePathFromRequest( response ), true);
}

function getTreePathFromRequest(response)
{
	var sText = response.getBody();
    var doc = top.nsStringToXML(sText);

    var items = doc.getElementsByTagName('item');
    if (processReturnStatus(doc) != STATUS_HELP_OK)
        return;

    var paths = doc.getElementsByTagName('path');
    if (!paths || paths.lengh == 0)
        return "";

    var firstPath = paths[0].firstChild;
    if (!firstPath)
        return "";

    var path = firstPath.nodeValue;
    if (path == null || path == "undefined")
        path = "";

	return path;
}

function showUserGuideLinks()
{
    loadHelpCenterWithConcreteTopic(getUserGuidesTopicAlias());
}

function showSuiteApps()
{
    loadHelpCenterWithConcreteTopic(getSuiteAppsTopicAlias());
}

function showReleaseNotes()
	{
    loadHelpCenterWithConcreteTopic(getReleaseNotesTopicAlias());
	}

function showFrontPage()
	{
    loadHelpCenterWithConcreteTopic(getFrontPageTopicAlias());
	}
function showTrainingVideos()
{
    loadHelpCenterWithConcreteTopic(getTrainingVideos());
}

function correctSearchSpelling(sCorrection)
{
	getSearchField(top.headerbar.document).value = unescape(sCorrection).replace(/\+/g, ' ');
}

function positionHideTocImage(doc, bHideToc)
{
	var img = doc.createElement("IMG");
	if(bHideToc)
        img.src = IMG_TOC_SHOW;
	else
		img.src = IMG_TOC_HIDE;

	img.border = 0;
	img.width = 15;
	img.height = 15;
	img.id = "showhidetoc";
	tocToggleImage = img;
	var container = doc.getElementById("hidetocholder");
    container.appendChild(img);
}

function submitFeedback(doc)
{
	var rating = -1;
	var scores = doc.getElementsByName("scores");
    for(var i=0; i < scores.length; i++)
    {
      	if (scores[i].checked)
		{
			rating = scores[i].value;
			break;
		}
	}
	if(rating == -1)
	{
		alert("Please make a selection");
		return false;
	}
	else
	{
		var currentUrl = getFileAliasForCurrentPage();
		var feedback_url = doc.location;
		var comments_fld = doc.getElementById("nshelp_feedback_comments");
		if(feedback_url.length > 300)
			feedback_url = feedback_url.substring(feedback_url.length-299,feedback_url.length-1);
		var queryString = "url=" + escape(feedback_url);
		queryString += "&rating=" + rating;
		if(comments_fld != null && comments_fld.value != sTEXT_AREA_HELPER_TEXT)
			queryString += "&comments=" + escape(comments_fld.value);
		if(isSearchResultsPage())
			queryString += "&taskid=" + "search:"+escape(getSearchTermFromUrl());
		else
			queryString += "&taskid=" + "file:"+escape(currentUrl);
		requestHelpURL("/app/center/feedback/feedback.nl?method=save&" + queryString + "&ts=" + (new Date().getTime()), queryString);

        var outside = doc.getElementById("outerfeedback");
        outside.width = "100%";
		var inside = doc.getElementById("innerfeedback");
		inside.style.textAlign = "center";
        var msg = "Thank you for your feedback!";
		inside.innerHTML = "<BR>" + uir_getAlertBoxHtml(null, msg, NLAlertDialog.TYPE_LOWEST_PRIORITY, null, null, null, HELP_SERVER_HOST_NAME);
	}
}