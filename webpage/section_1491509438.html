<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_4563537302" href="./book_4563537302.html">SuiteScript 2.0</a>
            <a rev="part_4563537633" href="./part_4563537633.html">SuiteScript 2.0 API</a>
            <a rev="chapter_4387172495" href="./chapter_4387172495.html">SuiteScript 2.0 Script Types and Entry Points</a>
            <a rev="section_4387799161" href="./section_4387799161.html">Map/Reduce Script Type</a>
            <a rev="section_1490987232" href="./section_1490987232.html">Handling Server Restarts in Map/Reduce Scripts</a>
            <a class="nshelp_navlast" rev="section_1491509438" href="./section_1491509438.html">Execution of Restarted Map/Reduce Stages</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_1491509438" class="nshelp_title">Execution of Restarted Map/Reduce Stages</h1>
      <p>Keep in mind that processing a map/reduce script can involve many jobs. The getInput, shuffle, and summarize stages are each processed with a single job. However, any number of jobs can participate in the map and reduce stages. Within a map stage or a reduce stage, jobs can run in parallel. Any of the jobs can be forcefully terminated at any moment. The impact of this event depends on the status of the job (what it was doing), and in which stage it was running.</p>
      <p>For details, see the following topics:</p>
      <ul class="nshelp">
        <li>
          <p>
            <a rev="section_1491509438" href="#bridgehead_1490988848">Termination of getInput Stage</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_1491509438" href="#bridgehead_1490988890">Termination of Shuffle Stage</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_1491509438" href="#bridgehead_1490988950">Termination of Parallel Stages</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_1491509438" href="#bridgehead_1490989062">Termination of Summarize Stage</a>
          </p>
        </li>
      </ul>
      <h5 id="bridgehead_1490988848">Termination of getInput Stage</h5>
      <p>The work of a serial stage (getInput, shuffle, and summarize stages) is done in a single job. If the getInput stage job is forcefully terminated, it is later restarted. The getInput portion of the script can find out whether it is the restarted execution by examining the <code>isRestarted</code> attribute of the context argument (<a rev="section_4549604333" href="./section_4549604333.html">GetInputContext.isRestarted</a>). The script is being restarted if and only if <code>(context.isRestarted === true)</code>.</p>
      <p>Note that the input for the next stage is computed from the return value of the getInput script. Next stage input is written after the getInput stage finishes. Therefore, even the restarted getInput script is expected to return the same data. The map/reduce framework helps to ensure that no data is written twice.</p>
      <p>However, if the getInput script is changing some additional data (for example, creating NetSuite records), it should contain code to handle duplicated processing. The script needs idempotent operations to ensure that these records are not created twice, if this is undesired.</p>
      <h5 id="bridgehead_1490988890">Termination of Shuffle Stage</h5>
      <p>The shuffle stage does not contain any custom code, so if the shuffle stage job is forcefully terminated, it is later restarted and all the work is completely redone. There is no impact other than that the stage takes longer to finish.</p>
      <h5 id="bridgehead_1490988950">Termination of Parallel Stages</h5>
      <p>Map and reduce stages can execute jobs in parallel, so they are considered parallel stages. An application restart will affect parallel stages in the same way. The following example covers impact of restart during the map stage. Note that termination of a reduce stage will behave very similarly.</p>
      <p>The purpose of a map stage is to execute a map function on each key/value pair supplied by the previous stage (getInput). Multiple jobs participate in the map stage. Map jobs will claim key/value pairs (or a specific number of key/value pairs) for which the map function was not executed yet. The job sets a flag for these key/value pairs so that no other job can execute the map function on them. Then, the job sequentially executes the map function on the key/value pairs it flagged. The map stage is finished when the map function is executed on all key/value pairs.</p>
      <p>The number of jobs that can participate on the map stage is unlimited. Only the maximum concurrency is limited. Initially, the number of map jobs is equal to the selected concurrency in the corresponding map/reduce script deployment. However, to prevent a single map/reduce task from monopolizing all computational resources in the account, each map job can yield itself to allow other jobs to execute. The yield creates an additional map job and the number of yields is unlimited.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">This is a different type of yield compared to yield in a SuiteScript 1.0 scheduled script. In SuiteScript 1.0, the yield happens in the middle of a script execution. In a map job, the yield can happen only between two map function executions, and not in the middle of one.</p>
      </div>
      <br>
      <p>If a map job is forcefully terminated, it is later restarted. First, the job executes the map function on all key/value pairs that it took and did not mark finished before termination. It is the only map job that can execute the map function on those pairs. They cannot be taken by other map job. After those key/value pairs are processed, the map job continues normally (takes other unfinished key/value pairs and executes the map function on them).</p>
      <p>In some cases, the map function can be re-executed on multiple key/value pairs. The number of pairs that a map function can re-execute will depend on the buffer size selected on the deployment page. The buffer size determines the number of key/value pairs originally taken in a batch. The job marks the batch as finished only when the map function is executed on all of them. Therefore, if the map job is forcefully terminated in the middle of the batch, the entire batch will be processed from the beginning when the map job is restarted.</p>
      <p>Note that the map/reduce framework deletes all key/value pairs written from a partially-executed batch, so that they are not written out twice. Therefore, the map function does not need to check whether or not <a rev="section_4472712995" href="./section_4472712995.html">MapContext.write(key,value)</a> for a particular key/value has already been executed. However, if the map function is changing some additional data, it must also be designed to use idempotent operations. For example, if a map function created NetSuite records, the script should perform additional checks to ensure that these records are not created twice, if this is undesired.</p>
      <p>To check if a map function execution is a part of a restarted batch, the script must examine the <code>isRestarted</code> attribute in the context argument (<a rev="section_4540852756" href="./section_4540852756.html">MapContext.isRestarted</a>). The map function is in the restarted batch if and only if <code>(context.isRestarted === true)</code>.</p>
      <p>Be aware that a restarted value of <code>true</code> is only an indication that some part of the script might have already been executed. Even if <code>context.isRestarted === true</code>, a map function could run on a particular key/value for the first time. For example, the map job was forcefully terminated after the map job took the key/value pair for processing, but before it executed the map function on it. This is more likely to occur if a high buffer value is set on the map/reduce deployment. For more information about the buffer, see <a rev="section_4685215171" href="./section_4685215171.html#bridgehead_4688758032">Optimizing Processing Speed during Map and Reduce Stages</a>.</p>
      <h5 id="bridgehead_1490989062">Termination of Summarize Stage</h5>
      <p>If the summarize stage job is forcefully terminated, it is later restarted. The summarize portion of the script can find out whether it is the restarted execution by examining the <code>isRestarted</code> attribute of the summary argument (<a rev="section_4540094342" href="./section_4540094342.html">Summary.isRestarted</a>).</p>
      <p>The script is being restarted if and only if <code>(summary.isRestarted === true)</code>.</p>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

