<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2453322" href="./set_N2453322.html">SuiteCommerce</a>
            <a rev="book_4260168573" href="./book_4260168573.html">SuiteCommerce Advanced</a>
            <a rev="part_4387345266" href="./part_4387345266.html">Developer’s Guide</a>
            <a rev="section_4307434462" href="./section_4307434462.html">SuiteCommerce Advanced Architecture</a>
            <a rev="section_4307437864" href="./section_4307437864.html">Module Architecture</a>
            <a rev="section_4307439952" href="./section_4307439952.html">Models, Collections, and Services</a>
            <a rev="section_4484201002" href="./section_4484201002.html">Frontend Models</a>
            <a class="nshelp_navlast" rev="section_4484187863" href="./section_4484187863.html">Asynchronous Data Transactions</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4484187863" class="nshelp_title">Asynchronous Data Transactions</h1>
      <p><span id="dyn_help_feature" dynid="SUITECOMMERCEENTERPRISE">&nbsp;</span>   </p>
      <p>When working with data stored in NetSuite or internally within the application, SuiteCommerce Advance often needs to perform tasks asynchronously. Handling tasks asynchronously improves the performance and user experience of SuiteCommerce Advanced.</p>
      <p>To handle tasks asynchronously, SuiteCommerce Advanced uses the jQuery Deferred Object API. This API provides an easy way of registering callbacks and monitoring the success or failure of data transactions. See https://api.jquery.com/category/deferred-object/ for information on the methods provided by the Deferred Object API.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Although there are other mechanisms JavaScript for performing tasks asynchronously, NetSuite recommends that you use jQuery Deferred Objects when customizing or extending SuiteCommerce Advanced.</p>
      </div>
      <br>
      <p>There are two main contexts where SuiteCommerce Advanced uses the Deferred Object API:</p>
      <ul class="nshelp">
        <li>
          <p>When performing AJAX calls when using the <code>save()</code>, <code>fetch()</code>, and <code>destroy()</code> methods of a model or collection.</p>
        </li>
        <li>
          <p>When performing tasks asynchronously using promises</p>
        </li>
      </ul>
      <h4 id="bridgehead_4502624150">Using the Deferred Object in AJAX Calls</h4>
      <p>SuiteCommerce Advanced frequently interacts with data stored in NetSuite. To perform a data transaction, code within a Router or View calls a method on the front-end model. This method then makes an AJAX call that sends an HTTP request to the service.</p>
      <p>In general, this AJAX call needs to be asynchronous since other application code may need to continue processing while the data is loading. The primary reason for using asynchronous AJAX calls is that each browser window operates within a single thread. A synchronous AJAX call will freeze the browser until the AJAX call is complete.</p>
      <p>One frequent use of the deferred object is to wait until the AJAX call is complete to perform other tasks.</p>
      <p>The following example from Case.Detail.View shows how the done method is used when saving data to NetSuite:</p>
      <pre class="nshelp">this.model.save().done(function()
{
    self.showContent();
    self.showConfirmationMessage(_('Case successfully closed').translate());
    jQuery('#reply').val('');
});
</pre>
      <p>In this example, the done method accepcts a single function as an argument. This function calls the <code>showContent()</code> method of the view and performs other tasks. This defers calling the <code>showContent()</code> method until the data transaction initiated by the save method is completed.</p>
      <h4 id="bridgehead_4502624563">Using Promises in Asynchronous Data Transfers</h4>
      <p>SuiteCommerce Advanced also uses the Deferred Object API in other contexts where tasks need to be performed asynchronously. In this case, a module explicitly creates an instance of the jQuery.Deferred object. After creating this object the module must ensure that the deferred object is ultimately resolved. This resolution is handled using promises.</p>
      <p>Promises are part of the jQuery Deferred Object that enable asynchronous data transfers. Promises prevent other methods from interfering with the progress or status of the request. The following code example from the the ProductList.CartSaveForLater.View module shows how a deferred object is created and ultimately resolved:</p>
      <pre class="nshelp">addItemToList: function (product)
      {
         var defer = jQuery.Deferred();

         if (this.validateGiftCertificate(product))
         {
            var self = this;


            this.application.ProductListModule.Utils.getSavedForLaterProductList().done(function(pl_json)
            {
               if (!pl_json.internalid)
               {
                  var pl_model = new ProductListModel(pl_json);

                  pl_model.save().done(function (pl_json)
                  {
                     self.doAddItemToList(pl_json.internalid, product, defer);
                  });
               }
               else
               {
                  self.doAddItemToList(pl_json.internalid, product, defer);
               }
            });
         }
         else
         {
            defer.resolve();
         }

         return defer.promise();
      }
</pre>
      <h4 id="section_4489106459">Stopping AJAX Requests</h4>
      <p>SuiteCommerce Advanced includes an AjaxRequestsKiller utility module used to stop execution of AJAX requests. Performance is enhanced by stopping AJAX requests that are no longer needed. This module is primarily used in two contexts:</p>
      <ul class="nshelp">
        <li>
          <p><strong>URL changes</strong>: When a URL change is detected, this module stops execution of all pending AJAX requests that were initiated by another Router or View. Some AJAX requests are initiated without a URL change, for example, the <code>collection.fetch</code> and <code>model.fetch</code> methods.&nbsp;In such cases, you should pass the <code>killerID</code> property to the method as in the following:</p>
          <pre class="nshelp">model.fetch({
   killerId: AjaxRequestsKiller.getKillerId()
})</pre>
        </li>
        <li>
          <p><strong>Site Search</strong>: The SiteSearch module uses the AjaxRequestsKiller module to delete unnecessary AJAX calls when using the type ahead functionality. As a user enters a character in the search field, the SiteSearch module sends an AJAX request to retrieve information from the Item Search API. As the user continues entering characters, additional AJAX requests are sent. Using the AjaxRequestsKiller improves performance be deleting old requests.</p>
        </li>
      </ul>
      <p>When the value of the <code>killerID</code> property is reset, this module aborts the AJAX request associated with the old value of the <code>killerID</code> property. It also sets the <code>preventDefault</code> property of the request to <code>true</code> to avoid sending an error to the ErrorHandling module.</p>
      <p>This module defines the following properties and methods:</p>
      <ul class="nshelp">
        <li>
          <p><code>getKillerId</code> (method): returns an unique ID that identifies the AJAX request. Each time the URL is reset, a new ID is generated.</p>
        </li>
        <li>
          <p><code>getLambsToBeKilled</code> (method): returns an array of AJAX request IDs to be halted.</p>
        </li>
        <li>
          <p><code>mountToApp</code> (method): loads the module into the application context. This method performs the following</p>
          <ul class="nshelp">
            <li>
              <p>Sets the value of the <code>killerID</code> property.</p>
            </li>
            <li>
              <p>Pushes the <code>killerID</code> property to an array.</p>
            </li>
            <li>
              <p>When the <code>killerID</code> property is reset, calls the abort method on the AJAX request.</p>
            </li>
          </ul>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4307439952" href="./section_4307439952.html">Models, Collections, and Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4484201002" href="./section_4484201002.html">Frontend Models</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4484202570" href="./section_4484202570.html">Collections</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4484203090" href="./section_4484203090.html">Services and Backend Models</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

