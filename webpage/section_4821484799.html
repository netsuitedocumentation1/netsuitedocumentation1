<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_4423275132" href="./set_4423275132.html">Order Management</a>
            <a rev="book_4416316451" href="./book_4416316451.html">Order Fulfillment and Shipping</a>
            <a rev="chapter_4440622374" href="./chapter_4440622374.html">Automating Order Fulfillment</a>
            <a rev="section_4440622680" href="./section_4440622680.html">Automatic Location Assignment</a>
            <a rev="section_4482928271" href="./section_4482928271.html">Creating Configurations and Rules</a>
            <a rev="section_4485469716" href="./section_4485469716.html">Creating Automatic Location Assignment Rules</a>
            <a class="nshelp_navlast" rev="section_4821484799" href="./section_4821484799.html">Choosing a Fulfillment Strategy</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4821484799" class="nshelp_title">Choosing a Fulfillment Strategy</h1>
      <p><span id="dyn_help_feature" dynid="AUTOLOCATIONASSIGNMENT">&nbsp;</span>   </p>
      <p>You use configurations and rules to implement your desired automatic location assignment strategy. In a rule, a location assignment strategy determines the way in which fulfillment locations are assigned to sales order lines. A strategy is applied in a rule after locations have been filtered according to type and distance to the shipping address. The strategies available include the following:</p>
      <ul class="nshelp">
        <li>
          <p>You can choose whether to <strong>minimize fulfillment locations</strong> in the rule. By default, NetSuite minimizes fulfillment locations and tries to assign as few locations as possible.</p>
        </li>
        <li>
          <p>You can choose one of the following strategies to determine the optimal fulfillment location:</p>
          <ul class="nshelp">
            <li>
              <p><strong>Closest location</strong>. Assigns the location that is closest to the shipping address.</p>
            </li>
            <li>
              <p><strong>Highest ranked location</strong>. Assigns the highest ranked location in the region in which the shipping address is located.</p>
            </li>
            <li>
              <p><strong>Fulfillment workload distribution</strong>. Assigns the location with the current highest available work capacity.</p>
            </li>
          </ul>
        </li>
      </ul>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/OrderManagement/OrderFulfillmentShipping/ALARuleRecordStrategies.png" width="630px" height="314px">
      <h3 id="bridgehead_4642546657">Minimize Fulfillment Locations</h3>
      <p>When you choose to minimize fulfillment locations, NetSuite attempts to assign as few locations as possible, taking into account locations already assigned on other line items in the order. Minimizing the total number of fulfillment locations can help to reduce shipping costs and speed delivery. If you choose not to minimize locations, NetSuite assigns locations to sales order lines on a line-by-line basis.</p>
      <p>Within the minimize fulfillment locations option you can choose how restrictive you want to be with the maximum number of locations assigned. You do this by specifying the maximum number of locations to be assigned. You can choose the number of locations in the following ways:</p>
      <ul class="nshelp">
        <li>
          <p>1 Location, 2 Locations, 3 Locations, 4 Locations, 5 Locations</p>
          <p>You can indicate the maximum number of fulfillment locations (between one and five) you want to be assigned between all lines in the sales order. Selecting one location is similar to setting up the order so that it can be shipped complete.</p>
          <p>If there are multiple shipping addresses in the sales order, the rules are evaluated separately for each address. For example, if there are two shipping addresses in a sales order, NetSuite evaluates all rules for items shipping to the first shipping address and then evaluates all rules for the second shipping address. To use multiple shipping addresses, the Multiple Shipping Routes feature must be enabled – see <a rev="section_N1263041" href="./section_N1263041.html">Multiple Shipping Routes</a>.</p>
          <p>Specifying a lower number of locations typically results in less shipments and therefore reduced shipment costs. However, a lower number of locations is also more restrictive, which might prevent the rule from assigning locations to all lines. Specifying a higher number of locations increases the likelihood that all lines in the order will be assigned a location, but might split lines in an order between several locations and result in increased shipping costs.</p>
          <div class="nshelp_note">
            <h3>Note</h3>
            <p class="nshelp_first">The number of locations you specify is the <u>maximum</u> number of locations that will be assigned automatically. The actual number of locations assigned might be less depending on the inventory available at each fulfillment location.</p>
          </div>
          <br>
        </li>
        <li>
          <p>No Limit</p>
          <p>NetSuite attempts to minimize the number of fulfillment locations in the sales order, but will use as many locations as required. This is the most flexible option because it helps to ensure a location is assigned to each sales order line, especially if inventory is spread between multiple locations. This is the default option when minimizing fulfillment locations.</p>
        </li>
      </ul>
      <p>If you choose not to minimize fulfillment locations in the rule, NetSuite evaluates sales order lines individually. Fulfillment locations already assigned to other lines in the sales order are not taken into account. Not minimizing fulfillment locations might result in more locations being assigned overall to a sales order.</p>
      <p>When NetSuite starts to evaluate a rule, a sales order might already have a location assigned to one or more lines. This can happen when you assign a location manually (perhaps at the same time you create the sales order) or when a previous rule has assigned a location to some – but not all – of the lines. If a line already has a location assigned to it, the maximum number of locations refers to <u>additional</u> locations when the rule is evaluated. For example, if one of the lines in a sales order already has a location, and Minimize Fulfillment Locations is set to 1 Location, the engine assigns a maximum of two locations to the lines in the sales order.</p>
      <h3 id="bridgehead_4642547156">Closest Location</h3>
      <p>The location that is closest to the shipping address is assigned to the sales order line. NetSuite determines the closest location by measuring the straight line distance between each fulfillment location and the shipping address. The geographic position of a fulfillment location is based on its postal code, or the latitude and longitude coordinates entered manually on the location record. The geographic position of the shipping address is based on the postal code in the address details.</p>
      <p>For more information about the use of latitude and longitude coordinates, see <a rev="section_4440701430" href="./section_4440701430.html#bridgehead_4486411633">Geolocation Method</a>.</p>
      <h3 id="bridgehead_4642600530">Highest Ranked Location</h3>
      <p>The highest ranked location in the region in which the shipping address is located is assigned to a line. You can create multiple regions and rank locations differently in each region. Regions define the geographical areas in which shipping addresses are located. For more information about location ranking, see <a rev="section_4440704638" href="./section_4440704638.html">Setting Up Regions for Automatic Location Assignment</a>.</p>
      <h3 id="bridgehead_4642600757">Fulfillment Workload Distribution (FWD)</h3>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The fulfillment workload distribution strategy is only available when the Fulfillment Request feature is enabled.</p>
      </div>
      <br>
      <p>Use the fulfillment workload distribution strategy when you want to assign fulfillment locations to sales order lines based on the available work capacity of locations. Locations with a higher shipping capacity will be assigned a higher ratio of orders. Choosing this strategy in a rule distributes orders evenly across locations. NetSuite uses two measures to calculate the capacity of a location:</p>
      <ul class="nshelp">
        <li>
          <p>The total shipping capacity of a location. You specify the total shipping capacity on a per location basis. Each location can have a different total shipping capacity.</p>
        </li>
        <li>
          <p>The total number of open orders already assigned to the location. NetSuite measures open orders by counting the number of pending fulfillment requests at a location.</p>
        </li>
      </ul>
      <p>Total shipping capacity is the maximum number of orders that employees can process and ship from a location. Only orders that need to be shipped count towards the total shipping capacity. Other orders, such as in-store pick up orders or transfer orders, do not count towards the total shipping capacity. Automatic location assignment continues assigning orders to a location until the total shipping capacity is reached. When employees start processing orders at a location, the work capacity at the location increases and automatic location assignment can resume allocating orders to the location.</p>
      <p>Fulfillment workload distribution measures capacity by counting the number of open fulfillment requests at locations. An open fulfillment request is a fulfillment request that has been acknowledged by employees at the location but has not yet been shipped to the customer. Open fulfillment requests have one of the following statuses: In Progress, Picked, Packed, and Partially Fulfilled. The number of line items in the fulfillment request is irrelevant as regards capacity.</p>
      <p>To use FWD, you need to complete several setup tasks:</p>
      <ul class="nshelp">
        <li>
          <p>Verify the Fulfillment Request feature is enabled. See <a rev="section_4685934956" href="./section_4685934956.html">Enabling Automation Features</a>.</p>
        </li>
        <li>
          <p>Specify the total shipping capacity at each location. If you omit the total shipping capacity on a particular location record, you can still specify a default total shipping capacity in a location assignment rule. When the automatic location assignment engine runs, the default capacity is applied to all locations that do not have a total shipping capacity defined.</p>
        </li>
        <li>
          <p>Create a fulfillment workload distribution rule in the configuration.</p>
        </li>
        <li>
          <p>Make sure fulfillment requests are set to be created automatically. See <a rev="section_4642548703" href="./section_4642548703.html">Automation for Fulfillment Requests</a> for more information.</p>
        </li>
      </ul>
      <p>NetSuite recalculates the work capacity of locations each time the automatic location assignment rules are evaluated. NetSuite assigns the sales order to the location with the current highest available work capacity. The current highest capacity is calculated as the ratio of current open fulfillment requests at the location to the total shipping capacity specified on the location record. Locations with a lower ratio have a higher work capacity. For example, if a location has three open fulfillment requests and its total shipping capacity is “8”, the calculated work capacity is 0.375 (3 divided by 8). The location has used 37.5% of its total shipping capacity.</p>
      <div class="nshelp_informalexample">
        <p>
          <strong>Example</strong>
        </p>
        <p>You create a rule with fulfillment workload distribution as a strategy. You have two locations, A and B, with a total shipping capacity of 10 and 15 respectively. Location A currently has two open fulfillment requests. Location B currently has seven open fulfillment requests. Both locations have sufficient inventory available when a new sales order is created. When automatic location assignment evaluates the rule, it calculates the current available work capacity for location A as 0.2 (2 divided by 10) and the work capacity for location B as 0.46 (7 divided by 15). Location A is assigned to the sales order because it has the current highest work capacity.</p>
      </div>
      <div class="nshelp_procedure">
        <h4>To set the fulfillment strategy in a rule:</h4>
        <ol class="nshelp">
          <li>
            <p>In an automatic location assignment configuration, create a new rule or open the rule you want to work with. See <a rev="section_4485469716" href="./section_4485469716.html">Creating Automatic Location Assignment Rules</a> for more information.</p>
          </li>
          <li>
            <p>Under Strategies, choose the fulfillment strategies you want to use in the rule:</p>
            <ol type="a" class="nshelp_substeps">
              <li>
                <p>Optimize shipping by minimizing the number of fulfillment locations in the sales order.</p>
                <p>To use the least number of locations possible across all lines in the sales order, check the <strong>Minimize Fulfillment Locations</strong> box. Then, in the <strong>Limit Number Of Locations To</strong> field, select the maximum number of locations you want the rule to assign. For example, you can select 1 Location if you prefer to send all items in the order in one shipment from a single fulfillment location.</p>
              </li>
              <li>
                <p>Select an optimal fulfillment strategy:</p>
                <ul class="nshelp">
                  <li>
                    <p><strong>Closest Location</strong> – The location that is closest (in a straight line) to the shipping address is assigned to the sales order line.</p>
                  </li>
                  <li>
                    <p><strong>Highest Ranked Location</strong> – The top ranked location is assigned to the sales order line.</p>
                  </li>
                  <li>
                    <p><strong>Fulfillment Workload Distribution</strong> – The location with the highest capacity is assigned to the sales order line.</p>
                    <p>You must also enter a default total shipping capacity for locations. If the total shipping capacity is not specified for a particular location, NetSuite uses the default value you enter in the rule.</p>
                    <div class="nshelp_note">
                      <h3>Note</h3>
                      <p class="nshelp_first">The fulfillment workload distribution strategy is only available when the Fulfillment Request feature is enabled. See <a rev="section_4642548703" href="./section_4642548703.html">Automation for Fulfillment Requests</a> for more information.</p>
                    </div>
                    <br>
                  </li>
                </ul>
              </li>
            </ol>
          </li>
          <li>
            <p>Continue setting other criteria in the rule. See <a rev="section_4800065950" href="./section_4800065950.html">Limiting Rules to Specific Line Items</a> and <a rev="section_4800061720" href="./section_4800061720.html">Limiting Locations in a Rule</a>.</p>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4485469716" href="./section_4485469716.html">Creating Automatic Location Assignment Rules</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4800065950" href="./section_4800065950.html">Limiting Rules to Specific Line Items</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4800061720" href="./section_4800061720.html">Limiting Locations in a Rule</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

