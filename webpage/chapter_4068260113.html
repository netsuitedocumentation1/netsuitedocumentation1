<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2723865" href="./book_N2723865.html">SuiteFlow (Workflow)</a>
            <a class="nshelp_navlast" rev="chapter_4068260113" href="./chapter_4068260113.html">SuiteFlow Overview</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="chapter_4068260113" class="nshelp_title">SuiteFlow Overview</h1>
      <p>Use SuiteFlow to create and execute workflows in NetSuite. A workflow is the definition of a custom business process for a standard or custom record in NetSuite. Business processes can include transaction approval, lead nurturing, and record management. A workflow defines and automates the business process.</p>
      <p>You define workflows for a specific record type and contain the stages, or states, of a record as it moves through the business process. In each state, a workflow defines the actions to be performed, like sending emails or adding buttons to a record form, before the workflow completes or transitions to another state. A workflow can move between different states, or transition, depending on the business process requirements. The actions and transitions can contain conditions that must be met before the action or transitions execute.</p>
      <p>NetSuite starts an instance of a workflow on a record and a record transitions between states in a workflow based on specific triggers. Triggers are events that occur when records are viewed, created, or updated. You can also direct NetSuite to run workflow instances on records based on a schedule.</p>
      <p>Use the <a rev="section_4068295521" href="./section_4068295521.html">Workflow Manager Interface</a> interface in SuiteFlow to create and edit workflows. Create workflows in the Workflow Manager and a workflow instance initiates and executes according to the defined business process.</p>
      <p>The following figure shows a sample approval business process for an estimate:</p>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/SuiteFlowWorkflow/WorkflowExample_Estimate.png" width="251px" height="283px">
      <p>In the following example, a sales rep creates an Estimate record. SuiteFlow initiates an instance of the approval workflow to automate the approval process for the estimate. Workflow actions and conditions on each state determine how the estimate transitions through the approval process.</p>
      <p>The following table describes the states in the workflow and their associated actions and transitions when the sales rep creates the record:</p>
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:112px;" class="bolight_bgdark" valign="top">
						<p>State</p>
					</th>
            <th style="min-width:499px;" class="bolight_bgdark" valign="top">
						<p>Description</p>
					</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:112px;" class="bolight_bgwhite">
						<p>State 1: Entry</p>
					</td>
            <td style="min-width:499px;" class="bolight_bgwhite">
						<p>The first state a record enters in the workflow after estimate creation is State 1: Entry. This state begins the approval process.</p>
						<p>The Entry state sets the <strong>Approval Status</strong> field of the estimate to <strong>Pending Approval</strong> and then executes transitions according to the following conditions:</p>
						<ul class="nshelp"><li><p>If the sales rep has a supervisor, the estimate transitions to State 2: Pending Approval.</p></li><li><p>If the sales rep has no supervisor and the estimate value is less than $50,000, the estimate transitions to State 3b: Approved.</p></li><li><p>If the sales rep has no supervisor and the estimate value is $50,000 or greater, the estimate transitions to State 3a: Finance Approval.</p></li></ul>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>State 2: Pending Approval</p>
					</td>
            <td class="bolight_bglight">
						<p>This state locks the record to anyone other than the sales rep’s supervisor and adds <strong>Approve</strong> and <strong>Reject</strong> buttons to the record form.</p>
						<p>Each button is set up to transition to a different state. The next transition depends on the button clicked:</p>
						<ul class="nshelp"><li><p>Approve. If the estimate value is less than $50,000, record transitions to State 3b: Approved. If the estimate value is $50,000 or greater, the estimate transitions to State 3a: Finance Approval.</p></li><li><p>Reject. The estimate transitions to State 4: Rejected.</p></li></ul>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>State 3a: Finance Approval</p>
					</td>
            <td class="bolight_bgwhite">
						<p>This state locks the record to anyone other than the Finance Manager and adds <strong>Approve</strong> and <strong>Reject</strong> buttons to the record form.</p>
						<p>The next transition depends on the button clicked:</p>
						<ul class="nshelp"><li><p>Approve. The estimate transitions to State 3b: Approved.</p></li><li><p>Reject. The estimate transitions to State 4: Rejected.</p></li></ul>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>State 3b: Approved</p>
					</td>
            <td class="bolight_bglight">
						<p>Sets the <strong>Approval Status</strong> field of the estimate to <strong>Approved</strong> and the workflow completes.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>State 4: Rejected</p>
					</td>
            <td class="bolight_bgwhite">
						<p>Sets the <strong>Approval Status</strong> field of the estimate to <strong>Rejected</strong> and the workflow completes.</p>
					</td>
          </tr>
        </tbody>
      </table>
      <h2 id="bridgehead_4068260904">Getting Information About SuiteFlow</h2>
      <p>The following table describes SuiteFlow concepts and where you can get more information:</p>
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:150px;" class="bolight_bgdark" valign="top">
						<p>Concept</p>
					</th>
            <th style="min-width:461px;" class="bolight_bgdark" valign="top">
						<p>Description</p>
					</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:150px;" class="bolight_bgwhite">
						<p>Workflow Manager interface</p>
					</td>
            <td style="min-width:461px;" class="bolight_bgwhite">
						<p>Use the Workflow Manager interface to create and edit workflows. The interface includes the workflow definition page, diagrammer, and context panel. See <a rev="section_4068295521" href="./section_4068295521.html">Workflow Manager Interface</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>Workflow elements overview</p>
					</td>
            <td class="bolight_bglight">
						<p>Overview of each workflow element and links to more information on using the element. See <a rev="section_4071953058" href="./section_4071953058.html">Workflow Elements</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>Workflow audience</p>
					</td>
            <td class="bolight_bgwhite">
						<p>The types of users who can create or run workflows. See <a rev="section_4073598374" href="./section_4073598374.html">Workflow Audience</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>Workflow initiation</p>
					</td>
            <td class="bolight_bglight">
						<p>Definition for when NetSuite starts a workflow instance on a record. See <a rev="section_4080797941" href="./section_4080797941.html">Workflow Initiation</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>Tutorial</p>
					</td>
            <td class="bolight_bgwhite">
						<p>Follow the steps in the tutorial to create a workflow based on an Opportunity record. Use the steps in the tutorial to become familiar with creating the basic elements of a workflow, including states, actions, transitions, and conditions. See <a rev="chapter_N2725813" href="./chapter_N2725813.html">Creating Your First Workflow</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>Creating and editing workflows and workflow elements</p>
					</td>
            <td class="bolight_bglight">
						<p>All the procedures and description of the options required to create a workflow. See <a rev="chapter_4101515562" href="./chapter_4101515562.html">Working with Workflows</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>Administering workflows</p>
					</td>
            <td class="bolight_bgwhite">
						<p>After you create a workflow, you can perform specific administration tasks. These tasks include Searching for workflows, cancelling instances, performing mass updates, and bundling workflows. See <a rev="chapter_4103088279" href="./chapter_4103088279.html">Workflow Administration</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>Reference information</p>
					</td>
            <td class="bolight_bglight">
						<p>Get more information about the triggers and actions for workflow. Includes detailed information about each server and client trigger and each action types that you can use in a workflow, in addition to examples of use for workflow elements. See <a rev="chapter_4103690129" href="./chapter_4103690129.html">SuiteFlow Reference and Examples</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>Testing and troubleshooting workflows</p>
					</td>
            <td class="bolight_bgwhite">
						<p>During workflow development, you need to test your workflows. NetSuite tracks workflow instance activity and generates execution logs to assist you with workflow testing and troubleshooting. See <a rev="chapter_N2786070" href="./chapter_N2786070.html">Testing and Troubleshooting Workflows</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>Example workflows</p>
					</td>
            <td class="bolight_bglight">
						<p>Use the examples to see how to create certain types of workflows. Each example includes the detailed steps required to create and then test the workflow. See <a rev="chapter_N2796642" href="./chapter_N2796642.html">Workflow Samples</a>.</p>
					</td>
          </tr>
        </tbody>
      </table>
      <h2 id="bridgehead_4828758136">Accessing SuiteFlow</h2>
      <p>Access the SuiteFlow UI from Customization &gt; Workflows &gt; Workflows. The SuiteFlow feature must be enabled in the NetSuite account and a user must have the Setup type Worflow permission to access the SuiteFlow UI. For details about enabling SuiteFlow, see <a rev="section_1489429767" href="./section_1489429767.html">Enabling SuiteFlow</a>. For details about SuiteFlow permissions, see <a rev="section_1489429737" href="./section_1489429737.html">Required Permissions for SuiteFlow</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4068295521" href="./section_4068295521.html">Workflow Manager Interface</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4071953058" href="./section_4071953058.html">Workflow Elements</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4080797941" href="./section_4080797941.html">Workflow Initiation</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4073598374" href="./section_4073598374.html">Workflow Audience</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4101515562" href="./chapter_4101515562.html">Working with Workflows</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4103690129" href="./chapter_4103690129.html">SuiteFlow Reference and Examples</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

