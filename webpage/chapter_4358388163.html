<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N973142" href="./set_N973142.html">Marketing, Sales Force Automation, and Partners</a>
            <a rev="book_N973201" href="./book_N973201.html">Marketing</a>
            <a class="nshelp_navlast" rev="chapter_4358388163" href="./chapter_4358388163.html">Creating DMARC-compliant messaging in NetSuite</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="chapter_4358388163" class="nshelp_title">Creating DMARC-compliant messaging in NetSuite</h1>
      <p>DMARC (Domain-based Message Authentication, Reporting and Conformance) is an anti-spoofing technology that makes it possible for domain owners to use the Domain Name System (DNS) to inform receiving servers of their DMARC policy. This policy specifies how the domain owner wants the receiving mail server to handle messages claiming to be sent from their domain, but cannot be authenticated as having actually originated from it. DMARC has become a widely-recognized standard and is being implemented by major ISPs and mail service providers. This is a positive move and will go a long way to enhancing the email reputation of commercial organizations. In this article, we'll show you how DMARC works, and then how to configure a DMARC policy for incoming and outgoing mail messages in NetSuite.</p>
      <h2 id="bridgehead_4358388231">How Does DMARC work?</h2>
      <p>DMARC is a “policy layer” that sits on top of two email authentication technologies known as SPF and DKIM. SPF is used to authenticate the origin of an email. That is, it asks the question “Does this mail come from where it says it does?” DKIM looks at authenticating the actual message content. That is “Is this the same message as the one which the sender sent—has it been tampered with?”</p>
      <p>DMARC looks at authentication from an end-user perspective and tries to answer the most commonly posed question, whether the “FROM” name that users see in their inbox is actually authentic and originates from the domain it claims to be. DMARC uses the FROM address as the basis for performing what’s known as an “alignment check” against SPF and DKIM.</p>
      <p>DMARC works by testing and enforcing an “alignment check” on the incoming mail’s SPF and DKIM headers against the From domain in the mail header (known as RFC5322.From). DMARC requires that only one authenticated identifier (either SPF or DKIM) needs to match the From domain to be considered in alignment.</p>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/MarketingSalesForceAutomationPartners/Marketing/marketing_dmarc_email_auth_flow.png" width="580px" height="316px">
      <p>For more information on DMARC visit <a href="http://dmarc.org/" target="_blank">DMARC</a>, or view the DMARC training <a href="https://www.youtube.com/watch?v=DvSappL5aag" target="_blank">videos</a>.</p>
      <h2 id="bridgehead_4358388374">How is email handled in NetSuite?</h2>
      <p>If you have enabled the Capture Email Replies feature, a special NetSuite-generated “reply to” address is added to your email message. This address is used by NetSuite to log the communication when a customer replies to you. First the message is routed to NetSuite, where it is recorded in the system, and then it is forwarded to your regular email address (the one specified in your User Preferences). This process is done seamlessly by NetSuite.</p>
      <p>With DMARC alignment, forwarded email in NetSuite may cause an alignment check failure. This is because if NetSuite’s SMTP IP addresses are not recorded in the originating domain owner’s SPF record, then the SPF alignment check in DMARC will fail. Likewise, the DKIM alignment check will also fail in situations where NetSuite does not have access to the domain owner’s private key. If either of these two checks fail, it will not pass DMARC since at least one authentication method needs to be aligned in order for it to pass the mail. Recently, inbound messages to NetSuite originating from Yahoo and some of the larger ISPs and mail service providers have been failing DMARC alignment. Since Yahoo does not include NetSuite on its SPF record nor is it possible to have their private key for DKIM authentication, forwarded mail are not able to pass DMARC.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">This is only the case for inbound forwarded mail. Outbound mail in NetSuite is unaffected, assuming that the account owner has full control of the SPF records and DKIM signing, and it has been correctly configured. Similarly outbound mail is unaffected if the account owner does not have SPF and DKIM enabled on his email domain.</p>
      </div>
      <br>
      <h2 id="bridgehead_4358388466">Yahoo and DMARC</h2>
      <p>Yahoo has implemented DMARC on both outbound and inbound messaging as part of a determined effort to combat phishing attacks originating from spoofed addresses on their domain. Part of this strategy is to implement what’s known as a DMARC “reject policy”. Domain owners can choose three options in DMARC to inform mail receivers what to do with misaligned mail originating from their domain: <code>none</code> (report only), <code>quarantine</code>, and <code>reject</code>. Email providers that choose to adopt a hard line reject policy have reduced the number of phishing incidents, but legitimately spoofed email (that is, forwarded mail from a third party such as NetSuite) is also being blocked by these email providers.</p>
      <h2 id="bridgehead_4358388568">Implementing DMARC-compliant messages in NetSuite</h2>
      <p>In NetSuite Version 2015 Release 2, we released two system preferences that enable users to create DMARC-compliant messages when sending outgoing mail from NetSuite and for forwarding email replies using the Capture Email Replies feature. These preferences not only enable a DMARC-compliant mail stream from NetSuite, but also delivers an effective resolution to the reject policy issue outlined above.</p>
      <p>Domain owners should already have Sender Policy Framework (SPF) and Domain Keys Identified Mail (DKIM) records set up and have full access to their domain DNS before proceeding with this setup. SPF and DKIM are DNS records of the TXT type. Your domain administrator or someone who has access to your domain registrar can create and make changes to these records.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">You must have the SPF record set up correctly and published with your domain provider. Verify the SPF record contains “include: mailsenders.netsuite.com”.</p>
      </div>
      <br>
      <p>For more information on SPF, see <a href="http://www.openspf.org/" target="_blank">openspf.org</a>. For more information on the DKIM record, see <a rev="section_N999544" href="./section_N999544.html">DomainKeys Identified Mail (DKIM)</a>.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Selecting the DMARC compliance preferences will disable the Hard Bounce tracking feature; the envelope sender is aligned with the from sender and therefore cannot use the @bounces.netsuite.com domain.</p>
      </div>
      <br>
      <div class="nshelp_procedure">
        <h4>To setup DMARC compliance on outgoing mail in NetSuite:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to Setup &gt; Company &gt; Email Preferences.</p>
          </li>
          <li>
            <p>Check the <strong>Compose DMARC Compliant Email Messages</strong> box in the <strong>Domain Keys</strong> column.</p>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <div class="nshelp_procedure">
        <h4>To setup DMARC compliance on forwarded mail in NetSuite:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to <span id="dyn_help_taskpath" dynid="ADMI_FEATURES">&nbsp;</span>.</p>
          </li>
          <li>
            <p>Check the <strong>Capture Email Replies</strong> box in the <strong>Marketing</strong> column.</p>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
          <li>
            <p>Go to Setup &gt; Company &gt; Email Preferences.</p>
          </li>
          <li>
            <p>Check the <strong>Forward email replies in DMARC compliant format</strong> box in the <strong>Domain Keys</strong> column.</p>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <p>For more information about DMARC, go to <a href="www.dmarc.org" target="_blank">dmarc.org</a>. See also <a href="/app/crm/support/ptaredirector.nl?dl=https://netsuite.custhelp.com/app/answers/detail/a_id/14740/" target="_blank">DKIM Reference Guide</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N999544" href="./section_N999544.html">DomainKeys Identified Mail (DKIM)</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

