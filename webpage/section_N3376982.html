<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3363377" href="./book_N3363377.html">SuiteBundler</a>
            <a rev="chapter_N3364150" href="./chapter_N3364150.html">SuiteApp Creation and Distribution</a>
            <a rev="section_4422668647" href="./section_4422668647.html">Bundle Builder Reference</a>
            <a class="nshelp_navlast" rev="section_N3376982" href="./section_N3376982.html">Locking Objects in Customization Bundles</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N3376982" class="nshelp_title">Locking Objects in Customization Bundles</h1>
      <p><span id="dyn_help_feature" dynid="CREATESUITEBUNDLES">&nbsp;</span>   </p>
      <p>A bundle author can lock some of the objects in a customization bundle on the <a rev="section_N3373802" href="./section_N3373802.html">Step 4 Set Preferences</a> page of the Bundle Builder. When an administrator installs a bundle containing locked objects, these locked objects cannot be edited. Sometimes editing objects in a bundle can break a solution or make future upgrades impossible without data loss. Locking prevents this and allows customization in a controlled and safe manner.</p>
      <p>Object locking provides bundle authors and independent software vendors (ISVs) the ability to control which aspects of their solution can be extended and customized to avoid upgrade issues. In addition, locking can ensure compatibility when bundle updates are rolled out.</p>
      <p>Before a NetSuite account administrator installs or updates a bundle, the administrator can view which components in the bundle are locked, on the Preview Bundle Install Page. When an installed bundle includes locked objects, these objects' NetSuite pages do not have buttons that allow changes to the object, such as Edit, Delete, and Change ID. Also, all list and view pages display a lock icon to provide complete visibility of locked objects in installed bundles.</p>
      <p>You can lock bundle objects as part of bundle creation or editing. To lock a bundle object, check the Lock on Install box on the <a rev="section_N3373802" href="./section_N3373802.html">Step 4 Set Preferences</a> page of the Bundle Builder.</p>
      <div class="nshelp_warn">
        <h3>Warning</h3>
        <p class="nshelp_first">It is strongly recommended that you do not lock bundle objects to be included in a bundle developed in sandbox. Locking objects in the source sandbox account for the bundle can eventually lead to the objects being locked in that account. After the bundle is installed in production and the sandbox account is later refreshed, the production account settings for locked objects are copied to the sandbox account. This copy causes the objects to be locked in the source sandbox account, with their lock options disabled, preventing bundle developers from editing these objects.</p>
      </div>
      <br>
      <p>The installation of a bundle that contains locked objects requires the same steps as installing a bundle with no locked objects. For a detailed description of the procedure used to install a bundle, see <a rev="section_N3395142" href="./section_N3395142.html">Installing a Bundle</a>. Also, see <a rev="section_N3396121" href="./section_N3396121.html#bridgehead_N3396378">Review Locked Bundle Objects</a>.</p>
      <p>The <a rev="section_N3364871" href="./section_N3364871.html#bridgehead_N3364890">Objects Available in Customization Bundles</a> table includes a column indicating which object types are lockable.</p>
      <p>Effects of locking vary according to object type. See <a rev="section_N3376982" href="#bridgehead_N3377144">Effects of Locking Different Object Types</a>.</p>
      <h3 id="bridgehead_N3377087">Precautions about Locking Bundled Objects</h3>
      <p>Locking an object does not prevent target account users from seeing it, only from editing it. For details about how to hide bundle objects in target accounts, see <a rev="section_N3380618" href="./section_N3380618.html">Hiding Bundle Components in Target Accounts</a>. For details about how to prevent target account users from seeing code in bundled server SuiteScripts, see <a rev="section_N3377764" href="./section_N3377764.html">Protecting Your Bundled Server SuiteScripts</a>.</p>
      <div class="nshelp_warn">
        <h3>Warning</h3>
        <p class="nshelp_first">Bundle locking functionality is not a tool for Intellectual Property management and should not be treated as such. The intention of this functionality is to discourage end users from making changes to locked bundle objects. Locking an object does not prevent a user from installing another third party bundle with an object of the same name that is unlocked and that can be used to replace the locked object with an unlocked one.</p>
      </div>
      <br>
      <p>When you copy (rather than install) a bundle that includes locked objects, the objects are not locked in the account where the bundle is copied. However, their locked settings are maintained on the <a rev="section_N3373802" href="./section_N3373802.html">Step 4 Set Preferences</a> page of the copied bundle, and are enforced in target accounts where the copied bundle is installed. See <a rev="section_N3391248" href="./section_N3391248.html">Copying a Bundle to Other Accounts</a>.</p>
      <h3 id="bridgehead_N3377144">Effects of Locking Different Object Types</h3>
      <p>The following list summarizes the effects of locking bundle objects for target accounts:</p>
      <ul class="nshelp">
        <li>
          <p><strong>Custom Fields:</strong> Target account administrators can control access to locked custom fields through form customization as before.</p>
        </li>
        <li>
          <p>
            <strong>Custom Lists: Records cannot be added, edited, or deleted in locked custom lists in target accounts.</strong>
          </p>
        </li>
        <li>
          <p>
            <strong>Custom Records:</strong>
          </p>
          <ul class="nshelp">
            <li>
              <p>Data can be modified in locked custom records in target accounts. In other words, records can be added, edited, and deleted for these custom records.</p>
            </li>
            <li>
              <p>Edits to metadata are restricted for locked custom records in target accounts.</p>
              <ul class="nshelp">
                <li>
                  <p>For locked custom records that included data when installed in target accounts, no edits to metadata are allowed. Custom record fields cannot be added, edited, or deleted.</p>
                </li>
                <li>
                  <p>For locked custom records that did not include data when installed in target accounts, some edits to metadata are allowed. Fields can be added to these locked custom records, and these fields can later be edited and deleted. However, custom record fields from the source account cannot be edited or deleted.</p>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <p>
            <strong>File Cabinet</strong>
          </p>
          <ul class="nshelp">
            <li>
              <p><strong>Files:</strong> In target accounts, the file cabinet displays lock icons for locked files from installed bundles, as these files cannot be edited.</p>
              <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBundler/FileCabLockedFile.png" width="586px" height="96px">
              <ul class="nshelp">
                <li>
                  <p>Locked files cannot be overwritten in target accounts through Add File, Advanced Add, Copy Files, or Move Files actions in the file cabinet.</p>
                  <p>For example, if a user attempts to add a file with the same name as the locked file in the same location in the target account's file cabinet, the action is not completed and the user receives a warning that a file with the same name already exists.</p>
                </li>
                <li>
                  <p>The file cabinet Move Files and Delete Files actions are not available for these locked files. If you choose Move Files or Delete Files for a file cabinet folder that includes any locked files, the Folder Contents page disables the check box for these files and displays a lock icon for each one.</p>
                  <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBundler/FileCabFolderContents.png" width="404px" height="255px">
                </li>
              </ul>
            </li>
            <li>
              <p><strong>Folders:</strong> In target accounts, the file cabinet displays lock icons for files contained in locked folders from installed bundles. Files in locked bundle folders are treated in the same manner as locked bundle files, as described above.</p>
            </li>
          </ul>
        </li>
        <li>
          <p><strong>Custom Forms:</strong> Target account administrators who install a bundle containing locked objects can create a new instance of the locked form and customize it to fit their needs. This behavior is similar to how standard NetSuite forms behave.</p>
        </li>
        <li>
          <p><strong>Custom Roles:</strong> Target account administrators who install a bundle containing locked objects, can create a copy of the locked role and further customize it to fit their needs. This behavior is similar to how standard NetSuite roles are handled.</p>
        </li>
        <li>
          <p><strong>Saved Searches:</strong> Users who have access to the saved search can create a new copy of the saved search and add or remove filters and results columns.</p>
        </li>
        <li>
          <p><strong>SuiteScripts:</strong> Locking script records does not lock the script deployments. Customers can change properties of the deployment to fit their business needs. Script parameters lock automatically. However, users can define values for them, if the parameter is defined as company or user preference.</p>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3364150" href="./chapter_N3364150.html">SuiteApp Creation and Distribution</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3374254" href="./section_N3374254.html">Creating a Bundle with the Bundle Builder</a>
                </span>
              </dt>
              <dt>
                <p>
                  <em>Steps for Creating a Bundle</em>
                </p>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3373802" href="./section_N3373802.html">Step 4 Set Preferences</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4444212213" href="./chapter_4444212213.html">SuiteApp Development Process with SuiteBundler</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3394134" href="./chapter_N3394134.html">SuiteApp Installation and Update</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

