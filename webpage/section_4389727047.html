<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3412393" href="./book_N3412393.html">SuiteTalk (Web Services)</a>
            <a rev="part_N3412452" href="./part_N3412452.html">SuiteTalk (Web Services) Platform Guide</a>
            <a rev="chapter_4399047360" href="./chapter_4399047360.html">Managing Integrations</a>
            <a class="nshelp_navlast" rev="section_4389727047" href="./section_4389727047.html">Integration Record Overview</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_4389727047" class="nshelp_title">Integration Record Overview</h1>
      <p>The integration record enhances your ability to manage and monitor web services requests sent to your NetSuite account. This capability is particularly useful if you have more than one application that sends requests. For example, your sales department may use one application to send data about new customers. Your HR department may use a different application to update employee records. Each application can be represented by a separate integration record.</p>
      <p>For more details, see the following sections:</p>
      <ul class="nshelp">
        <li>
          <p>
            <a rev="section_4389727047" href="#bridgehead_4430959588">Benefits of the Integration Record</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_4389727047" href="#bridgehead_4430966124">Application IDs</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_4389727047" href="#bridgehead_4430961842">Endpoint Requirements</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_4389727047" href="#bridgehead_4430967734">Ownership of Integration Records</a>
          </p>
        </li>
      </ul>
      <h2 id="bridgehead_4430959588">Benefits of the Integration Record</h2>
      <p>When you use integration records to represent all of your external applications, you can do any of the following:</p>
      <ul class="nshelp">
        <li>
          <p><strong>View details about each application</strong> – Each integration record shows details such as the application’s name and description.</p>
        </li>
        <li>
          <p><strong>Block an application</strong> – You can use an integration record to block web services requests that come from the corresponding application. However, if you are using the 2015.1 or an earlier endpoint, be aware that some requests may be handled by the Default Web Services Integrations record, which has its own configuration options. For details, see <a rev="section_4431175537" href="./section_4431175537.html">Blocking Web Services Requests</a>.</p>
        </li>
        <li>
          <p><strong>Select permitted authentication methods for an application</strong> – When you create an integration record, you can permit the application to authenticate through user credentials, token-based authentication, or both. Note that when a request authenticates through user credentials, the request must also include an application ID, if you want the application’s requests to be tracked by the appropriate integration record. (For details, see <a rev="section_4389727047" href="#bridgehead_4430966124">Application IDs</a>.) Additionally, be aware that the permitted authentication methods vary depending on which endpoint the request uses. For details, see <a rev="section_4389727047" href="#bridgehead_4430961842">Endpoint Requirements</a>.</p>
        </li>
        <li>
          <p><strong>Distribute an integration record</strong> – You can bundle an integration record and make it available for installation in other NetSuite accounts. You can also configure a record so that it can be automatically installed in other accounts. For details, see <a rev="section_4389835426" href="./section_4389835426.html">Distributing Integration Records</a>.</p>
        </li>
        <li>
          <p><strong>View an execution log specific to each application</strong> – When you create and use an integration record for each application, that record’s Web Services Execution Log lets you view only those requests sent by that application. If you are not using unique integration records for each application, you can view all requests together by using the log at Setup &gt; Integration &gt; Web Services Usage Log.</p>
        </li>
        <li>
          <p><strong>View a list of all your applications</strong> – You can view a list of all your applications at Setup &gt; Integration &gt; Manage Integrations.</p>
        </li>
      </ul>
      <h2 id="bridgehead_4430966124">Application IDs</h2>
      <p>Every integration record has a unique application ID. This ID, a 32-character universally unique identifier, is generated when you create the record. The ID is displayed on the integration record in the Application ID field. However, application ID values are displayed only in the NetSuite account where they were created. So, if an integration record was created by a partner and distributed, that record’s application ID is not visible to the partner’s customers when they view the installed record in their own accounts.</p>
      <p>In some cases, your web services requests are required to <strong>include</strong> an application ID. However, if your request uses token-based authentication, you must <strong>omit</strong> an application ID.</p>
      <p>Use the following guidelines:</p>
      <ul class="nshelp">
        <li>
          <p>Requests that authenticate through user credentials are required to include an application ID, if the request uses the 2015.2 endpoint or later. (For details, see <a rev="section_4389727047" href="#bridgehead_4430961842">Endpoint Requirements</a>.) If the request uses the 2015.1 endpoint or earlier, an application ID is optional.</p>
        </li>
        <li>
          <p>Inbound SSO requests must include an application ID, if the requests use the 2015.2 endpoint or later. If the request uses the 2015.1 endpoint or earlier, an application ID is optional.</p>
        </li>
        <li>
          <p>Application IDs should not be included in outbound SSO calls, regardless of which endpoint is being used.</p>
        </li>
        <li>
          <p>If a request uses token-based authentication, you must not include an application ID, regardless of which endpoint you are using. If you include an application ID with a request that uses TBA, the request fails.</p>
        </li>
      </ul>
      <p>Although application IDs are considered identifiers and not authentication credentials, application IDs should nevertheless be considered confidential.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">Some customers may have application IDs that were provided to them by NetSuite Support prior to Version 2015 Release 2. These older IDs must not be used with the 2015.2 or a later WSDL. Older application IDs are permitted <strong>only</strong> in requests that use the 2015.1 WSDL or earlier. These requests are logged as part of the Default Web Services Integrations record’s Web Services Execution Log.</p>
        <p>If older application IDs are included in requests that use the 2015.2 or a later WSDL, the requests are denied.</p>
        <p>You can identify an old application ID by its length. Old application IDs have a maximum of nine digits. By contrast, new applications IDs are 32-character UUIDs.</p>
      </div>
      <br>
      <h2 id="bridgehead_4430961842">Endpoint Requirements</h2>
      <p>You can use integration records in conjunction with any supported endpoint. However, different requirements exist, depending on your endpoint.</p>
      <h3 id="bridgehead_4502755770">2015.1 and Earlier</h3>
      <p>If you use the 2015.1 endpoint or earlier, be aware of the following:</p>
      <ul class="nshelp">
        <li>
          <p>You have the option of letting some or all of your requests be tracked by a single integration record called Default Web Services Integrations. This record requires little maintenance, if any. The Default record was automatically created when your account was upgraded to Version 2015 Release 2 or subsequently, when you enabled the Web Services feature. The behavior of this record is slightly different from the behavior of integration records that you create. For details on this record, see <a rev="section_4431183712" href="./section_4431183712.html">Working with the Default Web Services Integrations Record</a>.</p>
        </li>
        <li>
          <p>Token-based authentication is not supported. If you want a request to be tracked by a specific integration record (other than the Default record), it must authenticate through user credentials and include an application ID. For more details about application IDs, see <a rev="section_4389727047" href="#bridgehead_4430966124">Application IDs</a>.</p>
        </li>
      </ul>
      <h3 id="bridgehead_4502755649">2015.2 and Later</h3>
      <p>If you upgrade to the 2015.2 or a later endpoint, all web services requests must be associated with an integration record other than the default record. That is, each request must include, either explicitly or implicitly, one of the following types of information:</p>
      <ul class="nshelp">
        <li>
          <p>The application ID that was generated when the integration record was created, in conjunction with valid user credentials. For more details about application IDs, see <a rev="section_4389727047" href="#bridgehead_4430966124">Application IDs</a>. If you use the login or ssoLogin operation, the application ID can be sent one time per session. For details, see <a rev="section_4502721888" href="./section_4502721888.html">Using Application ID with the Login and ssoLogin Operations</a>.</p>
        </li>
        <li>
          <p>Token-based authentication details. For details on updating your code to authenticate this way, see <a rev="section_4395622897" href="./section_4395622897.html">Sending Token-Based Authentication Details</a>.</p>
        </li>
      </ul>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">Only one web services operation does not require authentication, nor any information identifying the application. It is the <a rev="section_N3494684" href="./section_N3494684.html">getDataCenterUrls</a>.</p>
      </div>
      <br>
      <h2 id="bridgehead_4769062482">2016.2 and Later</h2>
      <p>NetSuite 2016.2 includes two new permissions that make it possible for users other than administrators to view and manage integration records.</p>
      <p>The List type Integration Applications permission provides view access to the Integrations list page.</p>
      <p>The Full level of the Setup type Integration Application permission provides the ability to view, edit, and create integration records. This permission also provides access to the <strong>Integration</strong> field on the Web Services Operations search.</p>
      <p>For details about web services searches, <a rev="section_4738049812" href="./section_4738049812.html">Searching for Web Services Log Information</a>.</p>
      <h2 id="bridgehead_4430967734">Ownership of Integration Records</h2>
      <p>When you create an integration record, it is automatically available to you in your NetSuite account. Your NetSuite account is considered to be the owner of the integration record, and the record is fully editible by administrators in your account.</p>
      <p>You can also install records in your account that were created elsewhere, as follows:</p>
      <ul class="nshelp">
        <li>
          <p>Installed records can be installed as part of a bundle.</p>
        </li>
        <li>
          <p>In some cases, integration records can also be auto-installed. With this approach, a record is automatically installed when a request is sent that references the record’s application ID. However, be aware that auto-installation is not recommended for records that are configured to permit token-based authentication. NetSuite recommends that these records be distributed through bundling.</p>
        </li>
      </ul>
      <p>Records that are installed through either of these methods are not fully editable, because they are considered to be owned by a different NetSuite account. On such records, you can make changes to only two fields: the Note field and the State field. All other fields on these records, including the permitted authentication options and the Description field, can be changed only by an authorized user in the account that owns the records. When the owner makes changes, they are pushed automatically to your account. These changes are not reflected in the system notes that appear in your account.</p>
      <p>For more information about creating integration records, see <a rev="section_4393879073" href="./section_4393879073.html">Creating an Integration Record</a>. For more details about distributing integration records, see <a rev="section_4389835426" href="./section_4389835426.html">Distributing Integration Records</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_4399047360" href="./chapter_4399047360.html">Managing Integrations</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4416360542" href="./section_4416360542.html">Using Integration Records in Sandbox Accounts</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4416335959" href="./section_4416335959.html">Using Integration Records in Conjunction With SSO Calls</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4429293934" href="./section_4429293934.html#bridgehead_4431831502">Configuring the Require-Approval Preference</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_4429221557" href="./section_4429221557.html">Removing Integration Records</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3412777" href="./chapter_N3412777.html">SuiteTalk Platform Overview</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3421363" href="./section_N3421363.html">SuiteTalk Development Considerations</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N3477815" href="./chapter_N3477815.html">Web Services Operations</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

