<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2249433" href="./book_N2249433.html">Inventory Management</a>
            <a rev="chapter_N2250682" href="./chapter_N2250682.html">Inventory Management</a>
            <a rev="section_N2258460" href="./section_N2258460.html">Basic Inventory Management</a>
            <a class="nshelp_navlast" rev="section_N2264208" href="./section_N2264208.html">Understanding and Avoiding Underwater Inventory</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2264208" class="nshelp_title">Understanding and Avoiding Underwater Inventory</h1>
      <p><span id="dyn_help_feature" dynid="INVENTORY">&nbsp;</span>   </p>
      <p>Selling an item when the data shows that you do not have the item in stock is known as an “underwater” sale. Inventory is in an underwater state when the on-hand quantity of the item is below zero. You should avoid entering an item on a sale transaction if the on hand count of the item is zero or a negative amount.</p>
      <p>Problems can arise if you enter sale transactions over a period when an item is underwater because NetSuite has difficulty calculating the cost of the item on those sale transactions. These cost calculations from underwater sales can lead to skewed results for reports and inventory data.</p>
      <p>During the time that the on hand count is below zero, on sales transactions that include the item, costing information at the time of the sale is shifted to another date. The estimated cost is calculated based on the last known cost when the item was in stock.</p>
      <p>At a later date when more of the item is received into stock, the true cost can be calculated and an adjustment can be posted to correct for transaction lines entered when the item was underwater. However, because the adjustment posts with the item receipt, the correction does not post with the original estimated cost.</p>
      <p>For example, consider the following scenario where inventory is sold underwater:</p>
      <ul class="nshelp">
        <li>
          <p>On March 30, an invoice is entered that sells 100 widgets and brings the on hand count of widgets to -100 widgets.</p>
        </li>
        <li>
          <p>Based on historical costing, the cost of the widgets is estimated at $10 each. Therefore, the cost of goods sold for the 100 widgets = ($10 x 100) = $1000.</p>
        </li>
        <li>
          <p>On April 2, a purchase is entered for 100 widgets at a cost of $12 each. The actual cost of the widgets is ($12 x 100) = $1200. An adjustment of $200 is entered to correct the purchase price of the widgets.</p>
        </li>
      </ul>
      <p>After you have entered a purchase that brings an underwater item count back above zero, the costing adjustments that are entered do balance totals across affected periods. However, when viewing reports by period, if a corresponding sale and purchase post in different periods, you may still see the results of costs shifted from one period to another.</p>
      <p>If you consider the balance for March and April <strong>together,</strong> then the total is balanced because the <em>sale</em> in March and the <em>purchase</em> in April that includes the adjustment are <strong>both</strong> considered.</p>
      <p>However, if you consider <strong>only sales</strong> in the month of March, the total is off by $200 because the adjustment that posted with the item receipt does not show in the totals. Likewise, if you consider <strong>only purchases</strong> for the month of April, the total is off by $200 because the adjustment is included in the total without the sale.</p>
      <p>If you do not enter purchases to bring the item count back above zero, the costing estimates remain in the system. For example, a costing estimate can be $0.00 if there is <em>no</em> purchase history and can cause larger adjustments after you enter a purchase to bring the item count back above zero.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">The longer the period between the underwater sale of the item and the purchase that brings the count back above zero, the more skewed your data and reports can be. For example, if an item is underwater only three days before you enter a purchase that brings it back above zero, reports and data will be less skewed than if an item is underwater for three months.</p>
      </div>
      <br>
      <h3 id="procedure_N2264319">Why should I care about selling underwater inventory?</h3>
      <ul class="nshelp">
        <li>
          <p><strong>Data is less accurate</strong> – When you have underwater items in your inventory, your inventory data is less accurate. Inaccurate inventory data can affect other aspects of your integrated NetSuite account and can potentially skew data in your chart of accounts, reports, and other areas.</p>
        </li>
        <li>
          <p><strong>Transactions are more confusing</strong> – Purchases that include costing estimates are often confusing for users who are not familiar with the costing workflow for underwater sales. Trying to sort out transactions associated with underwater sales can be time consuming and counterproductive.</p>
        </li>
        <li>
          <p><strong>Records are less fit for audits</strong> – Because transactions that include underwater items also include estimates and adjustments, they are less likely to be well received by auditors.</p>
        </li>
      </ul>
      <h3 id="procedure_N2264363">How can I tell if I am selling underwater inventory?</h3>
      <ul class="nshelp">
        <li>
          <p>The sale transaction shows a negative on-hand count of the item.</p>
        </li>
        <li>
          <p>You receive a warning that the item is not in stock.</p>
        </li>
      </ul>
      <p>It is best to focus concern on the point in your workflow at which items leave your inventory. For example, your workflow can include entering sales orders and then fulfilling them. Because items do not leave stock when you enter the sales order, your focus is not on that transaction. However, when you enter the fulfillment, items do leave stock and you need to be cautious that the fulfillment does not include underwater items.</p>
      <h3 id="procedure_N2264412">How can I avoid selling underwater inventory?</h3>
      <p>You can avoid being in an underwater state with inventory items by following these tips:</p>
      <ul class="nshelp">
        <li>
          <p>Insist on prompt entry of item receipts.</p>
        </li>
        <li>
          <p>Require item receipts to be entered with the date of receipt instead of the entry date if the receipt is not entered the same day it is received.</p>
        </li>
        <li>
          <p>Always use sales orders to sell inventory.</p>
        </li>
        <li>
          <p>Always fulfill orders from sales orders.</p>
        </li>
        <li>
          <p>Set the <strong>Fulfill Based on Commitment</strong> preference to <em>Limit to Committed</em>.</p>
          <p>For more information on this preference, see the <a rev="section_N1388149" href="./section_N1388149.html">Order Management Accounting Preferences</a> . .</p>
        </li>
      </ul>
      <ul class="nshelp">
        <li>
          <p>Avoid entering standalone cash sales and invoices. Standalone transactions have no commitments or checks and balances to prevent you from selling out or going underwater.</p>
        </li>
        <li>
          <p>Use the Inventory Level Warnings preference. This preference gives you instant feedback on each transaction as to whether or not you have items in stock.</p>
          <p>For more information on this preference, read <a rev="section_N2254698" href="./section_N2254698.html">Inventory Level Warnings</a>.</p>
        </li>
      </ul>
      <ul class="nshelp">
        <li>
          <p>Perform a physical inventory count on a regular basis to assess whether your inventory data is consistent with the physical count of your stock.</p>
          <p>If the physical count is higher or lower than the quantity on hand based on the item record, the count needs to be reconciled to match actual inventory levels.</p>
        </li>
        <li>
          <p>Use the Review Negative Inventory page to identify inventory items that are underwater. For more details, read <a rev="section_N2268458" href="./section_N2268458.html">Reviewing Negative Inventory</a>.</p>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics:</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2258460" href="./section_N2258460.html">Basic Inventory Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2258575" href="./section_N2258575.html">Inventory Management Forms</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2259648" href="./section_N2259648.html">Adjusting Inventory</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2263962" href="./section_N2263962.html">Handling Backorders</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2270284" href="./section_N2270284.html">Bin Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2285050" href="./chapter_N2285050.html">Advanced Inventory Management</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2303574" href="./section_N2303574.html">Multi-Location Inventory</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2353200" href="./chapter_N2353200.html">Inventory Reporting</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

