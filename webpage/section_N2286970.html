<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2249433" href="./book_N2249433.html">Inventory Management</a>
            <a rev="chapter_N2285050" href="./chapter_N2285050.html">Advanced Inventory Management</a>
            <a class="nshelp_navlast" rev="section_N2286970" href="./section_N2286970.html">Demand Planning</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2286970" class="nshelp_title">Demand Planning</h1>
      <p></p>
      <p><span id="dyn_help_feature" dynid="ITEMDEMANDPLANNING">&nbsp;</span>   </p>
      <p>You can use the Demand Planning feature to analyze your stock demand needs, determine replenishment requirements, and then create orders according to a supply plan that will add stock as needed. This information can be crucial for items with demand that varies and fluctuates throughout the year.</p>
      <p>You can pinpoint when to reorder items, and in what quantities, allowing you to maintain optimal stock levels. Demand planning helps you to have the right amount of stock on hand to fill orders without having overstock sitting idle on warehouse shelves.</p>
      <p>Demand Planning uses demand plan and supply plan records to track anticipated supply and demand.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">When you use the Demand Planning feature, you can also use the Available to Promise feature to calculate availability. Read <a rev="section_N2300269" href="./section_N2300269.html">Available to Promise</a>.</p>
      </div>
      <br>
      <h2 id="bridgehead_N2287026">Demand Plans</h2>
      <p>A demand plan records the expected future demand for an item based on previous or projected demand.</p>
      <p>A demand plan can be created automatically using the Calculate Demand Plan page. This page initiates the process to assess previous demand for items and calculate the estimated upcoming demand.</p>
      <p>You can forecast demand for items using one of the following methods:</p>
      <ul class="nshelp">
        <li>
          <p>Determine a time frame for examining an item's historical sales data (or data of a different item) to analyze previous sales trends and forecast future sales with similar trends.</p>
        </li>
        <li>
          <p>Use current demand such as opportunities, quotes and existing sales orders to forecast future sales. This method is not based on a calculated forecast.</p>
          <p>NetSuite uses this forecast data to project estimated demand across a designated time period in the future and suggests a plan for orders accordingly.</p>
        </li>
      </ul>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/InventoryManagement/ItemDemandPlan.png" width="600px" height="378px">
      <h2 id="bridgehead_N2287086">Supply Plans</h2>
      <p>Supply plans record the recommended schedule for purchasing or manufacturing additional supply of an item. The supply plan provides a list of recommended purchase orders and work orders to augment item supply in a timely manner based on lead times and expected demand.</p>
      <p>Safety stock level settings are considered in supply calculations. Also, the supply plan incorporates lead times so you place orders in time to receive the items when the demand is increased. Purchase orders generated from supply plans use the preferred vendor from the item record.</p>
      <p>For assembly item supply plans, all levels of a multi-tier assembly are considered and work orders are planned for all sub-components of the build, as well as purchasing of required raw materials.</p>
      <p>A supply plan can be generated from a demand plan using the Calculate Supply Plan page.</p>
      <img class="nshelp_screenshot" src="/help/helpcenter/en_US/Output/Help/images/InventoryManagement/ItemSupplyPlan.png" width="600px" height="175px">
      <p>Using demand plans and supply plans helps you maintain an optimal level of inventory for items that have fluctuating demand.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">For example, you sell a Deluxe Seasonal Widget that has a 15-day lead time and demand for the widget varies from month to month. To be sure you order and stock the right amounts of the widget as you need them, you can use demand planning to do the following:</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <p>Create a demand plan to analyze your historical sales data for the widget to project demand for the widget into the future. The demand plan will show demand across future periods, including high demand in April and August and low demand in October and May.</p>
              </dt>
              <dt>
                <p>From the demand plan, create a supply plan. The supply plan shows when to create purchase orders to replenish the widget based on the expected demand data.</p>
              </dt>
            </dl>
          </div>
        </div>
      </div>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">You can process a maximum of 10,000 items at one time for Demand Planning functions.</p>
      </div>
      <br>
      <p>The general workflow for demand planning is as follows:</p>
      <ol class="nshelp">
        <li>
          <p>Set up the feature.</p>
          <p>This includes enabling the feature, setting preferences, and setting up item records. Only items with the Time Phased method selected in the Replenishment Method field on their record can be used on demand plans and supply plans.</p>
          <p>For more details, read <a rev="section_N2288536" href="./section_N2288536.html">Setting Up Demand Planning</a>.</p>
          <p>After you enable the feature, the Demand Planning links show on the Transactions tab.</p>
        </li>
        <li>
          <p>Calculate demand for items.</p>
          <p>To calculate demand, at a minimum, you must identify a projection method, period type, historical period, and projection period. If you use Multiple-Location Inventory, you must also identify a location.</p>
          <p>Projection methods you can use include Linear Regression, Moving Average, Seasonal Average, and Sales Forecast.</p>
          <p>Read <a rev="section_N2290234" href="./section_N2290234.html">Calculating Item Demand</a> or <a rev="section_N2292418" href="./section_N2292418.html">Manual Item Demand Plans</a>.</p>
        </li>
        <li>
          <p>Review the demand plan.</p>
          <p>Review the projected demand as calculated and make any necessary changes.</p>
          <p>Read <a rev="section_N2291961" href="./section_N2291961.html">Viewing and Editing a Demand Plan</a>.</p>
        </li>
        <li>
          <p>Generate supply plans for items.</p>
          <p>This includes identifying a start date and end date to generate plans. If you use Multiple-Location Inventory, you must also identify a location.</p>
          <p>A supply plan can be generated from a demand plan using the Calculate Supply Plan page. For details, read <a rev="section_N2293372" href="./section_N2293372.html">Generating Item Supply Plans</a> or <a rev="section_N2294552" href="./section_N2294552.html">Manual Item Supply Plans</a>.</p>
        </li>
        <li>
          <p>Review supply plans.</p>
          <p>Review the orders suggested for item replenishment and make any necessary changes.</p>
          <p>Read <a rev="section_N2294140" href="./section_N2294140.html">Viewing and Editing a Supply Plan</a>.</p>
        </li>
        <li>
          <p>Order items.</p>
          <p>The orders suggested by supply plans must be generated using the Order Items page or the Mass Create Work Orders page.</p>
          <p>For details, read <a rev="section_N2294794" href="./section_N2294794.html">Creating Orders from Supply Plans</a> and <a rev="section_N2330082" href="./section_N2330082.html">Mass Creating Work Orders</a>.</p>
        </li>
      </ol>
      <p>To monitor your demand plans, supply plans, purchase orders, and work orders, you can use NetSuite reporting. For details, read <a rev="section_N2295256" href="./section_N2295256.html">Reporting on Demand Planning</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics:</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2289082" href="./section_N2289082.html">Demand Planning on Item Records</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2291615" href="./section_N2291615.html">Monitoring the Demand Plan Status</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2293790" href="./section_N2293790.html">Monitoring the Supply Plan Status</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

