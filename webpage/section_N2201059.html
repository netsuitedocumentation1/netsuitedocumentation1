<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="book_N2164398" href="./book_N2164398.html">Item Record Management</a>
            <a rev="chapter_N2191369" href="./chapter_N2191369.html">Item Costing</a>
            <a rev="section_N2199708" href="./section_N2199708.html">Standard Costing</a>
            <a class="nshelp_navlast" rev="section_N2201059" href="./section_N2201059.html">Creating Cost Categories</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2201059" class="nshelp_title">Creating Cost Categories</h1>
      <p><span id="dyn_help_feature" dynid="LANDEDCOST">&nbsp;</span>   </p>
      <p><span id="dyn_help_feature" dynid="STANDARDCOSTING">&nbsp;</span>   </p>
      <p>Cost category records are used to classify different types of costs associated with your items. Using cost categories helps you to track costs and variances in the manufacturing workflow.</p>
      <p>For example, you manufacture widgets to sell to your customers. When you manufacture a widget, you assemble materials made of wood and metal and then paint the widget after it is put together. For accounting purposes, you want to track the cost of each material and service you use to create each widget. To do so, you can create cost categories that define several kinds of costs that can be incurred during widget manufacturing.</p>
      <p>You might create cost category records such as the following:</p>
      <ul class="nshelp">
        <li>
          <p>Material: Metal</p>
        </li>
        <li>
          <p>Material: Wood</p>
        </li>
        <li>
          <p>Labor: Painting</p>
        </li>
      </ul>
      <p>After the cost category records are created, you can then assign a cost category to each item and material you use. Cost category assignment might look like the following:</p>
      <table class="alternate_rows" style="min-width:325px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:89px;" class="bolight_bgdark">
								<p>Item Name</p>
							</th>
            <th style="min-width:130px;" class="bolight_bgdark">
								<p>Description</p>
							</th>
            <th style="min-width:79px;" class="bolight_bgdark">
								<p>Cost Category</p>
							</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:89px;" class="bolight_bgwhite">
								<p>Item AB1001</p>
							</td>
            <td style="min-width:130px;" class="bolight_bgwhite">
								<p>Wooden Widget Component 1</p>
							</td>
            <td style="min-width:79px;" class="bolight_bgwhite">
								<p>Material: Wood</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Item AB1002</p>
							</td>
            <td class="bolight_bglight">
								<p>Wooden Widget Component 2</p>
							</td>
            <td class="bolight_bglight">
								<p>Material: Wood</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
								<p>Item AB1003</p>
							</td>
            <td class="bolight_bgwhite">
								<p>Metal Widget Component 1</p>
							</td>
            <td class="bolight_bgwhite">
								<p>Material: Metal</p>
							</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
								<p>Service Item XY2002</p>
							</td>
            <td class="bolight_bglight">
								<p>Widget Painting</p>
							</td>
            <td class="bolight_bglight">
								<p>Labor: Painting</p>
							</td>
          </tr>
        </tbody>
      </table>
      <p>After each item has a cost category specified, it is easier to track total costs for each category. When you process a production run of widgets, you can know how much was spent on wooden materials, how much was spent on metal materials, and how much was spent on service labor to produce the widgets you created.</p>
      <p>Additionally, when there are variances in production costs for assembly items, the variances can be tracked by cost categories. For example, if you process a production run of widgets and the cost for that run is much higher than you expected, you can know that a higher cost for the components in the Material: Wood category were the cause of the cost overrun.</p>
      <p>Cost categories can be specified on each item record.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The Cost Category field is available only for these item types: Inventory, Non-inventory, Service, and Other Charges.</p>
      </div>
      <br>
      <ul class="nshelp">
        <li>
          <p>Inventory items can have only one material cost category.</p>
        </li>
        <li>
          <p>Service items can have only one service cost category.</p>
        </li>
        <li>
          <p>Assembly items can have more than one cost category because assembly costs are defined by the component members. The cost amount and cost category of each component member of an assembly is used to calculate the cost of the assembly and identify the categories associated with it. This calculation process is called a cost rollup for assemblies.</p>
          <p>For more details, read <a rev="section_N2204939" href="./section_N2204939.html">Standard Cost Rollup</a>.</p>
        </li>
      </ul>
      <p>When the Standard Cost feature is first enabled, NetSuite automatically creates one cost category. This category is used by default for all new inventory, assembly, and service item records you create. You can add more cost categories as needed.</p>
      <div class="nshelp_procedure">
        <h4 id="procedure_N2201428">To create a cost category:</h4>
        <ol class="nshelp">
          <li>
            <p>Go to <span id="dyn_help_taskpath" dynid="EDIT_ACCOUNTINGOTHERLIST">&nbsp;</span>.</p>
          </li>
          <li>
            <p>Click <strong>Cost Category</strong>.</p>
          </li>
          <li>
            <p>Enter a name for the category.</p>
          </li>
          <li>
            <p>In the <strong>Cost Type</strong> field, select one of the following:</p>
            <ul class="nshelp">
              <li>
                <p>
                  <strong>Landed</strong>
                </p>
              </li>
              <li>
                <p>
                  <strong>Material</strong>
                </p>
              </li>
              <li>
                <p>
                  <strong>Service</strong>
                </p>
              </li>
            </ul>
            <div class="nshelp_note">
              <h3>Note</h3>
              <p class="nshelp_first">The option for <strong>Landed</strong> shows only if you have enabled the Landed Cost feature.</p>
            </div>
            <br>
          </li>
          <li>
            <p>In the <strong>Expense Account</strong> field, select the appropriate default expense account to be used as a clearing account for the landed cost of items. Then, when the item is sold, the cost of goods sold is accurately reflected.</p>
            <div class="nshelp_note">
              <h3>Note</h3>
              <p class="nshelp_first">The <strong>Expense Category</strong> field only shows on a cost category record when you select <strong>Landed</strong> in the <strong>Cost Type</strong> field. An expense account cannot be associated with a Material or Service type cost category.</p>
            </div>
            <br>
            <p>If you prefer not to associate a landed cost category with a Cost Of Goods Sold (COGS) account, the landed cost category account is intended as a holding account.</p>
            <p>When landed cost is allocated, it posts to two accounts:</p>
            <ul class="nshelp">
              <li>
                <p>the asset account of the item</p>
              </li>
              <li>
                <p>the landed cost category account</p>
              </li>
            </ul>
            <p>That posting is balanced out by a purchase line, either on the same bill or another purchase transaction, such as a shipping bill. The costing is accounted for in the COGS account of the item after the item is sold.</p>
            <div class="nshelp_imp">
              <h3>Important</h3>
              <p class="nshelp_first">If the Expand Accounts preference is enabled, you can choose any account, not just bank accounts or expense accounts.</p>
            </div>
            <br>
          </li>
          <li>
            <p>Check the <strong>Inactive</strong> box only if you do not want this category to show in lists.</p>
            <div class="nshelp_note">
              <h3>Note</h3>
              <p class="nshelp_first">A cost category can only be inactivated if there are no items associated with that category.</p>
            </div>
            <br>
          </li>
          <li>
            <p>Click <strong>Save</strong>.</p>
          </li>
        </ol>
      </div>
      <p>Now, you can select this cost category on item records and landed costs can be included on receiving transactions.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics:</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2199708" href="./section_N2199708.html">Standard Costing</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2200714" href="./section_N2200714.html">Enabling Standard Costing</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2201836" href="./section_N2201836.html">Setting Up Item Records for Standard Costing</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2202327" href="./section_N2202327.html">Defining Cost Versions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2202799" href="./section_N2202799.html">Entering Planned Standard Cost Records</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2204939" href="./section_N2204939.html">Standard Cost Rollup</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2205401" href="./section_N2205401.html">Revaluing Standard Cost Inventory</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2205923" href="./section_N2205923.html">Manually Enter an Inventory Cost Revaluation</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2206397" href="./section_N2206397.html">Standard Costing and Transactions</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2209083" href="./section_N2209083.html">Assembly Build Production Cost Variances</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2211106" href="./section_N2211106.html">Standard Costing FAQ</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2211461" href="./section_N2211461.html">Standard Costing Reporting</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

