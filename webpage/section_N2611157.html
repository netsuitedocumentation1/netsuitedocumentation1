<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2453322" href="./set_N2453322.html">SuiteCommerce</a>
            <a rev="book_N2561640" href="./book_N2561640.html">SuiteCommerce Site Builder</a>
            <a rev="chapter_N2601007" href="./chapter_N2601007.html">Site Builder Customization</a>
            <a class="nshelp_navlast" rev="section_N2611157" href="./section_N2611157.html">Web Site URL Parameters</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2611157" class="nshelp_title">Web Site URL Parameters</h1>
      <p><span id="dyn_help_feature" dynid="ADVANCEDSITECUST">&nbsp;</span>   </p>
      <p>This section describes the parameters exposed on your web store URLs and describes how web store managers and web site designers can use these parameters to display specific pages in a NetSuite web store.</p>
      <p>The following topics are discussed in this section:</p>
      <ul class="nshelp">
        <li>
          <p><a rev="section_N2611580" href="./section_N2611580.html">URL Parameters for Displaying Shopping Pages</a> – Describes how to display search results as well as specific tab, category, and item pages, on your web store.</p>
        </li>
        <li>
          <p><a rev="section_N2612472" href="./section_N2612472.html">URL Parameters for Adding Items to the Cart</a> – Includes information about how to construct a URL for adding items to the shopping cart in a OneWorld account.</p>
        </li>
        <li>
          <p><a rev="section_N2613194" href="./section_N2613194.html">URL Parameters for Passing Marketing Information</a> - Describes how to construct URLs for tracking partners and for use in marketing your web store.</p>
        </li>
        <li>
          <p><a rev="section_N2614795" href="./section_N2614795.html">URL Parameters for Setting Values in Your OneWorld Web Store</a> - Describes URL parameters for use with a OneWorld web store.</p>
        </li>
        <li>
          <p><a rev="section_N2615184" href="./section_N2615184.html">URL Parameters for Setting the Currency on your Web Site</a> - Identifies the URL parameters you can use to set the currency for prices displayed on a page of your web store.</p>
        </li>
      </ul>
      <h2 id="bridgehead_N2611291">Anatomy of a Web Site Shopping URL</h2>
      <p>Before you can start working with URL parameters, it is important to understand how NetSuite web store URLs are formed. Whether you use NetSuite's shopping domain (shopping.netsuite.com) or your own domain, understanding how to add parameters to your web site URLs gives you more options for customization. For more information about domains, see <a rev="section_N2479088" href="./section_N2479088.html">Domain Names</a>.</p>
      <h3 id="bridgehead_N2611312">Forming the Base Domain of a URL</h3>
      <p>The base domain of a NetSuite web store consists of the following variables: the domain for your web site, your NetSuite account number (if you use the NetSuite shopping domain), and variables indicating which web site page to display, such as internal IDs for item and category records.</p>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">The NetSuite shopping domain used to construct the base domain for your site varies according to the host data center for your NetSuite account. You can always find the shopping domain for your site on the Set Up Domains page or on the Domains subtab on the Web Site Setup page. For more information on data centers, see <a rev="section_N240322" href="./section_N240322.html">Understanding Multiple Data Centers</a>.</p>
      </div>
      <br>
      <p>You may notice that NetSuite shopping URLs appear in two different formats:</p>
      <ul class="nshelp">
        <li>
          <p>http://shopping.netsuite.com/s.nl/c.123456/id.12/.f?category=123</p>
        </li>
        <li>
          <p>http://shopping.netsuite.com/s.nl?c=123456&amp;id=12&amp;category=123</p>
        </li>
      </ul>
      <p>The first format above is more favorable for search engine optimization (SEO). However, when you are constructing a URL for a process, such as adding an item to the shopping cart, both formats work equally well. Note that custom domains also appear in both formats:</p>
      <ul class="nshelp">
        <li>
          <p>http://www.wolfeelectronics.com/s.nl/id.12/.f?category=123</p>
        </li>
        <li>
          <p>http://www.wolfeelectronics.com/s.nl?id=12&amp;category=123</p>
          <div class="nshelp_imp">
            <h3>Important</h3>
            <p class="nshelp_first">If you use your own custom domain, you should never use the NetSuite shopping domain to construct URL parameters for your web site.</p>
          </div>
          <br>
        </li>
      </ul>
      <p>Alternating between the NetSuite shopping domain (shopping.netsuite.com), and your own custom domain (www.mysite.com) can cause cookie and session information to get lost as visitors click through the pages of your site.</p>
      <h3 id="bridgehead_N2611392">Required Web Site URL Parameters</h3>
      <p>If you use the NetSuite shopping domain, the <em>c</em> parameter is required to identify your NetSuite account. If you use the Multiple Web Sites feature, the <em>n</em> parameter, which identifies the internal ID for the site, is also required. For example, the URL below points to a tab on site 3 <em>(n.3)</em> in NetSuite account number 123456.</p>
      <p>
        <code>http://shopping.netsuite.com/s.nl/c.123456/n.3/sc.13/.f</code>
      </p>
      <p>If you use custom domains for your web stores, however, the <em>c</em> and <em>n</em> parameters are not required because your custom domain is mapped to the correct NetSuite account and web site at the database level. The URL below uses a custom domain to show the same page as the URL above.</p>
      <p>
        <code>http://www.wolfeelectronics.com/s.nl/sc.13/.f</code>
      </p>
      <p>All web site URLs from your NetSuite account use <em>/s.nl/</em> in the path to indicate an e-commerce page request. Note that URLs which contain <em>/.f?</em> before additional parameters are more favorable for SEO.</p>
      <h3 id="bridgehead_N2611462">Adding Parameters to a URL</h3>
      <p>In addition to identifying the domain and the web site page, the URL includes parameters which can be used in a query string.</p>
      <p>By adding a query string to the URL of a NetSuite web page, you can display specific information on the page. A query string contains name and value pairs, with the name and value in each pair separated by an equal sign (=).</p>
      <p>For example, when a shopper clicks on one of the URLs below, he or she is taken to a web store tab presenting a list of results for the search term, “electronics.” Note that the first example uses the NetSuite shopping domain and identifies the NetSuite account number (c.123456).</p>
      <ul class="nshelp">
        <li>
          <p>http://shopping.netsuite.com/s.nl/c.123456/sc.2/.f?search=electronics</p>
        </li>
        <li>
          <p>http://www.wolfeelectronics.com/s.nl/sc.2/.f?search=electronics</p>
        </li>
      </ul>
      <p>The tab's internal ID is defined in the sc parameter. You can find the internal ID for any record in NetSuite in the address bar of your browser when you navigate to that record. For more information, see <a rev="section_N2904065" href="./section_N2904065.html">How do I find a record's internal ID?</a></p>
      <p>Note that the first parameter (in this case “search=electronics”) is always preceded by a question mark (?), and any subsequent parameters are preceded by ampersands (&amp;). The URLs below display items 1 through 5 in a list of 10 search results for “electronics.”</p>
      <ul class="nshelp">
        <li>
          <p>http://shopping.netsuite.com/s.nl/c.123456/sc.2/.f?search=electronics&amp;range=1,5,10</p>
        </li>
        <li>
          <p>http://www.wolfeelectronics.com/s.nl/sc.2/.f?search=electronics&amp;range=1,5,10</p>
        </li>
      </ul>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2602412" href="./section_N2602412.html">Basic Customization</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2603624" href="./section_N2603624.html">Advanced Site Customization</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2601007" href="./chapter_N2601007.html">Site Builder Customization</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

