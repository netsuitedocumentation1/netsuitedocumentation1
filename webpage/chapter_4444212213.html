<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3363377" href="./book_N3363377.html">SuiteBundler</a>
            <a class="nshelp_navlast" rev="chapter_4444212213" href="./chapter_4444212213.html">SuiteApp Development Process with SuiteBundler</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="chapter_4444212213" class="nshelp_title">SuiteApp Development Process with SuiteBundler</h1>
      <p><span id="dyn_help_feature" dynid="CREATESUITEBUNDLES">&nbsp;</span>   </p>
      <p>The SuiteBundler feature offers flexibility in how you distribute bundles to other NetSuite accounts. Whether you are an internal developer creating customizations for your company, an independent software developer (ISV) distributing solutions to your customers, or an administrator who wants to make a bundle available to other users at your company, NetSuite provides different methods for you to use.</p>
      <p>At the core of this SuiteApp development process is the development account. A NetSuite development account is an account, isolated from production, in which you can develop and test new applications and customizations without worrying about affecting your production account. Each development account has the same features and modules as a production account, but it does not contain production data. For more information about development accounts, see <a rev="chapter_4422375565" href="./chapter_4422375565.html">NetSuite Development Accounts</a>.</p>
      <p>You can use separate development accounts for developing, testing, and releasing customizations. Use SuiteBundler in conjunction with the customizations created in a development account to package the SuiteApp as a bundle for distribution.</p>
      <p>Support for bundle operations varies across the different types of NetSuite accounts, including development accounts. See <a rev="section_4853873261" href="./section_4853873261.html">Bundle Support Across Account Types</a>.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">This section contains best practices for managing SuiteApp development with development accounts in conjunction with sandbox accounts. These best practices are recommended by NetSuite. You can, however, adapt the best practices to your own specific requirements.</p>
        <p>You can use SuiteCloud Development Framework (SDF) to develop your SuiteApp. SDF includes support for SuiteApp projects, self-contained, standalone projects that enable SuiteCloud Developer Network (SDN) members to develop and deploy SuiteApps to their NetSuite accounts. SuiteBundler is used to bundle and share SDF SuiteApps. You can create a bundle that contains all objects from an SDF SuiteApp project without manually adding all of the objects to the bundle in the Bundle Builder. For information about using SDF, see <a rev="chapter_4702622163" href="./chapter_4702622163.html">SuiteCloud Development Framework Overview (Beta)</a>.</p>
      </div>
      <br>
      <p>The following table lists the topics where you can get more information about these processes:</p>
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:148px;" class="bolight_bgdark" valign="top">
						<p>Topic</p>
					</th>
            <th style="min-width:463px;" class="bolight_bgdark" valign="top">
						<p>Description</p>
					</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:148px;" class="bolight_bgwhite">
						<p><a rev="chapter_4444212213" href="#bridgehead_4444219652">SuiteApp Development Terminology</a></p>
					</td>
            <td style="min-width:463px;" class="bolight_bgwhite">
						<p>Terminology used to describe the bundle development process.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p><a rev="chapter_4444212213" href="#bridgehead_4444220418">Which Method Should I Choose?</a></p>
					</td>
            <td class="bolight_bglight">
						<p>Description of the different methods used to develop SuiteApps with development accounts. Includes information to decide on the best method for your requirements.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p><a rev="chapter_4444212213" href="#bridgehead_4444243484">Additional SuiteApp Development Guidelines</a></p>
					</td>
            <td class="bolight_bgwhite">
						<p>Guidelines to use during SuiteApp development.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p><a rev="section_4444212730" href="./section_4444212730.html">Single Development Account Method</a></p>
					</td>
            <td class="bolight_bglight">
						<p>How to use a single development account to develop SuiteApps.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p><a rev="section_4444245637" href="./section_4444245637.html">Multiple Development Account Methods</a></p>
					</td>
            <td class="bolight_bgwhite">
						<p>How to use multiple development accounts in a SuiteApp development environment. The recommended approach depends on how many simultaneous versions of an individual SuiteApp that you want to develop:</p>
						<ul class="nshelp"><li><p><a rev="section_4444245991" href="./section_4444245991.html">Single Development Account Environment Method</a></p></li><li><p><a rev="section_4444246225" href="./section_4444246225.html">Multiple Development Account Environments Method</a></p></li></ul>
					</td>
          </tr>
        </tbody>
      </table>
      <h2 id="bridgehead_4444219652">SuiteApp Development Terminology</h2>
      <p>This section uses the following terminology when describing the SuiteApp development process:</p>
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:137px;" class="bolight_bgdark" valign="top">
						<p>Term</p>
					</th>
            <th style="min-width:474px;" class="bolight_bgdark" valign="top">
						<p>Description</p>
					</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:137px;" class="bolight_bgwhite">
						<p>development account</p>
					</td>
            <td style="min-width:474px;" class="bolight_bgwhite">
						<p>Account in which you can develop and test new applications and customizations. For a more detailed description of development accounts, see <a rev="chapter_4422375565" href="./chapter_4422375565.html">NetSuite Development Accounts</a>.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>development account environment</p>
					</td>
            <td class="bolight_bglight">
						<p>Set of development accounts used as an environment in which to develop SuiteApps. Each development account is used for a specific purpose in the SuiteApp development process.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>target account</p>
					</td>
            <td class="bolight_bgwhite">
						<p>NetSuite account into which an administrator installs a bundle.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>source account</p>
					</td>
            <td class="bolight_bglight">
						<p>NetSuite development account from which a bundle is installed into another account.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>sandbox account</p>
					</td>
            <td class="bolight_bgwhite">
						<p>NetSuite can provision one or more sandbox accounts for use with a production account. For more information, see <a rev="chapter_N333400" href="./chapter_N333400.html">NetSuite Sandbox</a>.</p>
						<p>NetSuite account used for the integration and user acceptance testing of customizations created in a development account environment before installation in a production account.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>release account</p>
					</td>
            <td class="bolight_bglight">
						<p>NetSuite development account dedicated to the release of bundles. A release account can serve as the source account of bundles that customers install into their sandbox or production accounts.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p>test account</p>
					</td>
            <td class="bolight_bgwhite">
						<p>NetSuite development account dedicated to the testing of bundles. Use a testing account to perform QA on NetSuite customizations that were developed in a development account.</p>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p>develop account</p>
					</td>
            <td class="bolight_bglight">
						<p>NetSuite development account dedicated to the development of customizations, independent of production customizations or production data.</p>
					</td>
          </tr>
        </tbody>
      </table>
      <h2 id="bridgehead_4444220418">Which Method Should I Choose?</h2>
      <p>The following table lists the different development methods and the relative benefits:</p>
      <table class="alternate_rows" style="min-width:629px" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="min-width:93px;" class="bolight_bgdark" valign="top">
						<p>Method</p>
					</th>
            <th style="min-width:147px;" class="bolight_bgdark" valign="top">
						<p>Description</p>
					</th>
            <th style="min-width:144px;" class="bolight_bgdark" valign="top">
						<p>Benefits</p>
					</th>
            <th style="min-width:209px;" class="bolight_bgdark" valign="top">
						<p>Drawbacks</p>
					</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="min-width:93px;" class="bolight_bgwhite">
						<p><a rev="section_4444212730" href="./section_4444212730.html">Single Development Account Method</a></p>
					</td>
            <td style="min-width:147px;" class="bolight_bgwhite">
						<p>Uses a single development account.</p>
						<p>The single development account is used for developing, testing, and releasing a SuiteApp.</p>
					</td>
            <td style="min-width:144px;" class="bolight_bgwhite">
						<ul class="nshelp"><li><p><strong>Size.</strong> Appropriate for smaller customization projects.</p></li><li><p><strong>Release and update management.</strong> Easy to release or update production-ready bundles.</p></li></ul>
					</td>
            <td style="min-width:209px;" class="bolight_bgwhite">
						<ul class="nshelp"><li><p><strong>Bug fixes.</strong> Difficult to release bug fixes for older versions of the bundle.</p></li><li><p><strong>Future releases.</strong> If customers do not upgrade in a timely manner to the new version of a bundle, then the capability to jump versions during a future upgrade becomes problematic. For example, it may not be possible to upgrade from version 1.0 of a bundle to version 3.5 of the same bundle.</p></li><li><p><strong>No difference between development and release.</strong> No de-coupling of development and release environments. Changes to objects in a bundle in the development account are included in subsequent installations or updates by other accounts.</p></li><li><p><strong>Consistency.</strong> No consistent copy of current release version of the bundle.</p></li><li><p><strong>Changed objects affect bundle components.</strong> Any change to an object in a bundle affects the behavior of the bundle. So, a change to a single object in a bundle could be installed as an update.</p></li><li><p><strong>No team development.</strong> Multiple developers need to work in the same environment.</p></li></ul>
					</td>
          </tr>
          <tr>
            <td class="bolight_bglight">
						<p><a rev="section_4444245991" href="./section_4444245991.html">Single Development Account Environment Method</a></p>
					</td>
            <td class="bolight_bglight">
						<p>Uses three development accounts to develop one version of a bundle at a time.</p>
						<p>A bundle developer works in one account, stages the bundle for release in a second account, and tests in a third account.</p>
					</td>
            <td class="bolight_bglight">
						<ul class="nshelp"><li><p><strong>Separate accounts.</strong> De-couples development, test, and release accounts.</p></li><li><p><strong>Avoids bundle overwrites.</strong> Inadvertent overwriting of bundle content more unlikely without using sandbox for testing and release.</p></li><li><p><strong>Testing with production data.</strong> Uses sandbox account for user acceptance and integration testing with production data.</p></li></ul>
					</td>
            <td class="bolight_bglight">
						<ul class="nshelp"><li><p><strong>Bug fixes.</strong> Difficult to release bug fixes for older versions of the bundle.</p></li><li><p><strong>No multiple version development.</strong> Supports development of only one version of a SuiteApp at a time.</p></li><li><p><strong>Team development.</strong> Multiple developers working on a single version of a SuiteApp would be accessing the same account.</p></li><li><p><strong>SuiteApp versioning.</strong> Can be difficult to manage SuiteApp versioning between development cycles.</p></li></ul>
					</td>
          </tr>
          <tr>
            <td class="bolight_bgwhite">
						<p><a rev="section_4444246225" href="./section_4444246225.html">Multiple Development Account Environments Method</a></p>
					</td>
            <td class="bolight_bgwhite">
						<p>Uses two environments, where each environment contains three development accounts. Used to simultaneously develop multiple versions of a single SuiteApp.</p>
						<p>In each environment, a bundle developer works in one account, stages the bundle for release in a second account, and tests in a third account.</p>
					</td>
            <td class="bolight_bgwhite">
						<ul class="nshelp"><li><p><strong>Bug fixes.</strong> Can fix bugs in one environment and at the same time develop new version in a separate environment.</p></li><li><p><strong>Separate code bases.</strong> Bundle being developed separate from release version.</p></li><li><p><strong>Separate accounts.</strong> De-couples development, test, and release accounts.</p></li><li><p><strong>Avoids bundle overwrites.</strong> Inadvertent overwriting of bundle content more difficult without using sandbox for testing and release.</p></li><li><p><strong>Testing with production data.</strong> Uses sandbox account for user acceptance and integration testing with production data.</p></li></ul>
					</td>
            <td class="bolight_bgwhite">
						<ul class="nshelp"><li><p><strong>Team development.</strong> Multiple developers working on a single version of a SuiteApp would be accessing the same account.</p></li><li><p><strong>SuiteApp versioning.</strong> Can be difficult to manage SuiteApp versioning between development cycles.</p></li></ul>
					</td>
          </tr>
        </tbody>
      </table>
      <h2 id="bridgehead_4444243484">Additional SuiteApp Development Guidelines</h2>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">Use the following guidelines when developing SuiteApps with the SuiteBundler development methods for development accounts.</p>
      </div>
      <br>
      <ul class="nshelp">
        <li>
          <p>NetSuite follows a phased release schedule for each new version. The phasing process from initial release until the point where all customers are using the new release occurs over several weeks. During the phasing periods, developers, particularly ISVs, must coordinate the installation and update of bundles to ensure that bundles requiring new features are installed into upgraded accounts.</p>
          <p>For more information, see <a rev="section_N3382536" href="./section_N3382536.html">Bundle Support during NetSuite Release Phasing</a>.</p>
        </li>
        <li>
          <p>You can use managed bundles to streamline the process of updating and supporting your bundles. For details, see <a rev="section_N3382953" href="./section_N3382953.html">Understanding Managed Bundles</a>. ISVs must join the SuiteCloud Developer Network to obtain the appropriate development and testing accounts. See the <a href="http://www.netsuite.com/portal/developers/main.shtml" target="_blank">SuiteCloud Developer Network</a> page on the NetSuite web site.</p>
        </li>
        <li>
          <p>The development methods described in this section are not intended for bundles developed in sandbox accounts. For information about developing bundles in sandbox accounts, see <a rev="chapter_N3405306" href="./chapter_N3405306.html#bridgehead_3770203406">Sandbox Bundle Deployment Models</a>.</p>
        </li>
        <li>
          <p>If you use SuiteScript 2.0 from a development account, make sure that you review and set an appropriate module scope for scripts using a JSDoc tag. To successfully import a SuiteScript 2.0 script created in a development account into another environment (such as your NetSuite production or sandbox account), the script must include the following JSDoc Tag:</p>
          <pre class="nshelp">/**
*@NModuleScope Public</pre>
          <p>For more information, see <a rev="chapter_4387175355" href="./chapter_4387175355.html">SuiteScript 2.0 JSDoc Validation</a> and <a rev="section_4502146091" href="./section_4502146091.html#bridgehead_4528959411">NetSuite Development Accounts and Module Scope</a>.</p>
        </li>
      </ul>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

