<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N3412393" href="./book_N3412393.html">SuiteTalk (Web Services)</a>
            <a rev="part_N3412452" href="./part_N3412452.html">SuiteTalk (Web Services) Platform Guide</a>
            <a rev="chapter_N3477815" href="./chapter_N3477815.html">Web Services Operations</a>
            <a rev="section_N3514306" href="./section_N3514306.html">search</a>
            <a class="nshelp_navlast" rev="section_N3514760" href="./section_N3514760.html">Basic Searches in Web Services</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N3514760" class="nshelp_title">Basic Searches in Web Services</h1>
      <p>See the following topics to learn how to execute a basic search in NetSuite using web services:</p>
      <ul class="nshelp">
        <li>
          <p>
            <a rev="section_N3514760" href="#bridgehead_N3514825">What is a basic search?</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N3514760" href="#bridgehead_N3514890">Which SuiteTalk objects are used in a basic search?</a>
          </p>
        </li>
        <li>
          <p>
            <a rev="section_N3519853" href="./section_N3519853.html#bridgehead_N3519940">Basic Search Code Sample</a>
          </p>
        </li>
      </ul>
      <h3 id="bridgehead_N3514825">What is a basic search?</h3>
      <p>A basic search lets you search records of a specific type using the fields on that record as search filters. The following figure shows the UI equivalent of a basic Customer search. Start by going to Lists &gt; Relationships &gt; Customers &gt; Search, and ensure that the Use Advanced Search box is not checked. You can specify one or more field values to use as filters for search results.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteTalkWebServices/SimpleSearch.png" width="630px" height="386px">
      <p>In a basic search, field criteria are the <strong>only</strong> values you set. You cannot specify search return columns . In web services, specifying search return columns is the equivalent of performing an advanced search. (See <a rev="section_N3516862" href="./section_N3516862.html">Advanced Searches in Web Services</a> for details.)</p>
      <p>In the example of a basic Customer search (see previous figure), the results returned include the record ID of every customer that has the Category field set to <em>From Advertisement</em> and the Status field set to <em>Customer-Closed Won</em>. Note that <strong>ALL</strong> the other data associated with these specific customer records are returned as well. As a consequence, in web services a basic search tends to increase the search response time.</p>
      <h2 id="bridgehead_N3514890">Which SuiteTalk objects are used in a basic search?</h2>
      <p>To perform a basic search in which you specify search filter criteria only, use:</p>
      <ol class="nshelp">
        <li>
          <p>&lt; <em>Record</em> &gt; <strong>Search</strong></p>
        </li>
        <li>
          <p>&lt; <em>Record</em> &gt; <strong>SearchBasic</strong></p>
        </li>
      </ol>
      <p>For more details, see <a rev="section_N3514760" href="#bridgehead_N3514958">Basic Search Objects Explained</a> and <a rev="section_N3519853" href="./section_N3519853.html#bridgehead_N3519940">Basic Search Code Sample</a>.</p>
      <h3 id="bridgehead_N3514958">Basic Search Objects Explained</h3>
      <p>In SuiteTalk, any record that supports search has a corresponding &lt; <em>Record</em> &gt; <strong>Search</strong> object, which contains a <em>basic</em> element. The <em>basic</em> element references a &lt; <em>Record</em> &gt; <strong>SearchBasic</strong> object, which defines all available search criteria (filter fields) specific to that record type.</p>
      <p>The XSD snippet below shows the CustomerSearch object. The <em>basic</em> element references the CustomerSearchBasic object, which defines all available search criteria for the Customer record.</p>
      <pre class="nshelp">&lt;complexType name=" CustomerSearch "&gt;
        &lt;complexContent&gt;
            &lt;extension base="platformCore:SearchRecord"&gt;
                &lt;sequence&gt;
                    &lt;element name=" basic " type="platformCommon: CustomerSearchBasic " minOccurs="0"/&gt;
                    &lt;element name="callJoin" type="platformCommon:PhoneCallSearchBasic" minOccurs="0"/&gt;
                    &lt;element name="campaignResponseJoin" type="platformCommon:CampaignSearchBasic"
         minOccurs="0"/&gt;
                 .....
                
      &lt;/sequence&gt;
            &lt;/extension&gt;
        &lt;/complexContent&gt;
    &lt;/complexType&gt;</pre>
      <div class="nshelp_note">
        <h3>Note</h3>
        <p class="nshelp_first">&lt; <em>Record</em> &gt; <strong>Search</strong> objects reside in the same XSD as their corresponding record objects. In this case, both the Customer and CustomerSearch objects reside in the <a href="https://webservices.netsuite.com/xsd/lists/v2016_2_0/relationships.xsd" target="_blank">listRel</a> XSD. Also note that the CustomerSearch object, like all &lt; <em>Record</em> &gt; <strong>Search</strong> objects, provide available search joins for that record type. For information on joined searches, see <a rev="section_N3516345" href="./section_N3516345.html">Joined Searches in Web Services</a>.</p>
      </div>
      <br>
      <p>This snippet shows the CustomerSearchBasic object. All &lt; <em>Record</em> &gt; <strong>SearchBasic</strong> objects are defined in the <a href="https://webservices.netsuite.com/xsd/platform/v2016_2_0/common.xsd" target="_blank">platformCommon</a> XSD. This sample shows four of the available fields (accountNumber, address, addressee, addressLabel) that can be used as search criteria on the Customer record.</p>
      <pre class="nshelp">&lt;complexType name=" CustomerSearchBasic "&gt;
      &lt;complexContent&gt;
   &lt;extension base="platformCore:SearchRecord"&gt;
      &lt;sequence&gt;
        &lt;element name=" accountNumber " type="platformCore:SearchStringField" minOccurs="0" /&gt; 
        &lt;element name=" address " type="platformCore:SearchStringField" minOccurs="0" /&gt; 
       &lt;element name=" addressee " type="platformCore:SearchStringField" minOccurs="0" /&gt; 
        &lt;element name=" addressLabel " type="platformCore:SearchStringField" minOccurs="0" /&gt; 
               ...
     &lt;/sequence&gt;
         &lt;/extension&gt;
   &lt;/complexContent&gt;
&lt;/complexType&gt;</pre>
      <p>For a code sample of a basic search, see <a rev="section_N3519853" href="./section_N3519853.html#bridgehead_N3519940">Basic Search Code Sample</a>.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3514306" href="./section_N3514306.html">search</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3516345" href="./section_N3516345.html">Joined Searches in Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3516862" href="./section_N3516862.html">Advanced Searches in Web Services</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3518134" href="./section_N3518134.html">Setting Valid Search Values</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3518157" href="./section_N3518157.html">Setting the anyof, mine, or myteam Filtering Values</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3518534" href="./section_N3518534.html">Searching by lastModifiedDate</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3518731" href="./section_N3518731.html">Understanding Sorting in Advanced Search</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3519853" href="./section_N3519853.html">Search-Related Sample Code</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N3522048" href="./section_N3522048.html">Searching for a Multi-select Custom Field</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

