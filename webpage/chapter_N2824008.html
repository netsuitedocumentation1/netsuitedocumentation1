<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N2807372" href="./set_N2807372.html">SuiteCloud (Customization, Scripting, and Web Services)</a>
            <a rev="book_N2823893" href="./book_N2823893.html">SuiteBuilder (Customization)</a>
            <a class="nshelp_navlast" rev="chapter_N2824008" href="./chapter_N2824008.html">SuiteBuilder Overview</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="chapter_N2824008" class="nshelp_title">SuiteBuilder Overview</h1>
      <p></p>
      <p>With the NetSuite SuiteBuilder customization tools you can tailor NetSuite to your individual business needs.</p>
      <p>These features allow you to adapt NetSuite to meet your company's business process. You can control the information that is accessed and entered by each user of your NetSuite account. SuiteBuilder provides a point and click interface for creating custom fields and forms, custom record types, transaction form layouts, and custom centers.</p>
      <p>Additionally, the SuiteBundler feature enables you to share customization objects with other NetSuite users.</p>
      <h2 id="bridgehead_N2824032">Who Uses SuiteBuilder?</h2>
      <p>There are three primary types of user of SuiteBuilder customization features:</p>
      <ul class="nshelp">
        <li>
          <p><strong>Administrators</strong> - When a company first begins using NetSuite, administrators spend time customizing transaction forms, adding custom record types, and setting up custom centers for roles within the company to get the rest of the company using the application. Some of the customization work is unique to a specific business. Some of the work can be accomplished by installing previously created ‘bundles', or packaged customizations.</p>
          <p>Bundles are created using SuiteBundler. Finding and installing bundles that have been created by other similar businesses can greatly reduce the work required to get a company started using NetSuite.</p>
          <p>Administrators also assign roles to each NetSuite user. These roles determine which information each user has access to in your NetSuite account.</p>
        </li>
        <li>
          <p><strong>IT Staff</strong> - For a company that uses NetSuite, the issues addressed by the IT department often include requests for changes to the NetSuite account. These issues can range from small tasks such as adding a field to a form to larger work items like creating custom record types.</p>
          <p>IT staff members also may have access to the administrative tools that allow them to audit changes to your NetSuite account, manage data in the system, and schedule batch processing jobs.</p>
        </li>
        <li>
          <p><strong>Developers</strong> – For developers of partner solutions and independent software vendors (ISVs), most time with NetSuite is spent coding SuiteScript and SuiteTalk, but to work with these features, developers need a solid understanding of how customization objects interact with their code.</p>
        </li>
      </ul>
      <h2 id="bridgehead_N2824091">NetSuite Anatomy</h2>
      <p>Making changes to NetSuite is a point-and-click process. Whether you have experience with other software applications or not, you can get up and running with NetSuite in no time. First, however, it helps to understand the custom elements you have to work with and how they work together.</p>
      <p>NetSuite uses the following terminology:</p>
      <ul class="nshelp">
        <li>
          <p><strong>Record</strong> – A single entry of information related to a single business concept.</p>
        </li>
        <li>
          <p><strong>Form</strong> – Page through which you enter records and transactions. Forms contain fields and usually have subtabs.</p>
        </li>
        <li>
          <p><strong>Field</strong> – Place on a record or transaction where information is entered.</p>
        </li>
        <li>
          <p><strong>Subtab</strong> – Section of a record or transaction that groups similar fields.</p>
          <p>An example of a standard subtab is the Address subtab where the shipping and billing addresses are entered on transactions and records.</p>
        </li>
        <li>
          <p><strong>Custom list</strong> – List of values that can be selected in a custom field.</p>
        </li>
        <li>
          <p><strong>Sublist</strong> – The results of a saved search displayed on a custom or standard record. Sublists can also be generated through parent-child relationships.</p>
        </li>
        <li>
          <p><strong>Script</strong> – SuiteScript JavaScript file that runs against a specific form or record type or that creates a custom portlet. Scripts can also be scheduled to execute periodically.</p>
        </li>
        <li>
          <p><strong>Role</strong> – Set of permissions that can be assigned to a NetSuite user.</p>
        </li>
        <li>
          <p><strong>Center</strong> – Configuration of NetSuite created for a specific group of roles with similar tasks.</p>
        </li>
        <li>
          <p><strong>Center tab</strong> – Section of NetSuite that groups similar links and other information. Standard tabs include Home, Reports, Documents, Activities, and Setup. You can also create custom center tabs.</p>
        </li>
      </ul>
      <p>Understanding forms is key to understanding how to work in NetSuite. The following screenshot shows a sales order form with the following elements highlighted:</p>
      <ol class="nshelp">
        <li>
          <p>fields</p>
        </li>
        <li>
          <p>subtabs</p>
          <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/Overview_formsFieldsSubtabs_anatomy_composite.png" width="608px" height="430px">
        </li>
      </ol>
      <p>In the previous example, notice that some of the fields allow the person filling out the form to choose from a list of values, some allow the entry of text or numbers, and some are auto-populated and read-only.</p>
      <p>Although some of the fields shown in the preceding example allow you to choose from lists defined in your account – Customer, Location, and Department, for example – you can also create your own lists of values to choose from. The example below shows a custom field that uses a custom list.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/Overview_customList.png" width="630px" height="232px">
      <p>For more information on fields, see <a rev="chapter_N2826978" href="./chapter_N2826978.html">Custom Fields</a>.</p>
      <p>You can create custom versions of most forms in NetSuite to include the fields – both standard and custom – you want to show. Adding custom fields to forms lets you capture information that is not included in NetSuite by default.</p>
      <p>With custom sublists, you can add lists of information to a form. Sublists present saved search results on form subtabs. In the following example, the <strong>Preferred Item</strong> sublist has been added to the <strong>Financial</strong> subtab on vendor records. The sublist shows selected fields for the items most often sold by the vendor.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/Overview_sublist_composite.png" width="630px" height="127px">
      <p>Customizing forms also enables you to adapt the information captured to specific roles in your company. When you create a form for a role, you choose which fields and subtabs appear on the form and which are hidden. You also have the flexibility of setting a custom form as the preferred (default) form or to restrict the role to only use a specific form.</p>
      <p>The following Custom Sales Person role is restricted to use only the Custom Cash Refund form when issuing refunds to customers. Note that the boxes in the Restricted and Preferred columns are checked.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/Overview_preferredRestrictedForm.png" width="630px" height="541px">
      <p>For more information on forms, see <a rev="chapter_N2852749" href="./chapter_N2852749.html">Custom Forms</a>.</p>
      <p>NetSuite enables you to change the labels on standard records to use terminology that is familiar to your users. For more information about changing the label of any standard record, see <a rev="section_N252023" href="./section_N252023.html">Renaming Records and Transactions</a>.</p>
      <p>SuiteBuilder allows standard NetSuite records to be customized by adding custom fields, subtabs, or sublists or through the removal of existing fields. NetSuite's standard records can support many of your business needs, but with SuiteBuilder you can create your own custom record types to fill in the gaps.</p>
      <p>The following screenshot shows the Applies To subtab of a custom entity field. On the Applies To subtab, you choose the standard records where your custom field appears.</p>
      <img class="nshelp_screenshot_lightbox" rel="prettyPhoto" title="Click to enlarge image" src="/help/helpcenter/en_US/Output/Help/images/SuiteCloudCustomizationScriptingWebServices/SuiteBuilderCustomization/Overview_AppliesToSubtab.png" width="630px" height="145px">
      <p>In the same way as standard records, custom records can be designed to be associated with other custom records or with standard records. You might need a lookup relationship where one type of record refers to another as well as a situation where a tighter parent-child relationship is needed. If the standard records do not suit your needs, you can use custom records instead. For example, if your organization finds standard records too complicated, you can create and use a copy that reduces the information required.</p>
      <p>You can create custom forms for your record types to give you finer control on how different roles within your company are able to interact with your custom records. You can copy and edit forms, and create new versions to set as preferred. Custom records ensure that a particular segment or user has a form that covers only the information they need, is named what they want, and uses field names that are meaningful to them (in the language they prefer). For more information on record types, see <a rev="chapter_N2875173" href="./chapter_N2875173.html">Custom Records</a>.</p>
      <p>If you need even more flexibility and automation, you can use SuiteScript, NetSuite’s JavaScript-based API. You can use SuiteScript to perform validations and calculations on forms, to access and update records when fields are changed or pages are submitted, or to automatically create or update records. For more information on what you can do with SuiteScript, see <a rev="chapter_N2902609" href="./chapter_N2902609.html">SuiteScript - The Basics</a>.</p>
      <p>Now, that you have a general idea of how NetSuite's customization objects work together, read <a rev="section_N2824537" href="./section_N2824537.html">Customizing Your NetSuite Account</a> for an overview of what is possible with SuiteBuilder.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2826978" href="./chapter_N2826978.html">Custom Fields</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2852749" href="./chapter_N2852749.html">Custom Forms</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2875173" href="./chapter_N2875173.html">Custom Records</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2890160" href="./chapter_N2890160.html">Custom Centers</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

