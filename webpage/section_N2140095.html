<script type="text/javascript" src="./assets/codeinject.js"></script>
 
 
 
 
<div id="nshelp" class="nshelp_body">
  <div class="nshelp_navheader">
    <table class="nshelp_nav">
      <tbody><tr>
        <td class="nshelp_navhome">
          <a rev="set_N0000001" href="./set_N0000001.html" class="nshelp_navfirst" title="Help Center Home Page" alt="Help Center Home Page">
            <img width="20px" height="20px" src="/help/helpcenter/en_US/Output/Help/css/images/icon_home.gif">
          </a>
        </td>
        <td class="nshelp_navpath">
          <div class="nshelp_navigation">
            <a rev="set_N1379402" href="./set_N1379402.html">Accounting</a>
            <a rev="preface_3710626334" href="./preface_3710626334.html">Fixed Assets Management</a>
            <a rev="chapter_N2137458" href="./chapter_N2137458.html">Depreciation Methods</a>
            <a class="nshelp_navlast" rev="section_N2140095" href="./section_N2140095.html">Depreciation Formula</a>
          </div>
        </td>
        <td class="nshelp_navbuttons">
          <a href="javascript:void(0)" onclick="parent.printContentFrame(); return false;" title="Print" alt="Print">
            <img width="20px" height="18px" src="/help/helpcenter/en_US/Output/Help/css/images/li-print.png">
          </a>
        </td>
      </tr>
    </tbody></table>
  </div>
  <div class="nshelp_content">
    <div class="nshelp_page">
      <h1 id="section_N2140095" class="nshelp_title">Depreciation Formula</h1>
      <p>In the <a rev="chapter_N2126830" href="./chapter_N2126830.html">Fixed Assets Management SuiteApp</a>, each depreciation method consists of a formula that describes how the amount of monthly or annual depreciation is calculated.</p>
      <div class="nshelp_imp">
        <h3>Important</h3>
        <p class="nshelp_first">For depreciation periods, only monthly is currently supported for the book (accounting) methods. Monthly and annually are both supported for alternate (tax depreciation) methods. For information about using tax methods in the Fixed Assets Management SuiteApp, see <a rev="chapter_N2140557" href="./chapter_N2140557.html">Alternate Methods (Tax Depreciation Methods)</a>.</p>
      </div>
      <br>
      <p>The Depreciation Method page includes a list of available operators and constants that you can use:</p>
      <ul class="nshelp">
        <li>
          <p>^ (to the power of, e.g., 5^2 = 5 squared)</p>
        </li>
        <li>
          <p>( ) * / +</p>
        </li>
        <li>
          <p>any number with decimals (e.g., 12345.67)</p>
          <div class="nshelp_note">
            <h3>Note</h3>
            <p class="nshelp_first">When creating a custom depreciation formula, you cannot use a comma for decimal places (e.g., 12345,67).</p>
          </div>
          <br>
        </li>
        <li>
          <p>~ (maximum of two values, i.e., 2~5 = 5)</p>
        </li>
        <li>
          <p>Original Asset Cost (OC) – The original cost of the asset, usually purchase price.</p>
        </li>
        <li>
          <p>Current Asset Cost (CC) – The current cost of the asset. This will typically be the same as original cost, but it provides an additional cost value to track and use where the cost may vary from the original cost. Write downs affect this value.</p>
        </li>
        <li>
          <p>Net Book Value (NB) – The current depreciated value of the asset.</p>
        </li>
        <li>
          <p>Residual Value (RV) – The minimum value the asset will be reduced to. This is usually zero, unless a residual value has been configured against the asset.</p>
        </li>
        <li>
          <p>Asset Lifetime (AL) – The number of periods an asset will be depreciated for (asset effective life for tax).</p>
        </li>
        <li>
          <p>Current Period or Age (CP) – The current age of the asset.</p>
        </li>
        <li>
          <p>Total Depreciation Amount (TD) – The total amount of depreciation applied to the asset.</p>
        </li>
        <li>
          <p>Current Usage (CU) – The current recorded usage of the asset.</p>
        </li>
        <li>
          <p>Lifetime Usage (LU) – The total usage lifetime configured against the asset.</p>
        </li>
        <li>
          <p>Last Depreciation Amount (LD) – The last depreciation amount.</p>
        </li>
        <li>
          <p>Days held in current period (DH) – The number of days between the asset acquisition date or start date of the current period (whichever comes later) and the end of life (disposal) date or the end of the current period (whichever comes earlier).</p>
        </li>
        <li>
          <p>Prior year net book value (PB) – The closing net book value at the end of the prior financial year as stored on the asset record. The start and end of the year for the method is determined by the Financial Year Start field. The Prior Year NBV is updated when the month being depreciated is the same as the month set as the Financial Year Start. This captures the NBV value as it was for the financial year that ended.</p>
        </li>
        <li>
          <p>Depreciation Period (DP) — The number of days in a period.</p>
        </li>
        <li>
          <p>Fiscal Year (FY) — The number of days in a fiscal year.</p>
        </li>
      </ul>
      <h3 id="bridgehead_N2140285">Formula Example: Straight Line Depreciation</h3>
      <p>Straight line depreciation formula:</p>
      <p>(CC-RV)/AL</p>
      <p>(Current Asset Cost – Residual Value) / Asset Lifetime</p>
      <p>Example:</p>
      <p>Current Asset Cost: 20,000</p>
      <p>Residual Value: 2,000</p>
      <p>Asset Lifetime: 60 months</p>
      <p>(20,000 – 2,000) / 60 = 300</p>
      <p>As it is a straight line depreciation this will be the same depreciation amount every month.</p>
      <h3 id="bridgehead_N2140329">Formula Example: Maximum of Two Values</h3>
      <p>The formula can also carry out two different depreciation calculations, and then select the calculation that returns the highest value to use for the deprecation. To use this functionality, the two different formulae are separated by the ~ character.</p>
      <p>For example, the formula for the 150DB method is:</p>
      <p class="indent_1">((NB-RV)*(1.5/AL))~((NB-RV)/(AL-CP+1))</p>
      <p>Fixed Assets Management will calculate the results of ((NB-RV)*(1.5/AL)) and ((NB-RV)/(AL-CP+1)) individually and then use the highest amount for the depreciation. The net effect of this example is that the first formula will return the highest value for the first part of the assets life before switching to the second.</p>
      <p class="indent_1">Month 1</p>
      <p class="indent_1">Net book value: 20,000</p>
      <p class="indent_1">Residual value: 2,000</p>
      <p class="indent_1">Asset lifetime: 60 (5 years)</p>
      <p class="indent_1">Current period: 1</p>
      <p class="indent_1">((20,000 – 2,000)*(1.5/60)) = 450</p>
      <p class="indent_1">((20,000 – 2,000)/(60 – 1 + 1)) = 300</p>
      <p>Therefore the first formula (450) is used.</p>
      <p class="indent_1">Month 30</p>
      <p class="indent_1">Net book value: 10,409</p>
      <p class="indent_1">Residual value: 2,000</p>
      <p class="indent_1">Asset lifetime: 60 (5 years)</p>
      <p class="indent_1">Current period: 30</p>
      <p class="indent_1">((10,409 – 2,000)*(1.5/60)) = 210</p>
      <p class="indent_1">((10,409 – 2,000)/(60 – 30 + 1)) = 271</p>
      <p>Now the second formula (271) will be used, and the same amount will be used for the remainder of the lifetime because the second is a straight line depreciation method. In this example, the formula switches a third of the way through at about period 20.</p>
      <h3 id="bridgehead_N2140427">Formula Example: Diminishing Value Method for Tax</h3>
      <p>You can create a diminishing value method to calculate tax depreciation using a reduced rate in the initial period (year of acquisition). For example:</p>
      <p class="indent_1"><strong>Formula</strong>: (NB)*(DH/365)*(200/(AL/12)/100)</p>
      <p class="indent_1"><strong>Depreciation Period</strong>: Monthly</p>
      <p>Net book value is the depreciated value for tax purposes at the end of the prior period. The Prior Year Net Book Value is the depreciated value for tax purposes at the end of the prior year.</p>
      <div class="nshelp_relatedtopics">
        <h3 class="nshelp_relatedtopics">Related Topics</h3>
        <div class="nshelp_toc">
          <div class="nshelp_navigation">
            <dl>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2126441" href="./chapter_N2126441.html">Fixed Assets Management Overview</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2126830" href="./chapter_N2126830.html">Fixed Assets Management SuiteApp</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="chapter_N2137458" href="./chapter_N2137458.html">Depreciation Methods</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2137654" href="./section_N2137654.html">Preconfigured Depreciation Methods</a>
                </span>
              </dt>
              <dt>
                <span class="nshelp_toc">
                  <a rev="section_N2139727" href="./section_N2139727.html">Creating a New Depreciation Method</a>
                </span>
              </dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="nshelp_footer" class="nshelp_footer">
    <div class="nshelp_navigation">
      <p class="nshelp_footer_notices">
        <a rev="chapter_N000004" href="./chapter_N000004.html">General Notices</a>
      </p>
      <p>
        <a rev="chapter_N000004" href="./chapter_N000004.html">© 2017 NetSuite Inc.</a>
      </p>
    </div>
  </div>
</div> 
 

